﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Soutenance2.Levels
{
    class levels
    {
        public levels()
        {

        }

        public void level00(ref List<Rectangle> projCollision, ref Sprite map, ContentManager content, ref List<IA> IAList, ref List<Player> playerList, SpriteFont font, int heightScreen, int widthScreen, List<string> nameList, Sprite playerLeft, Sprite playerRight, Sprite IALeft, Sprite IARight, GraphicsDevice graphics)
        {
            //generation de la map
            map = new Sprite(content);
            map.LoadContent("Textures/map00");

            //Generation des joueurs / ia
            Play play = new Play(map);
            play.CreateListPlayers(ref projCollision, content, graphics, playerList, null, nameList, "Genius", playerLeft.Texture, playerRight.Texture, content.Load<SpriteFont>("name"), heightScreen, widthScreen, true, 2, true);
            play.CreateListPlayers(ref projCollision, content, graphics, null, IAList, nameList, "Berseker", IALeft.Texture, IARight.Texture, content.Load<SpriteFont>("name"), heightScreen, widthScreen, false, 2, true);
            StreamWriter sw = new StreamWriter("level00.txt");
            foreach (var item in projCollision)
            {
                sw.WriteLine("rect : " + item.X);
            }
            foreach (var item in playerList)
            {
                sw.WriteLine(item._positionPlayer.X);
            }
            sw.Close();

        }

        public void level01(ref List<Rectangle> projCollision, ref Sprite map, ContentManager content, ref List<IA> IAList, ref List<Player> playerList, SpriteFont font, int heightScreen, int widthScreen, List<string> nameList, Sprite playerLeft, Sprite playerRight, Sprite IALeft, Sprite IARight, GraphicsDevice graphics)
        {
            //generation de la map
            map = new Sprite(content);
            map.LoadContent("Textures/map01");

            //Generation des joueurs / ia
            Play play = new Play(map);
            play.CreateListPlayers(ref projCollision, content, graphics, playerList, null, nameList, "Genius", playerLeft.Texture, playerRight.Texture, content.Load<SpriteFont>("name"), heightScreen, widthScreen, true, 3, true);
            play.CreateListPlayers(ref projCollision, content, graphics, null, IAList, nameList, "Berseker", IALeft.Texture, IARight.Texture, content.Load<SpriteFont>("name"), heightScreen, widthScreen, false, 3, true);
            StreamWriter sw = new StreamWriter("level01.txt");
            foreach (var item in projCollision)
            {
                sw.WriteLine(item.X);
            }
            foreach (var item in playerList)
            {
                sw.WriteLine(item._positionPlayer.X);
            }
            sw.Close();
        }

        public void level02(ref List<Rectangle> projCollision, ref Sprite map, ContentManager content, ref List<IA> IAList, ref List<Player> playerList, SpriteFont font, int heightScreen, int widthScreen, List<string> nameList, Sprite playerLeft, Sprite playerRight, Sprite IALeft, Sprite IARight, GraphicsDevice graphics)
        {
            //generation de la map
            map = new Sprite(content);
            map.LoadContent("Textures/map02");
            Play.rainSprite.LoadContent("Textures/win8");
            Play.rain = new ParticleGenerator(Play.rainSprite.Texture, widthScreen / 2, 0.01f);
            //Generation des joueurs / ia
            Play play = new Play(map);
            play.CreateListPlayers(ref projCollision, content, graphics, playerList, null, nameList, "Genius", playerLeft.Texture, playerRight.Texture, content.Load<SpriteFont>("name"), heightScreen, widthScreen, true, 3, false);
            play.CreateListPlayers(ref projCollision, content, graphics, null, IAList, nameList, "Berseker", IALeft.Texture, IARight.Texture, content.Load<SpriteFont>("name"), heightScreen, widthScreen, false, 3, false);
            StreamWriter sw = new StreamWriter("level02.txt");
            foreach (var item in projCollision)
            {
                sw.WriteLine(item.X);
            }
            foreach (var item in playerList)
            {
                sw.WriteLine(item._positionPlayer.X);
            }
            sw.Close();
        }
    }
}
