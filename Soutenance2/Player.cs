﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Soutenance2
{
    class Player
    {
        //Une texture
        Texture2D _playerLeft, _playerRight;

        //une position de base
        public Vector2 _positionPlayer;
        Rectangle _positionRectangle;

        public Rectangle PositionRectangle { get { return _positionRectangle; } }


        //une animation
        AnimatedSprite _animatedPlayer;


        //une gravite
        Gravity _gravity;


        //Vie
        public int Life { get; set; }
        SpriteFont font;

        //pseudos
        public string teamName;
        public string nickName;

        //un tire
        public WeaponsClass _weapons;

        //position avec 0 = ne s'anime pas, 1 = droit, 2 = gauche
        int _direction;

        //differetes touches pour gerer plusieurs joueurs meme si c'est inutile je pense etant donnne que c'est du tour par tour
        Keys left, right, space, fire;

        //Taille de l'ecran 
        int _widthScreen, _heightScreen;

        #region Collision
        public bool colli = false;

        public bool col_bas = false;
        public bool col_gauche = false;
        public bool col_droite = false;

        public bool col_bas2 = false;
        public bool col_gauche2 = false;
        public bool col_droite2 = false;


        public Vector2 Position { get { return _positionPlayer; } set { value = _positionPlayer; } }
        #endregion

        //Player pour weapons
        public Player(ContentManager content, GraphicsDevice graphics, Texture2D playerLeft, Texture2D playerRight, SpriteFont font, int origineX, string teamName, string nickName, int height, int width, Keys left, Keys right, Keys space, Keys fire)
        {
            this._playerLeft = playerLeft;
            this._playerRight = playerRight;
            this._widthScreen = width;
            this._heightScreen = height;
            this.left = left;
            this.right = right;
            this.space = space;
            this.fire = fire;
            _positionPlayer.X = origineX;
            //on charge la gravite et l'animation ainsi que le projectile

            this._gravity = new Gravity(height);
            this._animatedPlayer = new AnimatedSprite(playerRight, playerLeft, 2, 2, 2, 2);

            //Vie
            Life = 100;
            this.font = font;

            //pseudos
            this.teamName = "[" + teamName + "]";
            this.nickName = this.teamName + nickName;
            _weapons = new WeaponsClass(content, graphics, /*new Vector2(0,0),*/ width, height, playerLeft);
            _positionRectangle = new Rectangle((int)_positionPlayer.X, (int)_positionPlayer.Y, _playerLeft.Width / 4, _playerLeft.Height / 4);
            Collision.Collision.PersoColor = new Color[playerLeft.Width * playerLeft.Height];
            playerLeft.GetData(Collision.Collision.PersoColor);

        }

        public Player(ContentManager content, GraphicsDevice graphics, Texture2D playerLeft, Texture2D playerRight, SpriteFont font, int origineX, string teamName, string nickName, int height, int width, Keys left, Keys right, Keys space, Keys fire, int INYOURFACEBABY)
        {
            this._playerLeft = playerLeft;
            this._playerRight = playerRight;
            this._widthScreen = width;
            this._heightScreen = height;
            this.left = left;
            this.right = right;
            this.space = space;
            this.fire = fire;
            _positionPlayer.X = origineX;
            //on charge la gravite et l'animation ainsi que le projectile

            this._gravity = new Gravity(height);
            this._animatedPlayer = new AnimatedSprite(playerRight, playerLeft, 2, 2, 2, 2);

            //Vie
            Life = 100;
            this.font = font;

            //pseudos
            this.teamName = "[" + teamName + "]";
            this.nickName = this.teamName + nickName;
            _weapons = new WeaponsClass(content, graphics, /*new Vector2(0,0),*/ width, height, playerLeft);
            _positionRectangle = new Rectangle((int)_positionPlayer.X, (int)_positionPlayer.Y, _playerLeft.Width / 4, _playerLeft.Height / 4);
        }

        //Initialisation du personnage
        public void Init(Texture2D map)
        {
            _positionPlayer.Y = _positionRectangle.Y = 0;
            while (!col_bas && _positionPlayer.Y < _heightScreen)
            {
                _positionPlayer.Y += 20;
                _positionRectangle.Y += 20;
                col_bas = Collision.Collision.col_bas(new Rectangle(-_widthScreen / 2, 0, map.Width, map.Height), _positionRectangle);
                if (_positionPlayer.Y >= _heightScreen)
                {
                    _positionPlayer.Y = _heightScreen / 2;
                }
            }
        }

        public void InitMulti(Texture2D map)
        {
            /*_positionPlayer.Y = _heightScreen;
            _positionRectangle.Y = _heightScreen;*/
            int tmp = (int)_positionPlayer.X;
            if (_positionPlayer.X < 0)
            {
                tmp += 800;
            }
            for (int y = 0; y < map.Height; y++)
            {
                for (int x = tmp; x < tmp + _playerLeft.Width / 3; x++)
                {
                    if (Collision.Collision.TerrainColor[y * map.Width + x].A == 255)
                    {
                        _positionPlayer.Y = y;
                        _positionRectangle.Y = y;
                        return;
                    }
                }
            }
        }

        //Si le perso saute/tire -> vrai
        public bool isActive()
        {
            //if (_weapons._alreadyShoot || _positionPlayer.Y != _heightScreen) return true;
            if (_weapons._alreadyShoot || (!col_bas && _positionPlayer.Y < _heightScreen + _playerLeft.Height)) return true;
            return false;
        }

 public bool isActive(Network network)
        {
            if (network._alreadyShoot || _weapons._weaponNbr != 0 || (!col_bas && _positionPlayer.Y < _heightScreen + _playerLeft.Height)) return true;
            return false;
        }

        //Update Weapons
        public void Update(GameTime gameTime/*, bool haveShoot*/, MouseState mouse)
        {
            mouse = Mouse.GetState();
            _direction = 0;

            if (Keyboard.GetState().IsKeyDown(left) && _positionPlayer.X > -_widthScreen / 2)
            {
                _direction = 2;
                if (col_bas)
                {
                    _positionPlayer.Y -= 3;
                }

                if(!col_gauche)
                {
                    _positionPlayer.X -= 3;
                }
            }

            //Si right et posPlay < widthScree /2 (on est centre sur 0)
            if (Keyboard.GetState().IsKeyDown(right) && _positionPlayer.X + _playerRight.Width / 2 < _widthScreen / 2 + _playerRight.Width / 2)
            {
                _direction = 1;
                if (col_bas)
                {
                    _positionPlayer.Y -= 3;
                }

                if(!col_droite)
                {
                    _positionPlayer.X += 3;
                }
            }
            _positionPlayer.Y = _gravity.positionY(gameTime, Keyboard.GetState().IsKeyDown(Keys.Space), col_bas, _positionPlayer.Y);

            //Update de l'animation et du projectile
            _animatedPlayer.Update(_positionPlayer, _direction);
            _weapons.Update(gameTime, mouse, _positionPlayer, _direction);
            _positionRectangle.X = (int)_positionPlayer.X;
            _positionRectangle.Y = (int)_positionPlayer.Y;
            Position = _positionPlayer;
        }

        public void UpdateMulti(GameTime gameTime, MouseState mouse, Network network)
        {
            _direction = 0;
            if (Keyboard.GetState().IsKeyDown(left) && _positionPlayer.X > 0)
            {
                _direction = 2;
                //_positionPlayer.X -= 6;
                if (col_bas)
                {
                    _positionPlayer.Y -= 3;
                }

                if (!col_droite)
                {
                    _positionPlayer.X -= 3;
                }

                if (network._isPlayer1)
                {
                    network.shootDirectionLeft1 = true;
                }
                else
                {
                    network.shootDirectionLeft2 = true;
                }
            }

            if (Keyboard.GetState().IsKeyDown(right) && _positionPlayer.X + _playerRight.Width / 4 < _widthScreen)
            {
                _direction = 1;
                //_positionPlayer.X += 6;
                if (col_bas)
                {
                    _positionPlayer.Y -= 3;
                }

                if (!col_droite)
                {
                    _positionPlayer.X += 3;
                }

                if (network._isPlayer1)
                {
                    network.shootDirectionLeft1 = false;
                }
                else
                {
                    network.shootDirectionLeft2 = false;
                }
            }
            _positionRectangle = new Rectangle((int)_positionPlayer.X, (int)_positionPlayer.Y, PositionRectangle.Width, PositionRectangle.Height);

            //Saut
            bool saut = Keyboard.GetState().IsKeyDown(space);
            if (network._isPlayer1)
                network._jump = saut;
            else
                network._jumpPlayer2 = saut;
            _positionPlayer.Y = _gravity.positionY(gameTime, saut, col_bas, _positionPlayer.Y);

            //Update de l'animation et des armes
            _animatedPlayer.Update(_positionPlayer, _direction);
            _weapons.UpdateMulti(gameTime, _positionPlayer, mouse, _direction, network);
            Position = _positionPlayer;
        }

        public void UpdateMulti(GameTime gameTime, int positionX, bool jump, int nbrWeapon, bool shoot, Network network, int direction)
        {
            if (col_bas && positionX == _positionPlayer.X)
                _positionPlayer.Y -= 3;

            _positionPlayer.X = positionX;
            _positionPlayer.Y = _gravity.positionY(gameTime, jump, col_bas, _positionPlayer.Y);

            _animatedPlayer.Update(_positionPlayer, direction);

            _positionRectangle.X = (int)_positionPlayer.X;
            _positionRectangle.Y = (int)_positionPlayer.Y;
            //Armes
            if (network._isPlayer1)
                _weapons.UpdateMulti(gameTime, _positionPlayer, network._shootPlayer2, network);
            else
                _weapons.UpdateMulti(gameTime, _positionPlayer, network._shoot, network);
        }

        //Update normal..
        public void Draw(SpriteBatch spriteBatch)
        {
            //if (_projectile.tir) _projectile.Draw(spriteBatch);
            _animatedPlayer.Draw(spriteBatch, _positionPlayer);

            //Display life and nickname
            spriteBatch.DrawString(font, Life.ToString(), new Vector2(_positionPlayer.X + _playerLeft.Width / 8 - (font.MeasureString(Life.ToString()).X / 2), _positionPlayer.Y - font.MeasureString(Life.ToString()).Y * 2), Color.Cyan);
            spriteBatch.DrawString(font, nickName, new Vector2(_positionPlayer.X + _playerLeft.Width / 8 - (font.MeasureString(nickName).X / 2), _positionPlayer.Y - font.MeasureString(nickName).Y * 3), Color.Cyan);
        }

        public void DrawWeapons(SpriteBatch spriteBatch)
        {
            _animatedPlayer.Draw(spriteBatch, _positionPlayer);

            if (Keyboard.GetState().IsKeyDown(Keys.F) || _weapons._alreadyShoot || _weapons.sheepActive)
                _weapons.Draw(spriteBatch);

            //Display life and nickname
            spriteBatch.DrawString(font, Life.ToString(), new Vector2(_positionPlayer.X + _playerLeft.Width / 8 - (font.MeasureString(Life.ToString()).X / 2), _positionPlayer.Y - font.MeasureString(Life.ToString()).Y * 2), Color.Cyan);
            spriteBatch.DrawString(font, nickName, new Vector2(_positionPlayer.X + _playerLeft.Width / 8 - (font.MeasureString(nickName).X / 2), _positionPlayer.Y - font.MeasureString(nickName).Y * 3), Color.Cyan);
        }

        //Draw Multi
        public void DrawWeapons(SpriteBatch spriteBatch, Color color, int nbrWeapon, Network network)
        {
            //Animations
            _animatedPlayer.Draw(spriteBatch, _positionPlayer);

            //Armes
            if (Keyboard.GetState().IsKeyDown(Keys.F) || _weapons.sheepActive || network._alreadyShoot)
                _weapons.DrawMulti(spriteBatch, nbrWeapon, network);

            //Display life and nickname
            spriteBatch.DrawString(font, Life.ToString(), new Vector2(_positionPlayer.X + _playerLeft.Width / 8 - (font.MeasureString(Life.ToString()).X / 2), _positionPlayer.Y - font.MeasureString(Life.ToString()).Y * 2), color);
            spriteBatch.DrawString(font, nickName, new Vector2(_positionPlayer.X + _playerLeft.Width / 8 - (font.MeasureString(nickName).X / 2), _positionPlayer.Y - font.MeasureString(nickName).Y * 3), color);
        }
    }
}