using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Soutenance2
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        ManageMenus menus;
        //Screen
        int heigthScreen;
        int widthScreen;

        Video video;
        VideoPlayer play;
        Texture2D videoTexture;
        bool playIt = true;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            IsMouseVisible = true;
            heigthScreen = 900;
            widthScreen = 1600;
            graphics.PreferredBackBufferHeight = heigthScreen;
            graphics.PreferredBackBufferWidth = widthScreen;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();


            base.Initialize();
        }


        protected override void LoadContent()
        {
            //test = Contenu.BackgroundGame;

            spriteBatch = new SpriteBatch(GraphicsDevice);
            video = Content.Load<Video>("intro-1");
            play = new VideoPlayer();
            play.IsLooped = false;
            menus = new ManageMenus(widthScreen, heigthScreen, Content.Load<Song>("Songs/mainTheme"), Content.Load<Song>("Songs/gameTheme"), Content.Load<SoundEffect>("Songs/gunSound"), graphics.GraphicsDevice, Content, graphics);
        }


        protected override void UnloadContent()
        {

        }


        protected override void Update(GameTime gameTime)
        {
            //Permet de quitter
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) || menus.currentGameState == ManageMenus.GameState.exit)
            {
                MediaPlayer.Stop();
                this.Exit();
            }

            if (playIt)
            {
                play.Play(video);

            }
            if (play.PlayPosition == (new TimeSpan(0, 0, 0, 2, 971)))
            {
                play.Stop();
                playIt = false;
            }
            if (play.State == MediaState.Stopped)
            {

                //Update du menu (sans blague !?)
                MouseState mouse = Mouse.GetState();
                playIt = false;
                menus.Update(mouse, gameTime);
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            if (playIt)
            {
                if (play.State != MediaState.Stopped)
                    videoTexture = play.GetTexture();

                spriteBatch.Begin();
                if (videoTexture != null)
                    spriteBatch.Draw(videoTexture, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.End();
            }
            else
            {
                //Permet de faire le cafe mais n'apporte pas encore les cookies...
                menus.Draw(spriteBatch);
            }
            base.Draw(gameTime);
        }
    }
}
