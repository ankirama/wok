﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Soutenance2
{
    class Gravity
    {
        //Gravite
        float position, velocity;
        const float gravity = 30f;
        float jumpSpeed = 650f;
        bool jump = false;
        private int _basePosition;



        float positionYVar;

        public Gravity(int basePosition)
        {
            _basePosition = basePosition;
            position = basePosition;
        }

        public float positionY(GameTime gameTime, bool active)
        {
            if (active && jump)
            {
                velocity = -jumpSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                jump = false;
            }

            if (!jump)
                velocity += gravity * (float)gameTime.ElapsedGameTime.TotalSeconds;
            else
                velocity = 0;

            position += velocity;

            jump = position >= _basePosition;

            if (jump)
                position = _basePosition;

            return position;
        }

        public float positionY(GameTime gameTime, bool active, bool collision, float positionPlayer)
        {
            if (active && !jump)
            {
                velocity = -jumpSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                jump = true;
            }

            if (jump)
                velocity += gravity * (float)gameTime.ElapsedGameTime.TotalSeconds;
            else
                velocity = 0;

            positionPlayer += velocity;

            jump = !collision;

            return positionPlayer;
        }

        public float positionY(float time, bool active, bool collision, float positionPlayer)
        {
            if (active && !jump)
            {
                velocity = -jumpSpeed * time;
                jump = true;
            }

            if (jump)
                velocity += gravity * time;
            else
                velocity = 0;

            positionPlayer += velocity;

            jump = !collision;

            return positionPlayer;
        }

        public float positionY5(GameTime gametime, bool col_bas, bool col_gauche, bool col_droit)
        {
            //Console.WriteLine((float)gametime.ElapsedGameTime.TotalSeconds);
            if (col_bas)
            {
                positionYVar -= 5;
            }
            if (!col_bas)
            {
                positionYVar += 5;
            }

            //saut
            /*
            if (!col_bas)
            {
                velocity += gravity * (float)gametime.ElapsedGameTime.TotalSeconds;
                positionY += 5;
            }*/
            return positionYVar;
        }




    }
}
