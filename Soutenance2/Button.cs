﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Soutenance2
{
    class Button
    {
        private Texture2D _texture;

        private Vector2 _position;
        public Vector2 _size;
        private Rectangle _rectangle;

        private bool min = false, max = false;
        private float angle = 0;

        SpriteFont _font;
        string _name;

        int _widthScreen;

        private Color color = new Color(255, 255, 255, 255);
        private Color colorStr = new Color(255, 255, 255, 255);

        //booleen pour gerer la couleur du button et l'etat du bouton
        private bool down;
        public bool isClicked = false;

        //Constructeur de Button attribuant texture, taille et son
        public Button(Texture2D newTexture, GraphicsDevice graphics)
        {
            _texture = newTexture;
            _size = new Vector2(_texture.Width, _texture.Height);
        }

        public Button(Texture2D newTexture, GraphicsDevice graphics, int widthTexture)
        {
            _texture = newTexture;
            _size = new Vector2(_texture.Width, _texture.Height);
            _widthScreen = widthTexture;
        }

        public Button(Texture2D newTexture, GraphicsDevice graphics, int widthTexture, int heightTexture)
        {
            _texture = newTexture;
            _size = new Vector2(widthTexture, heightTexture);
            _widthScreen = widthTexture;
        }

        public Button(string str, SpriteFont font, GraphicsDevice graphics)
        {
            this._font = font;
            this._name = str;
            _size = new Vector2(font.MeasureString(str).X, 20);
        }
        public Button(string str, SpriteFont font, GraphicsDevice graphics, int widthScreen)
        {
            this._font = font;
            this._name = str;
            _size = new Vector2(font.MeasureString(str).X, 20);
            _widthScreen = widthScreen;
        }


        //Fonction Update avec comme parametre l'etat de la souris
        public void Update(MouseState mouse)
        {
            _rectangle = new Rectangle((int)_position.X, (int)_position.Y, (int)_size.X, (int)_size.Y); //Rectangle du bouton donc ca position + sa taille

            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1); //Rectangle de la souris avec comme dimensions 1,1

            //Boucle permettant de savoir si la souris est sur le bouton + modifie la couche Alpha du bouton
            //Petit probleme a ce niveau la du surement a la fonction update principale, le bouton est clique plusieurs fois au lieu d'une
            if (mouseRectangle.Intersects(_rectangle))
            {
                if (color.A == 255) down = false;
                if (color.A == 0) down = true;
                if (down) color.A += 3; else color.A -= 3;
                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    isClicked = true;
                }

                else if (mouse.LeftButton == ButtonState.Released) isClicked = false;

                else
                    isClicked = false;

            }

            //Si la couche alpha est plus petite que 255 et que la souris n'est pas en intersection alors on reattribue doucement la couche alpha
            else if (color.A < 255)
            {
                color.A += 3;
                isClicked = false;
            }

        }

        public void UpdateWeapons(MouseState mouse)
        {
            _rectangle = new Rectangle((int)_position.X + _widthScreen / 2 - Camera.move, (int)_position.Y, (int)_size.X, (int)_size.Y); //Rectangle du bouton donc ca position + sa taille

            Rectangle mouseRectangle = new Rectangle(mouse.X - _widthScreen /2 + _widthScreen / 2 - Camera.move, mouse.Y, 1, 1); //Rectangle de la souris avec comme dimensions 1,1

            //Boucle permettant de savoir si la souris est sur le bouton + modifie la couche Alpha du bouton
            //Petit probleme a ce niveau la du surement a la fonction update principale, le bouton est clique plusieurs fois au lieu d'une
            if (mouseRectangle.Intersects(_rectangle))
            {
                if (color.B == 255) down = false;
                if (color.B == 0) down = true;
                if (down) color.B += 3; else color.B -= 3;
                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    isClicked = true;
                }

                else if (mouse.LeftButton == ButtonState.Released) isClicked = false;

                else
                    isClicked = false;

            }

            //Si la couche alpha est plus petite que 255 et que la souris n'est pas en intersection alors on reattribue doucement la couche alpha
            else if (color.B < 255)
            {
                color.B += 3;
                isClicked = false;
            }

        }

        //Special pause car valeur dedans...
        public void UpdateStringPauseCamera(MouseState mouse)
        {
            _rectangle = new Rectangle((int)_position.X + _widthScreen/2 - Camera.move, (int)_position.Y, (int)_size.X, (int)_size.Y); //Rectangle du bouton donc ca position + sa taille

            #region valeur bullshit!
            Rectangle mouseRectangle = new Rectangle(mouse.X - _widthScreen / 2 + _widthScreen / 2 - Camera.move, mouse.Y, 1, 1); //Rectangle de la souris avec comme dimensions 1,1
            #endregion

            //Boucle permettant de savoir si la souris est sur le bouton + modifie la couche Alpha du bouton
            //Petit probleme a ce niveau la du surement a la fonction update principale, le bouton est clique plusieurs fois au lieu d'une
            if (mouseRectangle.Intersects(_rectangle))
            {
                if (colorStr.R == 255) down = false;
                if (colorStr.R == 0) down = true;
                if (down) colorStr.R += 5; else colorStr.R -= 5;
                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    isClicked = true;
                }

                else if (mouse.LeftButton == ButtonState.Released) isClicked = false;

                else
                    isClicked = false;

            }

            //Si la couche Beta est plus petite que 255 et que la souris n'est pas en intersection alors on reattribue doucement la couche alpha
            else if (colorStr.R < 255)
            {
                colorStr.R += 5;
                isClicked = false;
            }

        }

        public void UpdateStringPause(MouseState mouse)
        {
            _rectangle = new Rectangle((int)_position.X, (int)_position.Y, (int)_size.X, (int)_size.Y); //Rectangle du bouton donc ca position + sa taille

            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1); //Rectangle de la souris avec comme dimensions 1,1

            //Boucle permettant de savoir si la souris est sur le bouton + modifie la couche Alpha du bouton
            //Petit probleme a ce niveau la du surement a la fonction update principale, le bouton est clique plusieurs fois au lieu d'une
            if (mouseRectangle.Intersects(_rectangle))
            {
                if (colorStr.R == 255) down = false;
                if (colorStr.R == 0) down = true;
                if (down) colorStr.R += 5; else colorStr.R -= 5;
                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    isClicked = true;
                }

                else if (mouse.LeftButton == ButtonState.Released) isClicked = false;

                else
                    isClicked = false;

            }

            //Si la couche Beta est plus petite que 255 et que la souris n'est pas en intersection alors on reattribue doucement la couche alpha
            else if (colorStr.R < 255)
            {
                colorStr.R += 5;
                isClicked = false;
            }

        }

        public void UpdateMove(MouseState mouse)
        {
            _rectangle = new Rectangle((int)_position.X, (int)_position.Y, (int)_size.X, (int)_size.Y); //Rectangle du bouton donc ca position + sa taille
            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1); //Rectangle de la souris avec comme dimensions 1,1


            if (mouseRectangle.Intersects(_rectangle))
            {

                if (angle >= 1)
                {
                    max = true;
                }

                if (!max)
                    angle += 0.04f;

                if (angle <= -1f) min = true;

                if (max && !min) angle -= 0.04f;

                if (min)
                {
                    max = false;
                    min = false;
                }

                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    isClicked = true;
                }

                else if (mouse.LeftButton == ButtonState.Released) isClicked = false;

                else
                    isClicked = false;
            }

            else if(angle >= -0.05f || angle <= 0.05f)
            {
                if (angle >= 1)
                {
                    max = true;
                }

                if (!max)
                    angle += 0.02f;

                if (angle <= -1f) min = true;

                if (max && !min) angle -= 0.02f;

                if (min)
                {
                    max = false;
                    min = false;
                }
                isClicked = false;
            }
        }

        public void UpdateString(MouseState mouse)
        {
            _rectangle = new Rectangle((int)_position.X, (int)_position.Y, (int)_size.X, (int)_size.Y); //Rectangle du bouton donc ca position + sa taille

            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1); //Rectangle de la souris avec comme dimensions 1,1

            //Boucle permettant de savoir si la souris est sur le bouton + modifie la couche Alpha du bouton
            //Petit probleme a ce niveau la du surement a la fonction update principale, le bouton est clique plusieurs fois au lieu d'une
            if (mouseRectangle.Intersects(_rectangle))
            {
                if (colorStr.B == 255) down = false;
                if (colorStr.B == 0) down = true;
                if (down) colorStr.B += 5; else colorStr.B -= 5;
                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    isClicked = true;
                    System.Threading.Thread.Sleep(100);
                }

                else if (mouse.LeftButton == ButtonState.Released)
                {
                    isClicked = false;
                }

                else
                    isClicked = false;

            }

            //Si la couche Beta est plus petite que 255 et que la souris n'est pas en intersection alors on reattribue doucement la couche alpha
            else if (colorStr.B < 255)
            {
                colorStr.B += 5;
                isClicked = false;
            }

        }

        //Definit la position du bouton
        public void setPosition(Vector2 newPosition)
        {
            _position = newPosition;
        }

        public void setPositionRect(Rectangle newRectangle)
        {
            _rectangle = newRectangle;
        }

        //Dessine le bouton
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, _rectangle, color);
        }

        public void DrawString(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(_font, _name, new Vector2(_rectangle.X, _rectangle.Y), colorStr);
        }

        public void DrawMove(SpriteBatch spriteBatch)
        {
            Rectangle sourceRectangle = new Rectangle(0, 0, _texture.Width, _texture.Height);
            Vector2 origin = new Vector2(_texture.Width / 2 - 5, 0);

            spriteBatch.Draw(_texture, _position, sourceRectangle, Color.White, angle, origin, 1.0f, SpriteEffects.None, 1);
        }

        public void DrawWeapons(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, new Vector2(_position.X + _widthScreen / 2 - Camera.move, _position.Y), color);
        }
    }
}
