﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using System;

namespace Soutenance2
{
    class Projectile
    {
        Rectangle _positionProjectile;
        Texture2D _player, _projectile;

        bool _alreadyShoot = false;
        public bool fireKeyDown = false;

        SoundEffect _gunSound;

        int _widthScreen;

        bool leftDirection = false;
        public bool tir = false;

        //Touche (coule ?)
        public bool touch = false;
        public bool Touch { get { return touch; } set { touch = value; } }

        public Rectangle PositionRectangle { get { return _positionProjectile; } }

        public Projectile(SoundEffect newGunSound, Texture2D projectile, Texture2D player, Vector2 position, int widthScreen)
        {
            _widthScreen = widthScreen;
            this._gunSound = newGunSound;
            this._player = player;
            this._projectile = projectile;

            //A changer le /20 a cause de la taille de la texture de Corentin ! :D
            _positionProjectile = new Rectangle((int)position.X + player.Width, (int)position.Y, _projectile.Width / 20, _projectile.Height / 20);
        }

        public void Update(Vector2 position)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                //touche gauche -> direction a gauche vrai et deplacement a gauche
                if (!tir)
                    leftDirection = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                //touche droite -> direction a gauche fausse et deplacement a droite
                if (!tir)
                    leftDirection = false;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                fireKeyDown = true;
                //appui sur espace -> le tir s'active en fonction de la direction du perso(grace a directionGauche)
                if (!tir)
                {
                    _gunSound.Play();
                    //autre secteur a changer en cas de plusieur projectiles : appel de classe pour la trajectoire
                    tir = true;
                    touch = false;
                    if (leftDirection)
                    {
                        _positionProjectile.X -= 10;
                    }
                    else
                    {
                        _positionProjectile.X += 10;
                    }
                }

            }
            else if (Keyboard.GetState().IsKeyUp(Keys.Enter) && tir == false)
            {
                //initialise la position du projectile avec celle du perso quand il na pas tire
                _positionProjectile.X = (int)position.X + _player.Width / 8;
                _positionProjectile.Y = (int)position.Y + _player.Height / 8;

            }

            //reinitialise le tir a faux si le projectile sort de la map ou entre en collision avec qqch
            if (_positionProjectile.X > _widthScreen / 2 || _positionProjectile.X < -_widthScreen / 2 || touch)
                tir = false;

            //continue la trajectoire du projectile dans la bonne direction sans continuer l'appui
            if (tir && leftDirection)
            {
                _positionProjectile.X -= 10;

            }//secteur a changer en cas de differents projectiles(autre classe pour roquette, balle,...)
            else if (tir && !leftDirection)
            {
                _positionProjectile.X += 10;

            }
        }

        public void UpdateMulti(Vector2 position)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                //touche gauche -> direction a gauche vrai et deplacement a gauche
                if (!tir)
                    leftDirection = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                //touche droite -> direction a gauche fausse et deplacement a droite
                if (!tir)
                    leftDirection = false;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                //appui sur espace -> le tir s'active en fonction de la direction du perso(grace a directionGauche)
                if (!tir)
                {
                    _gunSound.Play();
                    //autre secteur a changer en cas de plusieur projectiles : appel de classe pour la trajectoire
                    tir = true;
                    if (leftDirection)
                    {
                        _positionProjectile.X -= 10;
                    }
                    else
                    {
                        _positionProjectile.X += 10;
                    }
                }

            }
            else if (Keyboard.GetState().IsKeyUp(Keys.Enter) && tir == false)
            {
                //initialise la position du projectile avec celle du perso quand il na pas tire
                _positionProjectile.X = (int)position.X + _player.Width / 8 - _player.Width / 8;
                _positionProjectile.Y = (int)position.Y + _player.Height / 8 - _player.Height / 8;

            }

            //reinitialise le tir a faux si le projectile sort de la map ou entre en collision avec qqch(plus tard)
            if (_positionProjectile.X > _widthScreen || _positionProjectile.X <= 0)
                tir = false;

            //continue la trajectoire du projectile dans la bonne direction sans continuer l'appui
            if (tir && leftDirection)
            {
                _positionProjectile.X -= 10;

            }//secteur a changer en cas de differents projectiles(autre classe pour roquette, balle,...)
            else if (tir && !leftDirection)
            {
                _positionProjectile.X += 10;

            }
        }

        public void UpdateMultiLautreJouerDeOufCommeLaPhrase(Vector2 position, bool shootDirectionLeft, bool press, bool alreadyShoot)
        {

            if (press && !tir)
            {
                _positionProjectile.X = (int)position.X + _player.Width / 4;
                //appui sur espace -> le tir s'active en fonction de la direction du perso(grace a directionGauche)
                if (!tir)
                {
                    _gunSound.Play();
                    //autre secteur a changer en cas de plusieur projectiles : appel de classe pour la trajectoire
                    tir = true;

                    //gauche
                    if (shootDirectionLeft)
                    {
                        _positionProjectile.X -= 10;
                    }

                    //droite
                    else if (!shootDirectionLeft)
                    {
                        _positionProjectile.X += 10;
                    }
                }
            }
            else if (!press && tir == false)
            {
                //initialise la position du projectile avec celle du perso quand il na pas tire
                _positionProjectile.X = (int)position.X + _player.Width / 4;
                _positionProjectile.Y = (int)position.Y + _player.Height / 8;

            }

            //reinitialise le tir a faux si le projectile sort de la map ou entre en collision avec qqch(plus tard)
            if (alreadyShoot)
            {
                if (_positionProjectile.X > _widthScreen || _positionProjectile.X <= 0)
                {
                    tir = false;
                }
            }

            //continue la trajectoire du projectile dans la bonne direction sans continuer l'appui
            if (tir && shootDirectionLeft)
            {
                _positionProjectile.X -= 10;

            }//secteur a changer en cas de differents projectiles(autre classe pour roquette, balle,...)
            else if (tir && !shootDirectionLeft)
            {
                _positionProjectile.X += 10;
            }

            //_positionProjectile.Y = (int)position.Y + _player.Height / 8;
        }

        public void UpdateIA(Vector2 position, bool shootDirectionLeft, bool press, bool alreadyShootIA, int test)
        {
            if (!_alreadyShoot)
            {
                touch = false;
                _alreadyShoot = true;
            } 

            if (!tir)
            {
                if (press)
                {
                    _positionProjectile.X = (int)position.X;
                    _positionProjectile.Y = (int)position.Y;
                    tir = true;
                    _gunSound.Play();
                }
            }
            else
            {
                if (_positionProjectile.X > _widthScreen / 2 || _positionProjectile.X < -_widthScreen / 2 || touch)
                {
                    _alreadyShoot = false;
                    tir = false;
                    
                }
                else
                {
                    if (shootDirectionLeft)
                        _positionProjectile.X -= 10;
                    else
                        _positionProjectile.X += 10;
                }
            }

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_projectile, _positionProjectile, Color.White);

        }


    }
}
