﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

/*
 * 
 * 
 * Gestion des menus
 * 
 * 
 */
namespace Soutenance2
{
    class ManageMenus
    {
        ContentManager content;
        //Creation des autres classes (jeu + MainMenu + Option)
        Play jeu;
        MainMenu mainMenu;
        Options optionsMenu;
        Pause pauseMenu, pauseMenuMulti;
        MultiPlayers multiMenu;

        //Textures
        Sprite pauseBackground, gameBackground, map00, map01, map02;
        List<Sprite> maps = new List<Sprite>();
        //Taille de l'ecran
        int _heigthScreen, _widthScreen;

        bool alreadyPress = false;

        SpriteFont sprite;

        bool keyPause = false, keyExit = false;

        bool multiPause = false;

        //Enumeration des differents etats du jeu
        public enum GameState
        {
            mainMenu,
            gameMenu,
            optionMenu,
            pauseMenu,
            multiMenu,
            exit
        }

        //Etat actuel du jeu
        public GameState currentGameState = GameState.mainMenu;
        public GameState lastGameState;

        //Sons
        Song mainTheme, gameTheme;
        SoundEffect gunSound;

        Camera camera;

        Rectangle rect;

        GraphicsDevice graphics;

        //Initialisation + chargement des textures et autres
        public ManageMenus(int widthScreen, int heightScreen, Song newMainTheme, Song newGameTheme, SoundEffect newGunSound, GraphicsDevice graphics, ContentManager content, GraphicsDeviceManager manageGraphics)
        {
            sprite = content.Load<SpriteFont>("name");


            this.content = content;
            //Chargement textures
            pauseBackground = new Sprite(content);
            pauseBackground.LoadContent("Textures/backgroundPause");
            gameBackground = new Sprite(content);
            gameBackground.LoadContent("Textures/backgroundGame");
            map00 = new Sprite(content);
            map00.LoadContent("Textures/map00");
            maps.Add(map00);
            map01 = new Sprite(content);
            map01.LoadContent("Textures/map01");
            maps.Add(map01);
            map02 = new Sprite(content);
            map02.LoadContent("Textures/map01");
            maps.Add(map02);

            gunSound = newGunSound;
            //Definition taille de l'ecran
            _widthScreen = widthScreen;
            _heigthScreen = heightScreen;

            //-400 car taille de la pause /2

            rect = new Rectangle(-400, _heigthScreen / 2 - pauseBackground.Height / 2 + 300, 800, 600);

            //Chargement des sons
            gameTheme = newGameTheme;
            mainTheme = newMainTheme;

            //Chargement des classes
            camera = new Camera(graphics.Viewport);
            mainMenu = new MainMenu(newMainTheme, _heigthScreen, _widthScreen, graphics, this.content);
            
            pauseMenuMulti = new Pause(400, _heigthScreen / 2 - pauseBackground.Height / 2 + 300, pauseBackground.Height, content, graphics, widthScreen);
            optionsMenu = new Options(content, widthScreen, heightScreen, manageGraphics);

            this.graphics = graphics;

            MediaPlayer.Volume = 1.5f;
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(mainTheme);
        }

        public void Update(MouseState mouse, GameTime gameTime)
        {
            //rect = new Rectangle(-400 + _widthScreen /2 - Camera.move, _heigthScreen / 2 - pauseBackground.Height / 2 + 300, 800, 600);
            //permet de gerer la souris
            mouse = Mouse.GetState();
            //Switch entre les differents etats du jeu
            switch (currentGameState)
            {

                case GameState.mainMenu:
                    {
                        if (mainMenu.currentGameState == MainMenu.GameState.gameMenu)
                        {
                            MediaPlayer.Play(gameTheme);
                            jeu = new Play(maps, gameTheme, gunSound, _heigthScreen, _widthScreen, this.content, camera, graphics);
                            pauseMenu = new Pause(rect.X, rect.Y, pauseBackground.Height, content, graphics, _widthScreen);
                            currentGameState = GameState.gameMenu;
                            mainMenu.currentGameState = MainMenu.GameState.mainMenu;
                        }

                        if (mainMenu.currentGameState == MainMenu.GameState.optionsMenu)
                        {
                            mainMenu.currentGameState = MainMenu.GameState.mainMenu;
                            lastGameState = GameState.mainMenu;
                            System.Threading.Thread.Sleep(100);
                            currentGameState = GameState.optionMenu;

                        }

                        if (mainMenu.currentGameState == MainMenu.GameState.multiMenu)
                        {
                            currentGameState = GameState.multiMenu;
                            multiMenu = new MultiPlayers(content, _heigthScreen, gunSound, _widthScreen, graphics);
                            mainMenu.currentGameState = MainMenu.GameState.mainMenu;
                        }

                        if (mainMenu.currentGameState == MainMenu.GameState.exit)
                        {
                            currentGameState = GameState.exit;
                            try
                            {
                                multiMenu.Close();
                            }
                            catch { }
                        }

                        mainMenu.Update(mouse);
                        break;
                    }


                case GameState.pauseMenu:
                    {
                        #region leave Pause

                        //Si on clique sur P ou on appuye sur Retourner dans le jeu on est redirige dans l'ancien etat : ici le jeu
                        if (!multiPause)
                        {
                            if ((!keyPause && Keyboard.GetState().IsKeyDown(Keys.P)) || pauseMenu.currentGameState == Pause.GameState.playMenu)
                            {
                                currentGameState = GameState.gameMenu;
                                keyPause = true;
                            }
                        }
                        else
                            if (((!keyPause && Keyboard.GetState().IsKeyDown(Keys.P)) || pauseMenuMulti.currentGameState == Pause.GameState.multMenu) || Network._pause)
                            {
                                currentGameState = GameState.multiMenu;
                                keyPause = true;
                            }

                        //Permet de cliquer qu'une fois pour eviter que ca bug
                        if ((keyPause && Keyboard.GetState().IsKeyUp(Keys.P)) || pauseMenu.currentGameState == Pause.GameState.playMenu || pauseMenuMulti.currentGameState == Pause.GameState.multMenu)
                        {
                            pauseMenu.currentGameState = Pause.GameState.pauseMenu;
                            pauseMenuMulti.currentGameState = Pause.GameState.pauseMenu;
                            keyPause = false;
                            System.Threading.Thread.Sleep(100);
                        }


                        #endregion

                        if (pauseMenu.currentGameState == Pause.GameState.optionsMenu || pauseMenuMulti.currentGameState == Pause.GameState.optionsMenu)
                        {
                            pauseMenu.currentGameState = Pause.GameState.pauseMenu;
                            pauseMenuMulti.currentGameState = Pause.GameState.pauseMenu;

                            lastGameState = GameState.pauseMenu;
                            System.Threading.Thread.Sleep(100);
                            currentGameState = GameState.optionMenu;
                        }

                        if (pauseMenu.currentGameState == Pause.GameState.exit || pauseMenuMulti.currentGameState == Pause.GameState.exit) currentGameState = GameState.exit;

                        pauseMenu.Update(mouse, false);
                        pauseMenuMulti.Update(mouse, true);
                        break;

                    }

                case GameState.gameMenu:
                    {
                        multiPause = false;
                        if (Play.leave.isClicked) currentGameState = GameState.exit;
                        #region Leave Game
                        if (!keyExit && Keyboard.GetState().IsKeyDown(Keys.E))
                        {
                            keyExit = true;
                            MediaPlayer.Stop();
                            System.Threading.Thread.Sleep(100);
                            MediaPlayer.Play(mainTheme);
                            currentGameState = GameState.mainMenu;
                            try
                            {
                                Play.playerRight.UnloadContent();
                                Play.playerLeft.UnloadContent();
                            }
                            catch { }
                        }

                        if (keyExit)
                        {
                            keyExit = false;
                        }
                        #endregion

                        #region Pause
                        //La meme que pour le pauseMenu
                        if (!keyPause && Keyboard.GetState().IsKeyDown(Keys.P) || pauseMenu.currentGameState == Pause.GameState.playMenu)
                        {
                            currentGameState = GameState.pauseMenu;
                            keyPause = true;
                        }

                        if (keyPause && Keyboard.GetState().IsKeyUp(Keys.P))
                        {
                            keyPause = false;
                        }


                        jeu.Update(gameTime, mouse);
                        #endregion

                        break;
                    }
                case GameState.multiMenu:
                    {
                        multiMenu.Update(gameTime, mouse);
                        multiPause = true;
                        #region Leave Game
                        if (!keyExit && Keyboard.GetState().IsKeyDown(Keys.E))
                        {
                            keyExit = true;
                            MediaPlayer.Stop();
                            System.Threading.Thread.Sleep(100);
                            MediaPlayer.Play(mainTheme);
                            currentGameState = GameState.mainMenu;
                            multiMenu = new MultiPlayers(content, _heigthScreen, gunSound, _widthScreen, graphics);
                        }
                        #endregion
                        #region Pause
                        //La meme que pour le pauseMenu
                        /*if ((!keyPause && Keyboard.GetState().IsKeyDown(Keys.P) || pauseMenuMulti.currentGameState == Pause.GameState.multMenu) || Network._pause)
                        {
                            currentGameState = GameState.pauseMenu;
                            keyPause = true;
                        }*/
                        if (Network._Player1)
                        {
                            if ((!keyPause && Keyboard.GetState().IsKeyDown(Keys.P) || pauseMenuMulti.currentGameState == Pause.GameState.multMenu) || Network._pausePlayer2)
                            {
                                pauseMenu = new Pause(rect.X, rect.Y, pauseBackground.Height, content, graphics, _widthScreen);
                                currentGameState = GameState.pauseMenu;
                                //Network.ITakeMyCoffePause();
                                keyPause = true;
                            }
                        }
                        else
                        {
                            if ((!keyPause && Keyboard.GetState().IsKeyDown(Keys.P) || pauseMenuMulti.currentGameState == Pause.GameState.multMenu) || Network._pause)
                            {
                                pauseMenu = new Pause(rect.X, rect.Y, pauseBackground.Height, content, graphics, _widthScreen);
                                currentGameState = GameState.pauseMenu;
                                //Network.ITakeMyCoffePause();
                                keyPause = true;
                            }
                        }

                        if (keyPause && Keyboard.GetState().IsKeyUp(Keys.P))
                        {
                            keyPause = false;
                        }
                        if (keyExit)
                        {
                            keyExit = false;
                        }
                        #endregion

                        break;
                    }
                case GameState.optionMenu:
                    {
                        optionsMenu.Update(mouse);
                        if (lastGameState == GameState.pauseMenu && optionsMenu.isClicked)
                        {
                            pauseMenu = new Pause(rect.X, rect.Y, pauseBackground.Height, content, graphics, _widthScreen);
                            currentGameState = GameState.pauseMenu;
                        }

                        if (lastGameState == GameState.mainMenu && optionsMenu.isClicked)
                        {
                            mainMenu = new MainMenu(mainTheme, _heigthScreen, _widthScreen, graphics, this.content);
                            currentGameState = GameState.mainMenu;
                        }
                        break;
                    }

                case GameState.exit:
                    {
                        try
                        {
                            multiMenu.Close();
                        }
                        catch { }
                        break;
                    }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {

            switch (currentGameState)
            {
                case GameState.mainMenu:
                    {
                        spriteBatch.Begin();
                        mainMenu.Draw(spriteBatch);
                        spriteBatch.End();
                        break;
                    }
                case GameState.pauseMenu:
                    {
                        //On dessine l'etat du jeu actuel pour voir derriere + le background de la pause + le pauseMenu
                        //spriteBatch.Begin();
                        
                        // spriteBatch.Draw(gameBackground.Texture, new Vector2(-_widthScreen / 2, 0), Color.White);
                        
                        if (!multiPause)
                        {
                            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.transform);

                            //spriteBatch.Draw(map00.Texture, new Vector2(-_widthScreen / 2, 0), Color.White);

                            jeu.Draw(spriteBatch);

                            spriteBatch.Draw(pauseBackground.Texture, new Rectangle(-400 + _widthScreen / 2 - Camera.move, _heigthScreen / 2 - pauseBackground.Height / 2 + 300, 800, 600), new Color(42, 42, 42, 100));

                            pauseMenu.Draw(spriteBatch);
                        }

                        //REGLER TON PB DE MULTI.DRAW!!!!!!!!!!!!
                        else
                        {

                            spriteBatch.Begin();
                            //spriteBatch.Draw(map00.Texture, new Vector2(0, 0), Color.White);

                            multiMenu.Draw(spriteBatch, true);
                            spriteBatch.Draw(pauseBackground.Texture, new Rectangle(_widthScreen / 2 - 400, _heigthScreen / 2 - 300, 800, 600), new Color(42, 42, 42, 100));

                            pauseMenuMulti.Draw(spriteBatch);
                        }
                        
                        
                        spriteBatch.End();
                        break;
                    }
                case GameState.gameMenu:
                    {
                        // spriteBatch.Begin();
                        spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.transform);
                        //spriteBatch.Draw(map00.Texture, new Vector2(-_widthScreen / 2, 0), Color.White);
                        jeu.Draw(spriteBatch);
                        spriteBatch.End();
                        break;
                    }

                case GameState.multiMenu:
                    {
                        spriteBatch.Begin();
                        multiMenu.Draw(spriteBatch, false);
                        spriteBatch.End();
                        break;
                    }

                case GameState.optionMenu:
                    {
                        spriteBatch.Begin();
                        optionsMenu.Draw(spriteBatch);
                        spriteBatch.End();
                        break;
                    }
                case GameState.exit:
                    {
                        //l'exit est gere que dans le game1
                        break;
                    }
            }

        }
    }
}
