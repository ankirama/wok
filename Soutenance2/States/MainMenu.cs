﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System;


namespace Soutenance2
{
    class MainMenu
    {

        /*
         * 
         * 
         * Menu principal
         * 
         * 
         */


        //Boutons
        Button playButton, exitButton, optionsButton, multiButton;

        //Background
        Sprite background, exitTexture, playTexture, optionsTexture, multiTexture;

        //Taille ecran
        int widthScreen, heigthScreen;

        //Differents menus
        public enum GameState
        {
            mainMenu,
            gameMenu,
            multiMenu,
            optionsMenu,
            exit
        }

        //Etat du menu
        public GameState currentGameState = GameState.mainMenu;

        //Initialisation + chargements des textures et autres
        public MainMenu(Song newMainTheme, int heigth, int width, GraphicsDevice graphics, ContentManager content)
        {
            Console.WriteLine(Options._english);
            //Chargement des Textures
            background = new Sprite(content);
            background.LoadContent("Textures/backgroundMain");
            playTexture = new Sprite(content);
            
            multiTexture = new Sprite(content);
            multiTexture.LoadContent("Buttons/Multi");
            
            optionsTexture = new Sprite(content);
            optionsTexture.LoadContent("Buttons/Options");
            exitTexture = new Sprite(content);
            

            //Chargement de la taille
            widthScreen = width;
            heigthScreen = heigth;

            //Chargement des boutons

            if (Options._english)
            {
                playTexture.LoadContent("Buttons/Play");
                exitTexture.LoadContent("Buttons/Exit");
            }
            else
            {
                playTexture.LoadContent("Buttons/Jouer");
                exitTexture.LoadContent("Buttons/Quitter");
            }

            playButton = new Button(playTexture.Texture, graphics, 180, 154);
            exitButton = new Button(exitTexture.Texture, graphics, 180, 154);
            multiButton = new Button(multiTexture.Texture, graphics, 180, 154);
            optionsButton = new Button(optionsTexture.Texture, graphics, 180, 154);

            playButton.setPosition(new Vector2(100, heigthScreen / 2));

            exitButton.setPosition(new Vector2(1500, heigthScreen / 2));

            multiButton.setPosition(new Vector2(400, heigthScreen / 2));

            optionsButton.setPosition(new Vector2(1200, heigthScreen / 2));

        }

        public void Update(MouseState mouse)
        {
            mouse = Mouse.GetState();

            //Mise a jour des boutons
            if (playButton.isClicked) currentGameState = GameState.gameMenu;
            if (exitButton.isClicked) currentGameState = GameState.exit;
            if (optionsButton.isClicked) currentGameState = GameState.optionsMenu;
            if (multiButton.isClicked) currentGameState = GameState.multiMenu;

            playButton.UpdateMove(mouse);
            exitButton.UpdateMove(mouse);
            optionsButton.UpdateMove(mouse);
            multiButton.UpdateMove(mouse);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background.Texture, new Rectangle(0, 0, widthScreen, heigthScreen), Color.White);
            playButton.DrawMove(spriteBatch);
            exitButton.DrawMove(spriteBatch);
            multiButton.DrawMove(spriteBatch);
            optionsButton.DrawMove(spriteBatch);
        }

    }
}
