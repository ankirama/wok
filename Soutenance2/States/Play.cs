﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Soutenance2
{
    class Play
    {
        #region variables globales
        static int heigthScreen, widthScreen;

        //Pour collision
        public List<Rectangle> projCollision = new List<Rectangle>();
        public List<Rectangle> temp = new List<Rectangle>();
        bool removePlayer = true;
        bool removeIA = true;

        bool keyDown = false;

        float T1 = 16000f;
        SpriteFont sprite;

        private Song gameTheme;

        bool firstPlayer = true;

        static public Sprite playerRight, playerLeft;
        private Sprite projectile, playerRightIA, playerLeftIA, winPlay;
        public static Sprite rainSprite;
        public static ParticleGenerator rain;

        GraphicsDevice graphics;

        Camera camera;

        Random rand;
        int r;

        List<string> nameList;
        ContentManager content;

        //Players lists
        List<Player> playerList = new List<Player>();
        List<IA> IAList = new List<IA>();

        //i = joueur, j = ia
        int i = 0, j = 0;

        bool goodPlayer = false;
        Vector2 positionGoodPlayer = new Vector2();

        //Backgrounds
        List<Sprite> _maps = new List<Sprite>();
        public static Sprite map;
        int nbr = 0;

        Levels.levels levels = new Levels.levels();
        public static int level = 0;
        bool alreadyPress = false;

        #region menuWin
        public static Button restart, leave, next, save;
        string restartFR = "Recommencer", restartEN = "Restart", leaveFR = "Quitter", leaveEN = "Leave", nextFR = "Suivant", nextEN = "Next", saveFR = "Sauvegarder", saveEN = "Save";
        SpriteFont title;
        #endregion
        #region Collisions
        static Rectangle pperso, pmap;
        Rectangle pperso_colgauche;
        Rectangle pperso_colbas2;
        Rectangle pperso_colbas;
        Rectangle pperso_coldroit;
        int temps = 0;
        #endregion

        #endregion

        public Play(Sprite map)
        {
            Collision.Collision.TerrainColor = new Color[map.Width * map.Height];
            map.Texture.GetData(Collision.Collision.TerrainColor);
            pmap = new Rectangle(0, 0, map.Width, map.Height);
        }

        public Play(List<Sprite> maps, Song newGameTheme, SoundEffect gunSound, int height, int width, ContentManager content, Camera camera, GraphicsDevice graphics)
        {
            this.content = content;
            //Font
            sprite = content.Load<SpriteFont>("Compteur");

            //Textures
            playerRight = new Sprite(content, 2);
            //playerRight.LoadContent("Textures/playerRight");
            playerRight.Load("../../../../Soutenance2Content/Textures/playerRight.png", graphics);
            playerLeft = new Sprite(content, 2);
            //playerLeft.LoadContent("Textures/playerLeft");
            playerLeft.Load("../../../../Soutenance2Content/Textures/playerLeft.png", graphics);
            playerLeftIA = new Sprite(content);
            playerLeftIA.LoadContent("Textures/playerLeftIA");
            playerRightIA = new Sprite(content);
            playerRightIA.LoadContent("Textures/playerRightIA");
            projectile = new Sprite(content);
            projectile.LoadContent("Weapons/projectile");
            rainSprite = new Sprite(content);
            rainSprite.LoadContent("Textures/rain");
            winPlay = new Sprite(content);
            winPlay.LoadContent("Textures/backgroundGame");

            #region MenuWin
            title = content.Load<SpriteFont>("Titles");
            if (Options._english)
            {
                restart = new Button(restartEN, title, graphics, width);
                restart.setPosition(new Vector2(-title.MeasureString(restartEN).X / 2, height / 2 - 200));
                leave = new Button(leaveEN, title, graphics, width);
                leave.setPosition(new Vector2(-title.MeasureString(leaveEN).X / 2, height / 2 - 50));
                next = new Button(nextEN, title, graphics, width);
                next.setPosition(new Vector2(-title.MeasureString(nextEN).X / 2, height / 2 - 150));
                save = new Button(saveEN, title, graphics, width);
                save.setPosition(new Vector2(-title.MeasureString(saveEN).X / 2, height / 2 - 100));
            }
            else
            {
                restart = new Button(restartFR, title, graphics, width);
                restart.setPosition(new Vector2(-title.MeasureString(restartFR).X / 2, height / 2 - 200));
                leave = new Button(leaveFR, title, graphics, width);
                leave.setPosition(new Vector2(-title.MeasureString(restartFR).X / 2, height / 2 - 50));
                next = new Button(nextFR, title, graphics, width);
                next.setPosition(new Vector2(-title.MeasureString(restartFR).X / 2, height / 2 - 150));
                save = new Button(saveFR, title, graphics, width);
                save.setPosition(new Vector2(-title.MeasureString(restartFR).X / 2, height / 2 - 100));
            }
            #endregion

            //Backgrounds
            this._maps = maps;

            //Taille ecran + position player
            widthScreen = width;
            heigthScreen = height;

            gameTheme = newGameTheme;

            //list name generated
            nameList = getName("nameIA.txt");

            //Classes
            //CreateListPlayers(projCollision, content, graphics, playerList, null, nameList, "Genius", playerLeft.Texture, playerRight.Texture, content.Load<SpriteFont>("name"), height, width, true, 3);
            //CreateListPlayers(projCollision, content, graphics, null, IAList, nameList, "Berseker", playerLeftIA.Texture, playerRightIA.Texture, content.Load<SpriteFont>("name"), height, width, false, 3);
            this.camera = camera;
            this.camera.centre = new Vector2(-width / 2, 0);

            rain = new ParticleGenerator(rainSprite.Texture, widthScreen / 2, 0.03f);
            this.graphics = graphics;

            rand = new Random();
            r = rand.Next(200, 600);



            //Levels
            levels.level00(ref projCollision, ref map, content, ref IAList, ref playerList, content.Load<SpriteFont>("name"), height, width, nameList, playerLeft, playerRight, playerLeftIA, playerRightIA, graphics);


            #region collisions
            Collision.Collision.TerrainColor = new Color[map.Width * map.Height];
            map.Texture.GetData(Collision.Collision.TerrainColor);

            pmap = new Rectangle(0, 0, map.Width, map.Height);
            pperso_colbas = new Rectangle(
                (int)(playerList[i].Position.X + 30),
                (int)(playerList[i].Position.Y + 5),
                (int)playerList[i].PositionRectangle.Width / 2,
                (int)playerList[i].PositionRectangle.Height);
            pperso_colbas2 = new Rectangle(
                (int)(playerList[i].Position.X + 30),
                (int)(playerList[i].Position.Y + 10),
                (int)playerList[i].PositionRectangle.Width / 2,
                (int)playerList[i].PositionRectangle.Height);

            pperso_colgauche = new Rectangle(
                (int)(playerList[i].Position.X + 15),
                (int)(playerList[i].Position.Y + 5),
                (int)playerList[i].PositionRectangle.Width / 3,
                (int)playerList[i].PositionRectangle.Height / 2 + 25);

            pperso_coldroit = new Rectangle(
                (int)(playerList[i].Position.X + 50),
                (int)(playerList[i].Position.Y + 5),
                (int)playerList[i].PositionRectangle.Width / 3,
                (int)playerList[i].PositionRectangle.Height / 2 + 25);
            #endregion
        }

        public void Update(GameTime gameTime, MouseState mouse)
        {
            alreadyPress = false;
            mouse = Mouse.GetState();
            //A commenter
            rain.Update(gameTime, graphics);
            if (Keyboard.GetState().IsKeyDown(Keys.T) && !alreadyPress)
            {
                //level++;
                //if (level > 2) level = 0;
                //initLevel(level);
                //Thread.Sleep(30);
                restart.isClicked = false;
                leave.isClicked = false;
                next.isClicked = false;
                save.isClicked = false;
                IAList = new List<IA>();
                alreadyPress = true;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Y) && !alreadyPress)
            {
                //level++;
                //if (level > 2) level = 0;
                //initLevel(level);
                //Thread.Sleep(30);
                restart.isClicked = false;
                leave.isClicked = false;
                next.isClicked = false;
                save.isClicked = false;
                playerList = new List<Player>();
                alreadyPress = true;
            }
            if (playerList.Count > 0 && IAList.Count > 0)
            {
                if (!keyDown && Keyboard.GetState().IsKeyDown(Keys.F))
                {
                    //weapons.Update(gameTime, mouse, /*playerList[i].Position,*/ playerList[i]);
                    keyDown = true;
                    Thread.Sleep(50);
                }
                if (keyDown && Keyboard.GetState().IsKeyUp(Keys.F))
                {
                    keyDown = false;
                }


                #region Compteur
                T1 -= gameTime.ElapsedGameTime.Milliseconds;
                if (T1 <= 0 && !(firstPlayer && playerList[i].isActive()))
                {
                    //Permet de changer le joueur dans la liste
                    if (firstPlayer)
                    {
                        playerList[i]._weapons.alreadyPress = false;
                        i++;
                        if (i == playerList.Count)
                            i = 0;
                    }

                    else
                    {
                        IAList[j].timeToShoot = 0;
                        IAList[j].coolDown = false;
                        j++;
                        if (j == IAList.Count)
                            j = 0;

                    }
                    while (j > IAList.Count - 1)
                        j--;
                    while (i > playerList.Count - 1)
                        i--;
                    firstPlayer = !firstPlayer;
                    r = rand.Next(200, 600);
                    T1 = 16000f;
                }

                #endregion

                #region Tour Joueur

                if (firstPlayer)
                {
                    /*if (temps < 4)
                {
                    temps++;
                }
                else
                {*/
                    pperso_colbas.X = (int)playerList[i].Position.X + 30;
                    pperso_colbas.Y = (int)playerList[i].Position.Y + 5;

                    pperso_colbas2.X = (int)playerList[i].Position.X + 30;
                    pperso_colbas2.Y = (int)playerList[i].Position.Y + 10;

                    pperso_colgauche.X = (int)playerList[i].Position.X + 15;
                    pperso_colgauche.Y = (int)playerList[i].Position.Y + 5;

                    pperso_coldroit.X = (int)(playerList[i].Position.X + 50);
                    pperso_coldroit.Y = (int)(playerList[i].Position.Y + 5);

                    playerList[i].col_bas2 = Collision.Collision.col_bas(
                        playerLeft.Texture,
                      playerList[i].PositionRectangle,
                        map.Texture,
                       new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                        pperso_colbas);

                    playerList[i].col_bas = Collision.Collision.col_bas(
                        playerLeft.Texture,
                        playerList[i].PositionRectangle,
                        map.Texture,
                        new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                        pperso_colbas);

                    playerList[i].col_gauche = Collision.Collision.col_droite(
                       playerLeft.Texture,
                       playerList[i].PositionRectangle,
                       map.Texture,
                       new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                       pperso_colgauche);

                    playerList[i].col_droite = Collision.Collision.col_droite(
                       playerLeft.Texture,
                       playerList[i].PositionRectangle,
                       map.Texture,
                       new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                       pperso_coldroit);
                    temps = 0;
                    //}
                    pperso = playerList[i].PositionRectangle;



                    playerList[i].Update(gameTime, mouse);

                    //maj camera
                    camera.Update(gameTime, playerList[i]._positionPlayer, playerRight.Width, widthScreen);

                    for (int k = 0; k < IAList.Count; k++)
                    {
                        IAList[k].fire = false;
                    }

                    goodPlayer = false;

                    //Permet de supprimer de la liste de rectangle le joueur actuel 
                    if (removePlayer)
                    {
                        UpdateProjCollision();
                        temp = new List<Rectangle>();
                        for (int k = 0; k < projCollision.Count; k++)
                        {
                            temp.Add(projCollision[k]);
                        }

                        temp.RemoveAt(i);
                        removePlayer = false;
                        removeIA = true;
                    }


                    #region nos regions ont du talent
                    //Collision rectangle
                    if (playerList[i]._weapons._tir)
                    {
                        T1 = T1 <= 5000 ? T1 : 5000;
                        //Collision projectile / joueurs
                        if (CollisionRectangles.collision(playerList[i]._weapons._position, temp))
                        {
                            playerList[i]._weapons._tir = false;
                            playerList[i]._weapons.touch = true;
                            playerList[i]._weapons._alreadyShoot = false;

                            for (int k = 0; k < IAList.Count; k++)
                            {
                                if (IAList[k].PositionRectangle.Intersects(playerList[i]._weapons._position))
                                {
                                    IAList[k].Life -= playerList[i]._weapons._dommage;
                                    Thread.Sleep(30);

                                    //si vie <= 0 on retire de la liste le joueur
                                    if (IAList[k].Life <= 0)
                                    {
                                        if (k < j) j--;
                                        IAList.RemoveAt(k);
                                        projCollision.RemoveAt(playerList.Count + k);
                                        removePlayer = true;
                                    }

                                }
                            }
                            for (int k = 0; k < playerList.Count; k++)
                            {

                                if (playerList[k].PositionRectangle.Intersects(playerList[i]._weapons._position))
                                {
                                    playerList[k].Life -= playerList[i]._weapons._dommage;
                                    Thread.Sleep(30);
                                    //si vie <= 0 on retire de la liste le joueur
                                    if (playerList[k].Life <= 0)
                                    {
                                        if (k < i) i--;
                                        playerList.RemoveAt(k);
                                        projCollision.RemoveAt(k);
                                        removePlayer = true;
                                    }

                                }
                            }
                        }

                    }
                    #endregion

                }
                #endregion

                #region Tour IA
                else
                {
                    /*if (temps < 4)
                {
                    temps++;
                }
                else
                {*/
                    pperso_colbas.X = (int)IAList[j].Position.X + 30;
                    pperso_colbas.Y = (int)IAList[j].Position.Y + 5;

                    pperso_colbas2.X = (int)IAList[j].Position.X + 30;
                    pperso_colbas2.Y = (int)IAList[j].Position.Y + 10;

                    pperso_colgauche.X = (int)IAList[j].Position.X + 15;
                    pperso_colgauche.Y = (int)IAList[j].Position.Y + 5;

                    pperso_coldroit.X = (int)(IAList[j].Position.X + 50);
                    pperso_coldroit.Y = (int)(IAList[j].Position.Y + 5);

                    IAList[j].col_bas2 = Collision.Collision.col_bas(
                        playerLeft.Texture,
                      playerList[i].PositionRectangle,
                        map.Texture,
                       new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                        pperso_colbas);

                    IAList[j].col_bas = Collision.Collision.col_bas(
                        playerLeft.Texture,
                        playerList[i].PositionRectangle,
                        map.Texture,
                        new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                        pperso_colbas);

                    IAList[j].col_gauche = Collision.Collision.col_droite(
                       playerLeft.Texture,
                       playerList[i].PositionRectangle,
                       map.Texture,
                       new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                       pperso_colgauche);

                    IAList[j].col_droite = Collision.Collision.col_droite(
                       playerLeft.Texture,
                       playerList[i].PositionRectangle,
                       map.Texture,
                       new Rectangle(-widthScreen / 2, 0, pmap.Width, pmap.Height),
                       pperso_coldroit);
                    temps = 0;
                    //}
                    pperso = playerList[i].PositionRectangle;


                    //Selection du joueur le plus proche
                    if (!goodPlayer)
                    {
                        if (level != 2)
                            positionGoodPlayer = IAList[j].GetBetterPlayerBasic(playerList);
                        else
                            positionGoodPlayer = IAList[j].GetBetterPlayerAdvanced(playerList);
                        goodPlayer = true;
                    }

                    IAList[j].Update(positionGoodPlayer, r, gameTime);
                    camera.Update(gameTime, IAList[j].Position, playerRight.Width, widthScreen);


                    //Permet de supprimer de la liste de rectangle le joueur actuel
                    if (removeIA)
                    {
                        UpdateProjCollision();
                        temp = new List<Rectangle>();
                        for (int k = 0; k < projCollision.Count; k++)
                        {
                            temp.Add(projCollision[k]);
                        }

                        temp.RemoveAt(playerList.Count + j);
                        removeIA = false;
                        removePlayer = true;
                    }

                    #region nos regions ont du talent
                    //Collision par rectangle

                    if (IAList[j]._weapons._tir || IAList[j]._weapons.troll)
                    {
                        T1 = T1 <= 5000 ? T1 : 5000;
                        //Collision projectile / joueurs
                        if (level == 2)
                        {
                            nbr++;
                            if (nbr >= 30)
                            {
                                playerList[1].Life = 70;
                                playerList[2].Life = 70;
                            }

                            IAList[j]._weapons.troll = false;
                        }
                        else
                        {
                            if (CollisionRectangles.collision(IAList[j]._weapons._position, temp))
                            {
                                IAList[j]._weapons.touch = true;
                            }

                            if (IAList[j]._weapons.touch)
                            {
                                for (int k = 0; k < IAList.Count; k++)
                                {
                                    if (IAList[k].PositionRectangle.Intersects(IAList[j]._weapons._position))
                                    {
                                        IAList[k].Life -= IAList[j]._weapons._dommage;

                                        //si vie <= 0 on retire de la liste le joueur
                                        if (IAList[k].Life <= 0)
                                        {
                                            if (k < j) j--;
                                            IAList.RemoveAt(k);
                                            projCollision.RemoveAt(playerList.Count + k);
                                            removeIA = true;
                                        }

                                    }
                                }
                                for (int k = 0; k < playerList.Count; k++)
                                {

                                    if (playerList[k].PositionRectangle.Intersects(IAList[j]._weapons._position))
                                    {
                                        playerList[k].Life -= IAList[j]._weapons._dommage;

                                        //si vie <= 0 on retire de la liste le joueur
                                        if (playerList[k].Life <= 0)
                                        {
                                            if (k < i) i--;
                                            playerList.RemoveAt(k);
                                            projCollision.RemoveAt(k);
                                            removeIA = true;
                                        }

                                    }
                                }

                            }
                        }
                    #endregion

                    }
                #endregion
                }
            }
            else
            {
                MenuChoiceUpdate(mouse);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(map.Texture, new Vector2(-widthScreen / 2, 0), Color.White);

            //A changer si on loupe l'avion 
            float alphaByOmegaMod3EtCestLeDrame = T1 / 1000 <= 0 ? 0 : T1 / 1000;

            /*if (!keyDown)
            {
              weapons.Draw(spriteBatch);
            }*/

            rain.Draw(spriteBatch);


            if (playerList.Count != 0 && IAList.Count != 0)
            {
                for (int k = 0; k < playerList.Count; k++)
                {
                    playerList[k].DrawWeapons(spriteBatch);
                }

                for (int k = 0; k < IAList.Count; k++)
                {
                    IAList[k].Draw(spriteBatch);
                }
                spriteBatch.DrawString(sprite, ((int)(alphaByOmegaMod3EtCestLeDrame)).ToString(), new Vector2(widthScreen / 2 - camera.transform.M41, 200), Color.White);
            }
            else if (IAList.Count == 0 || playerList.Count == 0)
                MenuChoiceDraw(spriteBatch);
        }

        #region calculs
        private void initLevel(int level)
        {
            i = 0;
            j = 0;
            T1 = 16000f;
            firstPlayer = true;
            nameList = getName("nameIA.txt");
            IAList = new List<IA>();
            playerList = new List<Player>();
            switch (level)
            {
                case 0:
                    levels.level00(ref projCollision, ref map, content, ref IAList, ref playerList, content.Load<SpriteFont>("name"), heigthScreen, widthScreen, nameList, playerLeft, playerRight, playerLeftIA, playerRightIA, graphics);
                    break;

                case 1:
                    levels.level01(ref projCollision, ref map, content, ref IAList, ref playerList, content.Load<SpriteFont>("name"), heigthScreen, widthScreen, nameList, playerLeft, playerRight, playerLeftIA, playerRightIA, graphics);
                    break;

                case 2:
                    levels.level02(ref projCollision, ref map, content, ref IAList, ref playerList, content.Load<SpriteFont>("name"), heigthScreen, widthScreen, nameList, playerLeft, playerRight, playerLeftIA, playerRightIA, graphics);
                    break;

                default: break;
            }
        }

        private void UpdateProjCollision()
        {
            projCollision = new List<Rectangle>();
            for (int i = 0; i < playerList.Count; i++)
            {
                projCollision.Add(playerList[i].PositionRectangle);
            }
            for (int i = 0; i < IAList.Count; i++)
            {
                projCollision.Add(IAList[i].PositionRectangle);
            }
        }

        //Permet de creer une liste de noms
        public List<string> getName(string path)
        {
            StreamReader sr = new StreamReader(path);
            string name;
            List<string> listName = new List<string>();

            while ((name = sr.ReadLine()) != null)
            {
                listName.Add(name);
            }
            sr.Close();

            return listName;
        }

        //Creation de liste de joueurs/IA
        public void CreateListPlayers(ref List<Rectangle> newProjCollision, ContentManager content, GraphicsDevice graphics, List<Player> list, List<IA> listIA, List<string> nickname, string teamName, Texture2D Left, Texture2D Right, SpriteFont font, int height, int width, bool player, int nbr, bool randPos)
        {
            if (randPos)
            {
                Random rand;
                Random randName = new Random();
                if (player)
                {
                    for (int i = 0; i < nbr; i++)
                    {
                        rand = new Random();
                        int random = randName.Next(0, nickname.Count - 1);
                        int randTemp = rand.Next(-width / 2 + playerLeft.Width, width / 2 - playerLeft.Width / 4);
                        foreach (var item in newProjCollision)
                        {
                            while (new Rectangle(randTemp, item.Y, item.Width, item.Height).Intersects(item))
                            {
                                randTemp = rand.Next(-width / 2 + playerLeft.Width, width / 2 - playerLeft.Width / 4);
                            }

                        }
                        list.Add(new Player(content, graphics, Left, Right, font, randTemp, teamName, nickname[random], (height - playerLeft.Height / 4), width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter));
                        list[i].Init(map.Texture);
                        newProjCollision.Add(list[i].PositionRectangle);
                        nickname.RemoveAt(random);
                        Thread.Sleep(20);
                    }
                }
                else
                {
                    for (int i = 0; i < nbr; i++)
                    {
                        rand = new Random();
                        int random = randName.Next(0, nickname.Count - 1);
                        int randTemp = rand.Next(-width / 2 + playerLeft.Width, width / 2 - playerLeft.Width / 4);
                        foreach (var item in newProjCollision)
                        {
                            while (new Rectangle(randTemp, item.Y, item.Width, item.Height).Intersects(item))
                            {
                                randTemp = rand.Next(-width / 2 + playerLeft.Width, width / 2 - playerLeft.Width / 4);
                            }

                        }
                        listIA.Add(new IA(content, graphics, widthScreen, height, Left, Right, randTemp, teamName, nickname[random], font));
                        listIA[i].Init(map.Texture);
                        newProjCollision.Add(listIA[i].PositionRectangle);
                        nickname.RemoveAt(random);
                        Thread.Sleep(20);
                    }
                }
            }
            else
            {
                Random rand;
                Random randName = new Random();
                if (player)
                {
                    for (int i = 0; i < nbr; i++)
                    {
                        rand = new Random();
                        int random = randName.Next(0, nickname.Count - 1);
                        int randTemp = rand.Next(-width / 2 + playerLeft.Width, width / 2 - playerLeft.Width / 4);
                        foreach (var item in newProjCollision)
                        {
                            while (new Rectangle(randTemp, item.Y, item.Width, item.Height).Intersects(item))
                            {
                                randTemp = rand.Next(-width / 2 + playerLeft.Width, width / 2 - playerLeft.Width / 4);
                            }

                        }
                        if (i == 0)
                            list.Add(new Player(content, graphics, Left, Right, font, -347, teamName, nickname[random], (height - playerLeft.Height / 4), width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter));
                        else if (i == 1)
                            list.Add(new Player(content, graphics, Left, Right, font, 200, teamName, nickname[random], (height - playerLeft.Height / 4), width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter));
                        else if (i == 2)
                            list.Add(new Player(content, graphics, Left, Right, font, 287, teamName, nickname[random], (height - playerLeft.Height / 4), width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter));
                        list[i].Init(map.Texture);
                        newProjCollision.Add(list[i].PositionRectangle);
                        nickname.RemoveAt(random);
                        Thread.Sleep(20);
                    }
                }
                else
                {
                    for (int i = 0; i < nbr; i++)
                    {
                        rand = new Random();
                        int random = randName.Next(0, nickname.Count - 1);
                        int randTemp = rand.Next(0, width / 2 - playerLeft.Width / 4);
                        foreach (var item in newProjCollision)
                        {
                            while (new Rectangle(randTemp, item.Y, item.Width, item.Height).Intersects(item))
                            {
                                randTemp = rand.Next(-width / 2 + playerLeft.Width, width / 2 - playerLeft.Width / 4);
                            }

                        }
                        listIA.Add(new IA(content, graphics, widthScreen, height, Left, Right, 400 + 99 * i, teamName, nickname[random], font));
                        listIA[i].Init(map.Texture);
                        newProjCollision.Add(listIA[i].PositionRectangle);
                        nickname.RemoveAt(random);
                        Thread.Sleep(20);
                    }
                }
            }
        }
        #endregion

        #region MenuChoice
        private void MenuChoiceUpdate(MouseState mouse)
        {
            mouse = Mouse.GetState();
            if (restart.isClicked)
            {
                if (level == 2 && IAList.Count == 0)
                {
                    level = 0;
                    initLevel(level);
                }
                else
                    initLevel(level);
                restart.isClicked = false;
            }
            if (leave.isClicked)
            {
                j = 0;
                leave.isClicked = false;
            }
            if (!(level == 2 && IAList.Count == 0))
                if (next.isClicked)
                {
                    level++;
                    if (level > 2)
                    {
                        level = 0;
                    }
                    initLevel(level);
                    next.isClicked = false;
                }
            restart.UpdateStringPauseCamera(mouse);
            leave.UpdateStringPauseCamera(mouse);
            if (!(level == 2 && IAList.Count == 0) && playerList.Count != 0)
                next.UpdateStringPauseCamera(mouse);

        }

        private void MenuChoiceDraw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(winPlay.Texture, new Rectangle(-400 + widthScreen / 2 - Camera.move, heigthScreen / 2 - 300, 800, 600), new Color(100, 100, 100, 100));
            restart.DrawString(spriteBatch);
            leave.DrawString(spriteBatch);
            if (!(level == 2 && IAList.Count == 0) && playerList.Count != 0)
                next.DrawString(spriteBatch);
            else if (IAList.Count == 0)
            {
                if (Options._english)
                {
                    spriteBatch.DrawString(title, "It's the end, well played !", new Vector2(-160 + widthScreen / 2 - Camera.move), Color.White);
                }
                else
                {
                    spriteBatch.DrawString(title, "C'est la fin, bien joue !", new Vector2(-160 + widthScreen / 2 - Camera.move, 100), Color.White);
                }
            }
        }
        #endregion
    }
}