﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Soutenance2
{
    class Options
    {
        Button upVolumeButton, downVolumeButton, backButton, length800Button, length1280Button, length1680Button, languageButton, personnalisationButton, englishButton, frenchButton;
        string backStrFR, backStrEN, length800Str, length1280Str, length1680Str, customStrFR, customStrEN, creditStr;

        int heightScreen = 900, widthScreen = 1600;

        GraphicsDeviceManager manageGraphics;

        float volume = 1.0f;

        SpriteFont font;
        Sprite background;

        public static bool _english = false;

        Personalisation personnalisation = new Personalisation();

        public enum GameState
        {
            optionsMenu,
            gameMenu,
            mainMenu
        }

        public GameState currentGameState = GameState.optionsMenu;

        public bool isClicked = false;

        public Options(ContentManager content, int widthScreen, int heightScreen, GraphicsDeviceManager manageGraphics)
        {
            font = content.Load<SpriteFont>("Titles");

            background = new Sprite(content);
            background.LoadContent("Textures/backgroundOptions");

            backStrFR = "Retour";
            backStrEN = "Back";
            length800Str = "800 X 600";
            length1280Str = "1280 X 800";
            length1680Str = "1600 X 900";
            customStrFR = "Personnalisation";
            customStrEN = "Customization";

            this.manageGraphics = manageGraphics;

            upVolumeButton = new Button(content.Load<Texture2D>("Textures/upVolume"), manageGraphics.GraphicsDevice, 50, 50);
            downVolumeButton = new Button(content.Load<Texture2D>("Textures/downVolume"), manageGraphics.GraphicsDevice, 50, 50);
            backButton = new Button(backStrFR, font, manageGraphics.GraphicsDevice);
            length800Button = new Button(length800Str, font, manageGraphics.GraphicsDevice);
            length1280Button = new Button(length1280Str, font, manageGraphics.GraphicsDevice);
            length1680Button = new Button(length1680Str, font, manageGraphics.GraphicsDevice);
            personnalisationButton = new Button(customStrFR, font, manageGraphics.GraphicsDevice);
            englishButton = new Button(content.Load<Texture2D>("Buttons/English"), manageGraphics.GraphicsDevice);
            frenchButton = new Button(content.Load<Texture2D>("Buttons/French"), manageGraphics.GraphicsDevice);

            setPosition(heightScreen);

            creditStr = "Cree par \nCorentin Meschi\nFabien Martnez\nHossein Vakili";

        }

        public float ratio(float ratio, int nbr)
        {
            return (1 - ratio / 100) * nbr;
        }

        public void setPosition(int heightScreen)
        {
            upVolumeButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(55f, heightScreen)));
            downVolumeButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(60, heightScreen)));
            backButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(85, heightScreen)));
            length800Button.setPosition(new Vector2(100, heightScreen / 2 - ratio(65, heightScreen)));
            length1280Button.setPosition(new Vector2(100, heightScreen / 2 - ratio(70, heightScreen)));
            length1680Button.setPosition(new Vector2(100, heightScreen / 2 - ratio(75, heightScreen)));
            personnalisationButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(80, heightScreen)));
            frenchButton.setPosition(new Vector2(widthScreen / 2 - frenchButton._size.X, 100));
            englishButton.setPosition(new Vector2(widthScreen / 2, 100));
        }

        public void Update(MouseState mouse)
        {

            isClicked = false;

            if (upVolumeButton.isClicked && volume < 1.0f)
            {
                volume += 0.1f;
                System.Threading.Thread.Sleep(100);
            }

            if (downVolumeButton.isClicked && volume > 0.0f)
            {
                volume -= 0.1f;
                System.Threading.Thread.Sleep(100);
            }

            MediaPlayer.Volume = volume;

            if (backButton.isClicked)
            {
                isClicked = true;
                System.Threading.Thread.Sleep(42);
            }
            #region taille
            if (length800Button.isClicked)
            {
                heightScreen = 600;
                widthScreen = 800;
                manageGraphics.PreferredBackBufferHeight = heightScreen;
                manageGraphics.PreferredBackBufferWidth = widthScreen;
                manageGraphics.ApplyChanges();
                System.Threading.Thread.Sleep(100);
                setPosition(heightScreen);
            }

            if (length1280Button.isClicked)
            {
                heightScreen = 800;
                widthScreen = 1280;
                manageGraphics.PreferredBackBufferHeight = heightScreen;
                manageGraphics.PreferredBackBufferWidth = widthScreen;
                manageGraphics.ApplyChanges();
                System.Threading.Thread.Sleep(100);
                setPosition(heightScreen);
            }
            if (length1680Button.isClicked)
            {
                heightScreen = 900;
                widthScreen = 1600;
                manageGraphics.PreferredBackBufferHeight = heightScreen;
                manageGraphics.PreferredBackBufferWidth = widthScreen;
                manageGraphics.ApplyChanges();
                System.Threading.Thread.Sleep(100);
                setPosition(heightScreen);
            }
            #endregion
            if (personnalisationButton.isClicked)
            {
                try
                {
                    personnalisation.Show();
                }
                catch
                {
                    personnalisation = new Personalisation();
                    personnalisation.Show();
                }
            }
            if (!_english && englishButton.isClicked)
            {
                _english = true;
                creditStr = "Made by \nCorentin Meschi\nFabien Martnez\nHossein Vakili";
                personnalisationButton = new Button(customStrEN, font, manageGraphics.GraphicsDevice);
                personnalisationButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(80, heightScreen)));
                backButton = new Button(backStrEN, font, manageGraphics.GraphicsDevice);
                backButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(85, heightScreen)));
                englishButton.isClicked = false;
            }
            if (_english && frenchButton.isClicked)
            {
                _english = false;
                creditStr = "Cree par \nCorentin Meschi\nFabien Martnez\nHossein Vakili";
                personnalisationButton = new Button(customStrFR, font, manageGraphics.GraphicsDevice);
                personnalisationButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(80, heightScreen)));
                backButton = new Button(backStrFR, font, manageGraphics.GraphicsDevice);
                backButton.setPosition(new Vector2(100, heightScreen / 2 - ratio(85, heightScreen)));
                frenchButton.isClicked = false;
            }

            upVolumeButton.Update(mouse);
            downVolumeButton.Update(mouse);
            backButton.UpdateString(mouse);
            length800Button.UpdateString(mouse);
            length1280Button.UpdateString(mouse);
            length1680Button.UpdateString(mouse);
            personnalisationButton.UpdateString(mouse);
            if (!_english) englishButton.Update(mouse);
            else    frenchButton.Update(mouse);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background.Texture, new Rectangle(0, 0, widthScreen, heightScreen), Color.White);
            upVolumeButton.Draw(spriteBatch);
            downVolumeButton.Draw(spriteBatch);
            backButton.DrawString(spriteBatch);
            length800Button.DrawString(spriteBatch);
            length1280Button.DrawString(spriteBatch);
            length1680Button.DrawString(spriteBatch);
            spriteBatch.DrawString(font, creditStr, new Vector2(1200, 100), Color.White);
            personnalisationButton.DrawString(spriteBatch);
            if (!_english) englishButton.Draw(spriteBatch);
            else    frenchButton.Draw(spriteBatch);
            
        }

    }
}
