﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Net;
using System.IO;

namespace Soutenance2
{
    class MultiPlayers
    {
        #region Viariables globales
        Song ronfle;

        Sprite playerR, playerL, playerRAdv, playerLAdv;
        //Rectangle positionPlayer1, positionPlayer2, positionAdv1, positionAdv2;
        Texture2D background, projectile, rainSprite;
        Gravity gravity;
        //Projectile _projectile;
        int direction1 = 0, direction2 = 0, i = 0, j = 0, _width, _height;


        Thread thread;
        SpriteFont font, fontName;
        float playingTime = 10000, timeChrono = 10f, waitingTime = 0;
        string drawMenu = "Rien a signaler, mon general!", personnalString = "Rien a signaler", player1Name = "Captain Chaussette", player2Name = "Maister killer";

        Player player1, player2, player1Adv, player2Adv;

        Network network = new Network();

        //AnimatedSprite animated, animated2, animatedAdv1, animatedAdv2;

        //Particules
        ParticleGenerator rain;
        GraphicsDevice _graphics;
        ContentManager _content;

        WeaponsClass weapon;

        public static List<Player> MyTeam, AdvTeam;

        int cpt = 0;
        //Buttons and others
        Button Join, Create, Next, Prec;

        #region Collisions
        Rectangle pperso_colgauche, pmap;
        Rectangle pperso_colbas2;
        Rectangle pperso_colbas;
        Rectangle pperso_coldroit;
        Color[] persoColor, terrainColor;
        #endregion
        #endregion

        public MultiPlayers(ContentManager content, int height, SoundEffect gunSound, int width, GraphicsDevice graphics)
        {
            network.isConnected = false;
            this._content = content;
            //son
            ronfle = content.Load<Song>("Songs/ronflement");

            //particules
            rainSprite = content.Load<Texture2D>("Textures/rain");
            rain = new ParticleGenerator(rainSprite, width, 0.03f);
            this._graphics = graphics;

            //Buttons
            if (Options._english)
            {
                Join = new Button(content.Load<Texture2D>("Buttons/JoinEng"), graphics);
                Create = new Button(content.Load<Texture2D>("Buttons/CreateEng"), graphics);
            }
            else
            {
                Join = new Button(content.Load<Texture2D>("Buttons/Join"), graphics);
                Create = new Button(content.Load<Texture2D>("Buttons/Create"), graphics);
            }
            Next = new Button(content.Load<Texture2D>("Buttons/next"), graphics);
            Prec = new Button(content.Load<Texture2D>("Buttons/prec"), graphics);
            Join.setPosition(new Vector2(200, 400));
            Create.setPosition(new Vector2(200, 200));
            Next.setPosition(new Vector2(450, 200));
            Prec.setPosition(new Vector2(350, 200));

            //Ecritures, font
            font = content.Load<SpriteFont>("Compteur");
            fontName = content.Load<SpriteFont>("name");

            //Sprites
            projectile = content.Load<Texture2D>("Weapons/projectile");
            playerR = new Sprite(content);
            playerR.LoadContent("Textures/playerRight");
            playerL = new Sprite(content);
            playerL.LoadContent("Textures/playerLeft");
            playerRAdv = new Sprite(content);
            playerRAdv.LoadContent("Textures/playerRightIA");
            playerLAdv = new Sprite(content);
            playerLAdv.LoadContent("Textures/playerLeftIA");

            background = content.Load<Texture2D>("Textures/map1");
            gravity = new Gravity(height - playerR.Height / 4);

            //Players
            if (File.Exists("Names.txt"))
            {
                StreamReader reader = new StreamReader("Names.txt");
                network.teamNameUs = reader.ReadLine();
                player1Name = reader.ReadLine();
                player2Name = reader.ReadLine();
                reader.Close();
            }
            _width = width;
            _height = height;

            //Langue
            if (Options._english)
            {
                drawMenu = "Nothing to do !";
            }
            Collision.Collision.PersoColor = new Color[playerR.Width * playerR.Height];
            terrainColor = new Color[background.Height * background.Width];
            playerR.Texture.GetData<Color>(Collision.Collision.PersoColor);
            background.GetData<Color>(terrainColor);
        }

        public void Initialise()
        {
            network.launched = true;
            network._moveX = 0;
            network._moveXPlayer2 = 500;
            network._continueGame = true;
            network._turnToPlayer1 = false;
            //Thread threadLauncher = new Thread(network.TryConnectionTCP);
            //threadLauncher.Start();
            //initPlayers(new List<Player>() { player1, player2 }, new List<Player>() { player1Adv, player2Adv });
        }

        public void Close()
        {
            if (!network.isConnected)
                MediaPlayer.Stop();
            Console.WriteLine("Ligne LAST");
            if (!network._continueGame)
            {
                network.isConnected = true;
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 2424));
                socket.Close();
            }

            network._continueGame = false;
            network.Close();
        }

        public void Update(GameTime gameTime, MouseState mouse)
        {
            mouse = Mouse.GetState();
            direction1 = direction2 = 0;
            #region initialisation
            if (network.initialize)
            {
                background = _content.Load<Texture2D>("Textures/map" + network.b);
                MediaPlayer.Stop();
                /*if (!network._isPlayer1)
                    j = 1;*/
                player1 = new Player(_content, _graphics, playerL.Texture, playerR.Texture, fontName, 120, network.teamNameUs, player1Name, _height - playerR.Height / 4, _width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter, 42);
                player2 = new Player(_content, _graphics, playerL.Texture, playerR.Texture, fontName, 600, network.teamNameUs, player2Name, _height - playerR.Height / 4, _width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter, 42);
                player1Adv = new Player(_content, _graphics, playerLAdv.Texture, playerRAdv.Texture, fontName, 300, network.teamNameAdv, "Adios", _height - playerRAdv.Height / 4, _width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter, 42);
                player2Adv = new Player(_content, _graphics, playerLAdv.Texture, playerRAdv.Texture, fontName, 800, network.teamNameAdv, "GnarkGnark", _height - playerRAdv.Height / 4, _width, Keys.Left, Keys.Right, Keys.Space, Keys.Enter, 42);

                MyTeam = new List<Player>() { player1, player2 };
                AdvTeam = new List<Player>() { player1Adv, player2Adv };
                if (!network._isPlayer1)
                {
                    float pos1 = player1._positionPlayer.X, pos2 = player2._positionPlayer.X;
                    player1._positionPlayer.X = player1Adv._positionPlayer.X;
                    player2._positionPlayer.X = player2Adv._positionPlayer.X;
                    player1Adv._positionPlayer.X = pos2;
                    player2Adv._positionPlayer.X = pos1;
                    AdvTeam = new List<Player>() { player1, player2 };
                    MyTeam = new List<Player>() { player1Adv, player2Adv };
                }
                network.initialize = false;
                #region collisions
                Collision.Collision.TerrainColor = new Color[background.Width * background.Height];
                background.GetData(Collision.Collision.TerrainColor);
                pmap = new Rectangle(0, 0, background.Width, background.Height);
                pperso_colbas = new Rectangle(
                    (int)(MyTeam[i].Position.X + 30),
                    (int)(MyTeam[i].Position.Y + 5),
                    (int)MyTeam[i].PositionRectangle.Width / 2,
                    (int)MyTeam[i].PositionRectangle.Height);
                pperso_colbas2 = new Rectangle(
                    (int)(MyTeam[i].Position.X + 30),
                    (int)(MyTeam[i].Position.Y + 10),
                    (int)MyTeam[i].PositionRectangle.Width / 2,
                    (int)MyTeam[i].PositionRectangle.Height);

                pperso_colgauche = new Rectangle(
                    (int)(MyTeam[i].Position.X + 15),
                    (int)(MyTeam[i].Position.Y + 5),
                    (int)MyTeam[i].PositionRectangle.Width / 3,
                    (int)MyTeam[i].PositionRectangle.Height / 2 + 25);

                pperso_coldroit = new Rectangle(
                    (int)(MyTeam[i].Position.X + 50),
                    (int)(MyTeam[i].Position.Y + 5),
                    (int)MyTeam[i].PositionRectangle.Width / 3,
                    (int)MyTeam[i].PositionRectangle.Height / 2 + 25);
                #endregion
                player1.InitMulti(background);
                player2.InitMulti(background);
                player1Adv.InitMulti(background);
                player2Adv.InitMulti(background);
            }
            #endregion

            #region Buttons
            if (!network.connected)
            {
                //rejoind le serveur
                if (Join.isClicked && !network.launched)
                {
                    MyTeam = new List<Player>() { player2, player1 };
                    AdvTeam = new List<Player>() { player1Adv, player2Adv };
                    network.timer = 10000;
                    Join.isClicked = false;
                    Initialise();
                    thread = new Thread(network.ClientUDP);
                    thread.Start();
                    if (Options._english)
                        personnalString = "Search servers..";
                    else
                        personnalString = "Recherche de serveurs..";
                }

                //creer le serveur
                if (Create.isClicked && !network.launched)
                {
                    network.timer = 15000;
                    Create.isClicked = false;
                    Initialise();
                    thread = new Thread(network.ServerUDP);
                    thread.Start();
                    MediaPlayer.Play(ronfle);
                    if (Options._english)
                        personnalString = "Waiting client..";
                    else
                        personnalString = "En attente de client..";
                }
                if (Next.isClicked && !network.launched && cpt <= 0)
                {
                    if (network.b < 3) network.b++;
                    background = _content.Load<Texture2D>("Textures/map" + network.b);
                    cpt = 10;
                }
                if (Prec.isClicked && !network.launched && cpt <= 0)
                {
                    if (network.b > 1) network.b--;
                    background = _content.Load<Texture2D>("Textures/map" + network.b);
                    cpt = 10;
                }
                cpt--;
                //Maj Bouttons
                if (!network.launched)
                {
                    Join.Update(mouse);
                    Create.Update(mouse);
                    Next.Update(mouse);
                    Prec.Update(mouse);
                }
                else
                {
                    //Decrementation du temps
                    network.timer -= gameTime.ElapsedGameTime.Milliseconds;
                    if (Options._english)
                        drawMenu = personnalString + "\nResting time : " + (network.timer / 1000);
                    else
                        drawMenu = personnalString + "\nTemps restant : " + (network.timer / 1000);
                    //Fermeture du thread Join
                    if (network.timer < 0)
                    {
                        if (!network._isPlayer1)
                        {
                            try
                            {
                                Socket clientTmp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                                IPEndPoint endPointUdp = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 2424);
                                clientTmp.SendTo(new byte[0], endPointUdp);
                                clientTmp.Close();
                            }
                            catch { }
                        }
                        if (Options._english)
                            drawMenu = "Connection fail";
                        else
                            drawMenu = "Connexion echouee";
                    }
                }
            }
            #endregion

            #region Si on est connecte
            if (network.connected)
            {

                rain.Update(gameTime, _graphics);
                //Joueur 1
                if (network._isPlayer1)
                {
                    #region Au joueur 1 de jouer
                    if (network._turnToPlayer1)
                    {
                        waitingTime = 0f;
                        //Collisions du perso
                        pperso_colbas.X = (int)MyTeam[i].Position.X + 30;
                        pperso_colbas.Y = (int)MyTeam[i].Position.Y + 5;

                        pperso_colbas2.X = (int)MyTeam[i].Position.X + 30;
                        pperso_colbas2.Y = (int)MyTeam[i].Position.Y + 10;

                        pperso_colgauche.X = (int)MyTeam[i].Position.X + 15;
                        pperso_colgauche.Y = (int)MyTeam[i].Position.Y + 5;

                        pperso_coldroit.X = (int)(MyTeam[i].Position.X + 50);
                        pperso_coldroit.Y = (int)(MyTeam[i].Position.Y + 5);

                        MyTeam[i].col_bas2 = Collision.Collision.col_bas(
                            playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_colbas);

                        MyTeam[i].col_bas = Collision.Collision.col_bas(
                            playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_colbas);

                        MyTeam[i].col_gauche = Collision.Collision.col_droite(
                           playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_colgauche);

                        MyTeam[i].col_droite = Collision.Collision.col_droite(
                           playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_coldroit);

                        //Update mes persos
                        MyTeam[i].UpdateMulti(gameTime, mouse, network);

                        #region Collision Armes
                        if (MyTeam[i]._weapons._tir)
                        {
                            timeChrono = timeChrono <= 3f ? timeChrono : 3f;
                            if (CollisionRectangles.collision(MyTeam[i]._weapons._position, rectList(MyTeam, AdvTeam, i, true)))
                            {
                                MyTeam[i]._weapons._tir = false;
                                MyTeam[i]._weapons.touch = true;
                                MyTeam[i]._weapons._alreadyShoot = false;

                                for (int k = 0; k < AdvTeam.Count; k++)
                                {
                                    if (AdvTeam[k].PositionRectangle.Intersects(MyTeam[i]._weapons._position))
                                    {
                                        AdvTeam[k].Life -= MyTeam[i]._weapons._dommage;
                                        break;
                                    }
                                }
                                for (int k = 0; k < MyTeam.Count; k++)
                                {

                                    if (MyTeam[k].PositionRectangle.Intersects(MyTeam[i]._weapons._position))
                                    {
                                        MyTeam[k].Life -= MyTeam[i]._weapons._dommage;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        //Envoi des donnees J1
                        timeChrono -= gameTime.ElapsedGameTime.Milliseconds;
                        playingTime -= gameTime.ElapsedGameTime.Milliseconds;
                        if (timeChrono < 0)
                        {
                            timeChrono = 10f;
                            if (playingTime <= 0 && !MyTeam[i].isActive(network))
                            {
                                network._alreadyShoot = false;
                                network._stillSamePlayer = false;
                                playingTime = 15000f;
                                network._turnToPlayer1 = false;
                                if (i < MyTeam.Count - 1)
                                    i += 1;
                                else
                                    i = 0;
                                if (j < AdvTeam.Count - 1)
                                    j += 1;
                                else
                                    j = 0;
                                if (network._continueGame)
                                    network.threadLaucher();
                            }
                            network.SenderTCP((int)MyTeam[i]._positionPlayer.X);
                            network._stillSamePlayer = true;
                        }
                    }
                    #endregion
                    #region Reception => au joueur 2 de jouer
                    else
                    {
                        //Collisions
                        pperso_colbas.X = (int)AdvTeam[j].Position.X + 30;
                        pperso_colbas.Y = (int)AdvTeam[j].Position.Y + 5;

                        pperso_colbas2.X = (int)AdvTeam[j].Position.X + 30;
                        pperso_colbas2.Y = (int)AdvTeam[j].Position.Y + 10;

                        pperso_colgauche.X = (int)AdvTeam[j].Position.X + 15;
                        pperso_colgauche.Y = (int)AdvTeam[j].Position.Y + 5;

                        pperso_coldroit.X = (int)(AdvTeam[j].Position.X + 50);
                        pperso_coldroit.Y = (int)(AdvTeam[j].Position.Y + 5);

                        AdvTeam[j].col_bas2 = Collision.Collision.col_bas(
                            playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_colbas);

                        AdvTeam[j].col_bas = Collision.Collision.col_bas(
                            playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_colbas);

                        AdvTeam[j].col_gauche = Collision.Collision.col_droite(
                           playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_colgauche);

                        AdvTeam[j].col_droite = Collision.Collision.col_droite(
                           playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_coldroit);
                        //Update players adverses <- reception
                        int direction = 0;
                        if (network._moveXPlayer2 > AdvTeam[j]._positionPlayer.X)
                        {
                            network.shootDirectionLeft2 = false;
                            direction = 1;
                        }
                        else if (network._moveXPlayer2 < AdvTeam[j]._positionPlayer.X)
                        {
                            network.shootDirectionLeft2 = true;
                            direction = 2;
                        }
                        AdvTeam[j].UpdateMulti(gameTime, network._moveXPlayer2, network._jumpPlayer2, 0, network._shoot, network, direction);
                        #region Collision Armes
                        if (AdvTeam[j]._weapons._tir)
                        {
                            timeChrono = timeChrono <= 3f ? timeChrono : 3f;
                            if (CollisionRectangles.collision(AdvTeam[j]._weapons._position, rectList(MyTeam, AdvTeam, j, false)))
                            {
                                AdvTeam[j]._weapons._tir = false;
                                AdvTeam[j]._weapons.touch = true;
                                AdvTeam[j]._weapons._alreadyShoot = false;

                                for (int k = 0; k < AdvTeam.Count; k++)
                                {
                                    if (AdvTeam[k].PositionRectangle.Intersects(AdvTeam[j]._weapons._position))
                                    {
                                        AdvTeam[k].Life -= AdvTeam[j]._weapons._dommage;
                                        break;
                                    }
                                }
                                for (int k = 0; k < MyTeam.Count; k++)
                                {

                                    if (MyTeam[k].PositionRectangle.Intersects(AdvTeam[j]._weapons._position))
                                    {
                                        MyTeam[k].Life -= AdvTeam[j]._weapons._dommage;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion
                        Console.WriteLine("alreadyShoot : " + network._alreadyShoot + "  WeaponNbr : " + AdvTeam[j]._weapons._weaponNbr);

                        waitingTime += gameTime.ElapsedGameTime.Milliseconds;
                    }
                    #endregion
                }
                //Joueur 2
                else
                {
                    #region Reception => Au joueur 1 de jouer
                    if (network._turnToPlayer1)
                    {
                        //Collisions du perso
                        pperso_colbas.X = (int)MyTeam[i].Position.X + 30;
                        pperso_colbas.Y = (int)MyTeam[i].Position.Y + 5;

                        pperso_colbas2.X = (int)MyTeam[i].Position.X + 30;
                        pperso_colbas2.Y = (int)MyTeam[i].Position.Y + 10;

                        pperso_colgauche.X = (int)MyTeam[i].Position.X + 15;
                        pperso_colgauche.Y = (int)MyTeam[i].Position.Y + 5;

                        pperso_coldroit.X = (int)(MyTeam[i].Position.X + 50);
                        pperso_coldroit.Y = (int)(MyTeam[i].Position.Y + 5);

                        MyTeam[i].col_bas2 = Collision.Collision.col_bas(
                            playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_colbas);

                        MyTeam[i].col_bas = Collision.Collision.col_bas(
                            playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_colbas);

                        MyTeam[i].col_gauche = Collision.Collision.col_droite(
                           playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_colgauche);

                        MyTeam[i].col_droite = Collision.Collision.col_droite(
                           playerR.Texture, MyTeam[i].PositionRectangle, background, pmap, pperso_coldroit);
                        //Update players gentil selon leurs decalage <- reception
                        int direction = 0;
                        if (network._moveX > MyTeam[j]._positionPlayer.X)
                        {
                            network.shootDirectionLeft1 = false;
                            direction = 1;
                        }
                        else if (network._moveX < MyTeam[j]._positionPlayer.X)
                        {
                            network.shootDirectionLeft1 = true;
                            direction = 2;
                        }
                        MyTeam[j].UpdateMulti(gameTime, network._moveX, network._jump, 0, network._shoot, network, direction);

                        #region Collision Armes
                        if (MyTeam[i]._weapons._tir)
                        {
                            timeChrono = timeChrono <= 3f ? timeChrono : 3f;
                            if (CollisionRectangles.collision(MyTeam[i]._weapons._position, rectList(MyTeam, AdvTeam, i, true)))
                            {
                                MyTeam[i]._weapons._tir = false;
                                MyTeam[i]._weapons.touch = true;
                                MyTeam[i]._weapons._alreadyShoot = false;

                                for (int k = 0; k < AdvTeam.Count; k++)
                                {
                                    if (AdvTeam[k].PositionRectangle.Intersects(MyTeam[i]._weapons._position))
                                    {
                                        AdvTeam[k].Life -= MyTeam[i]._weapons._dommage;
                                        break;
                                    }
                                }
                                for (int k = 0; k < MyTeam.Count; k++)
                                {

                                    if (MyTeam[k].PositionRectangle.Intersects(MyTeam[i]._weapons._position))
                                    {
                                        MyTeam[k].Life -= MyTeam[i]._weapons._dommage;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        waitingTime += gameTime.ElapsedGameTime.Milliseconds;
                    }
                    #endregion
                    #region Au joueur 2 de jouer
                    else
                    {
                        waitingTime = 0;

                        pperso_colbas.X = (int)AdvTeam[j].Position.X + 30;
                        pperso_colbas.Y = (int)AdvTeam[j].Position.Y + 5;

                        pperso_colbas2.X = (int)AdvTeam[j].Position.X + 30;
                        pperso_colbas2.Y = (int)AdvTeam[j].Position.Y + 10;

                        pperso_colgauche.X = (int)AdvTeam[j].Position.X + 15;
                        pperso_colgauche.Y = (int)AdvTeam[j].Position.Y + 5;

                        pperso_coldroit.X = (int)(AdvTeam[j].Position.X + 50);
                        pperso_coldroit.Y = (int)(AdvTeam[j].Position.Y + 5);

                        AdvTeam[j].col_bas2 = Collision.Collision.col_bas(
                            playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_colbas);

                        AdvTeam[j].col_bas = Collision.Collision.col_bas(
                            playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_colbas);

                        AdvTeam[j].col_gauche = Collision.Collision.col_droite(
                           playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_colgauche);

                        AdvTeam[j].col_droite = Collision.Collision.col_droite(
                           playerRAdv.Texture, AdvTeam[j].PositionRectangle, background, pmap, pperso_coldroit);

                        //Update Players adverses
                        AdvTeam[j].UpdateMulti(gameTime, mouse, network);

                        #region Collision Armes
                        if (AdvTeam[j]._weapons._tir)
                        {
                            timeChrono = timeChrono <= 3f ? timeChrono : 3f;
                            if (CollisionRectangles.collision(AdvTeam[j]._weapons._position, rectList(MyTeam, AdvTeam, j, false)))
                            {
                                AdvTeam[j]._weapons._tir = false;
                                AdvTeam[j]._weapons.touch = true;
                                AdvTeam[j]._weapons._alreadyShoot = false;

                                for (int k = 0; k < AdvTeam.Count; k++)
                                {
                                    if (AdvTeam[k].PositionRectangle.Intersects(AdvTeam[j]._weapons._position))
                                    {
                                        AdvTeam[k].Life -= AdvTeam[j]._weapons._dommage;
                                        break;
                                    }
                                }
                                for (int k = 0; k < MyTeam.Count; k++)
                                {

                                    if (MyTeam[k].PositionRectangle.Intersects(AdvTeam[j]._weapons._position))
                                    {
                                        MyTeam[k].Life -= AdvTeam[j]._weapons._dommage;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        timeChrono -= gameTime.ElapsedGameTime.Milliseconds;
                        playingTime -= gameTime.ElapsedGameTime.Milliseconds;

                        if (timeChrono < 0)
                        {
                            timeChrono = 10f;
                            if (playingTime <= 0 && !AdvTeam[j].isActive(network))
                            {
                                network._alreadyShoot = false;
                                if (i < MyTeam.Count - 1)
                                    i += 1;
                                else
                                    i = 0;
                                if (j < AdvTeam.Count - 1)
                                    j += 1;
                                else
                                    j = 0;
                                network._stillSamePlayer = false;
                                playingTime = 15000f;
                                network._turnToPlayer1 = true;
                                if (network._continueGame)
                                    network.threadLaucher();
                            }
                            network.SenderTCP((int)AdvTeam[j]._positionPlayer.X);
                            network._stillSamePlayer = true;
                        }
                    }
                    #endregion
                }
            }
            #endregion

        }

        public void Draw(SpriteBatch spriteBatch, bool display)
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, background.Width, background.Height), Color.White);
            if (!network.connected)
            {
                //Bouttons et phrase d'entree
                Join.Draw(spriteBatch);
                Create.Draw(spriteBatch);
                Next.Draw(spriteBatch);
                Prec.Draw(spriteBatch);
                spriteBatch.DrawString(font, drawMenu, new Vector2(500, 350), Color.White);
            }

            else
            {
                //Players
                for (int k = 0; k < MyTeam.Count; k++) MyTeam[k].DrawWeapons(spriteBatch, Color.Cyan, network.nbrWeapon, network);
                for (int k = 0; k < AdvTeam.Count; k++) AdvTeam[k].DrawWeapons(spriteBatch, Color.Red, network.nbrWeapon, network);

                //Affichage du temps de jeux restant
                if (network._turnToPlayer1 && network._isPlayer1 || !network._turnToPlayer1 && !network._isPlayer1)
                    spriteBatch.DrawString(font, ((int)((playingTime - waitingTime) / 1000)).ToString(), new Vector2(20, 20), Color.White);
                else
                {
                    if (Options._english)
                        spriteBatch.DrawString(font, "Now.. We wait to play !..", new Vector2(20, 20), Color.White);
                    else
                        spriteBatch.DrawString(font, "Bon.. Bah on patiente hein..!", new Vector2(20, 20), Color.White);
                }

                rain.Draw(spriteBatch);
            }
        }

        private List<Rectangle> rectList(List<Player> MyTeam, List<Player> AdvTeam, int pos, bool myTeamBool)
        {
            List<Rectangle> rectLst = new List<Rectangle>();
            for (int i = 0; i < MyTeam.Count; i++)
            {
                rectLst.Add(MyTeam[i].PositionRectangle);
            }

            for (int i = 0; i < AdvTeam.Count; i++)
            {
                rectLst.Add(AdvTeam[i].PositionRectangle);
            }
            if (myTeamBool) rectLst.RemoveAt(pos);
            else rectLst.RemoveAt(pos + MyTeam.Count);

            return rectLst;
        }
    }
}
