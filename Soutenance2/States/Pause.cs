﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Soutenance2
{

    class Pause
    {
        //Creation des boutons du menu + texte
         Button optionsButton, returnButton, exitButton, mainButton;
         string optionsStr = "Options du jeu", returnStr = "Retourner dans le jeu", exitStr = "Quitter le jeu", mainStr = "Retourner dans le menu principal";
         string optionStrEN = "Options", returnStrEN = "Back", exitStrEN = "Exit the game";
        int _widthScreen;
        GraphicsDevice _graphics;

         Rectangle recTemp;

        //Police
        SpriteFont font;

        //Enum pour les differents etats du menu pause
        public enum GameState
        {
            pauseMenu,
            exit,
            playMenu,
            optionsMenu,
            mainMenu, multMenu
        }

        public GameState currentGameState = GameState.pauseMenu;
        public GameState LastGameState { get; set; }

        //Initialisation des variables et autre
        public Pause(int positionWidth, int positionHeight, int pauseHeight, ContentManager content, GraphicsDevice graphics, int widthScreen)
        {
            _graphics = graphics;

            LastGameState = GameState.mainMenu;
            font = content.Load<SpriteFont>("Titles");

            //Creation d'un rectangle pour la taille de la pause
            recTemp = new Rectangle(positionWidth, positionHeight, 800, 600);

            this._widthScreen = widthScreen;

            #region Buttons
            //On instancie les boutons (et leur position) avec la superbe classe qui sert le the (Button.cs)

            //redefinir les positions des collisions...
            if (!Options._english)
            {
                optionsButton = new Button(optionsStr, font, graphics, widthScreen);
                optionsButton.setPosition(new Vector2(recTemp.X + 400 - font.MeasureString(optionsStr).X / 2, recTemp.Y + recTemp.Height / 2 - 100));
                returnButton = new Button(returnStr, font, graphics, widthScreen);
                returnButton.setPosition(new Vector2(recTemp.X + 400 - font.MeasureString(returnStr).X / 2, recTemp.Y + recTemp.Height / 2 - 50));
                exitButton = new Button(exitStr, font, graphics, widthScreen);
                exitButton.setPosition(new Vector2(recTemp.X + 400 - font.MeasureString(exitStr).X / 2, recTemp.Y + recTemp.Height / 2 + 200));
            }
            else
            {
                optionsButton = new Button(optionStrEN, font, graphics, widthScreen);
                optionsButton.setPosition(new Vector2(recTemp.X + 400 - font.MeasureString(optionStrEN).X / 2, recTemp.Y + recTemp.Height / 2 - 100));
                returnButton = new Button(returnStrEN, font, graphics, widthScreen);
                returnButton.setPosition(new Vector2(recTemp.X + 400 - font.MeasureString(returnStrEN).X / 2, recTemp.Y + recTemp.Height / 2 - 50));
                exitButton = new Button(exitStrEN, font, graphics, widthScreen);
                exitButton.setPosition(new Vector2(recTemp.X + 400 - font.MeasureString(exitStrEN).X / 2, recTemp.Y + recTemp.Height / 2 + 200));
            }
            #endregion
        }


        public void Update(MouseState mouse, bool multi)
        {
            //Comme pour main possedant des boutons on verifie si un des boutons est clique ou pas
            mouse = Mouse.GetState();
            if (optionsButton.isClicked) currentGameState = GameState.optionsMenu;



            if (exitButton.isClicked) currentGameState = GameState.exit;

            //MAJ des boutons
            if (multi)
            {
                if (returnButton.isClicked) currentGameState = GameState.multMenu;
                optionsButton.UpdateStringPause(mouse);
                returnButton.UpdateStringPause(mouse);
                exitButton.UpdateStringPause(mouse);
            }
            else
            {
                if (returnButton.isClicked) currentGameState = GameState.playMenu;
                optionsButton.UpdateStringPauseCamera(mouse);
                returnButton.UpdateStringPauseCamera(mouse);
                exitButton.UpdateStringPauseCamera(mouse);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //On dessine les boutons
            optionsButton.DrawString(spriteBatch);
            returnButton.DrawString(spriteBatch);
            exitButton.DrawString(spriteBatch);
        }
    }
}
