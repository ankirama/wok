﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Soutenance2
{

    //A commenter et a changer...
    class Camera
    {
        public Matrix transform;
        Viewport view;
        public Vector2 centre;
        public static int move;

        public Camera(Viewport newView)
        {
            view = newView;
        }

        public void Update(GameTime gameTime, Vector2 movement, int width,int widthScreen)
        {
            move = (int)transform.M41;
            //centre a changer
            centre = new Vector2(movement.X + width / 2 - widthScreen / 2, 0);
            transform = Matrix.CreateScale(new Vector3(1, 1, 0)) *
                Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));

            //Valeur....BULLSHIT!
            if (transform.M41 < 720) transform.M41 = 720;
            else if (transform.M41 > widthScreen / 2) transform.M41 = (widthScreen / 2);
            if (centre.X < -(widthScreen / 2)) centre.X = -(widthScreen / 2);
            if (centre.X >= (widthScreen / 2)) centre.X = (widthScreen / 2);
        }
    }
}
