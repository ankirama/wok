﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Soutenance2.Collision
{
    class Collision
    {
        public static Color[] TerrainColor, PersoColor, KameaColor, SheepColor, BazookaColor;

        public static bool col_bas(Texture2D personnage, Rectangle rectpersonnage, Texture2D terrain, Rectangle rectterrain, Rectangle rectpersonnage_bas)
        {
            //Intersection par pixel
            return IntersectPixels(rectpersonnage_bas, PersoColor, rectterrain, TerrainColor);
        }

        public static bool col_bas(Rectangle rectterrain, Rectangle rectpersonnage_bas)
        {
            //Intersection par pixel
            return IntersectPixels(rectpersonnage_bas, PersoColor, rectterrain, TerrainColor);
        }

        public static bool col_bas2(Texture2D personnage, Rectangle rectpersonnage, Texture2D terrain, Rectangle rectterrain, Rectangle rectpersonnage_bas)
        {
            rectpersonnage.Y += 10;

            //Intersection par pixel
            return IntersectPixels(rectpersonnage_bas, PersoColor, rectterrain, TerrainColor);
        }

        public static bool col_gauche(Texture2D personnage, Rectangle rectpersonnage, Texture2D terrain, Rectangle rectterrain, Rectangle rectpersonnage_gauche)
        {
            rectpersonnage.X -= 20;

            return IntersectPixels(rectpersonnage_gauche, PersoColor, rectterrain, TerrainColor);
        }

        public static bool col_droite(Texture2D personnage, Rectangle rectpersonnage, Texture2D terrain, Rectangle rectterrain, Rectangle rectpersonnage_droite)
        {
            return IntersectPixels(rectpersonnage_droite, PersoColor, rectterrain, TerrainColor);
        }

        public static bool IntersectPixels(Rectangle rectangleA, Color[] dataA, Rectangle rectangleB, Color[] dataB)
        {
            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    Color colorA = dataA[(x - rectangleA.Left) + (y - rectangleA.Top) * rectangleA.Width];
                    Color colorB = dataB[(x - rectangleB.Left) + (y - rectangleB.Top) * rectangleB.Width];

                    if (colorA.A != 0 && colorB.A != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
