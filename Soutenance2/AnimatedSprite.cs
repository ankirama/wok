﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;

/*
 * 
 * 
 * Animation des sprites/persos + gravite
 * 
 * 
 */

namespace Soutenance2
{
    class AnimatedSprite
    {
        //Definition des methodes et variables globales
        public Texture2D _TextureRight { get; set; }
        public Texture2D _TextureLeft { get; set; }
        public int RowsRight { get; set; }
        public int ColumnsRight { get; set; }
        public int RowsLeft { get; set; }
        public int ColumnsLeft { get; set; }
        public int currentFrameRight, currentFrameLeft;
        public int totalFrames;
        private int count = 0;


        public bool right = true;

        //Definit notre texture avec sa taille
        public AnimatedSprite(Texture2D new_TextureRight, Texture2D new_TextureLeft, int rowsRight, int columnsRight, int rowsLeft, int columnsLeft)
        {
            _TextureRight = new_TextureRight;
            _TextureLeft = new_TextureLeft;

            RowsRight = rowsRight;
            ColumnsRight = columnsRight;
            RowsLeft = rowsLeft;
            ColumnsLeft = columnsLeft;
            currentFrameRight = 0;
            currentFrameLeft = 0;
            totalFrames = RowsLeft * ColumnsLeft;
        }

        //Permet de faire bouger la texture
        public void Update(Vector2 newPosition, int direction)
        {

            //direction 1 = droit, 2 = gauche
            if (direction == 2)
            {
                count++;
                if (count == 5)
                {
                    currentFrameLeft++;
                    right = false;
                    count = 0;
                }
            }
            else if (direction == 1)
            {
                count++;
                if (count == 5)
                {
                    currentFrameRight++;
                    right = true;
                    count = 0;
                }
            }
            if (currentFrameRight == totalFrames) currentFrameRight = 0;
            else if (currentFrameLeft == totalFrames) currentFrameLeft = 0;
        }

        //Dessine la texture
        public void Draw(SpriteBatch spriteBatch, Vector2 location)
        {
            int widthRight = _TextureRight.Width / ColumnsRight; // la largeur = largeur de la texture / nbr de colones (4)
            int heigthRight = _TextureRight.Height / RowsRight; // la hauteur = hauteur de la texture / nbr de lignes (4)
            int rowRight = (int)((float)currentFrameRight / (float)RowsRight); //ligne actuelle = frame actuelle / nbr de colones (4)
            int columnRight = currentFrameRight % ColumnsRight; // colone actuelle = frame actuelle / nbr de colones (4)
            Rectangle sourceRectangleRight = new Rectangle(widthRight * columnRight, heigthRight * rowRight, widthRight, heigthRight);
            Rectangle destinationRectangleRight = new Rectangle((int)location.X, (int)location.Y, widthRight/2, heigthRight/2);

            int widthLeft = _TextureLeft.Width / ColumnsLeft; // la largeur = largeur de la texture / nbr de colones (4)
            int heigthLeft = _TextureLeft.Height / RowsLeft; // la hauteur = hauteur de la texture / nbr de lignes (4)
            int rowLeft = (int)((float)currentFrameRight / (float)RowsRight); //ligne actuelle = frame actuelle / nbr de colones (4)
            int columnLeft = currentFrameLeft % ColumnsLeft; // colone actuelle = frame actuelle / nbr de colones (4)
            Rectangle sourceRectangleLeft = new Rectangle(widthLeft * columnLeft, heigthLeft * rowLeft, widthLeft , heigthLeft);
            Rectangle destinationRectangleLeft = new Rectangle((int)location.X, (int)location.Y, widthLeft/2, heigthLeft/2);

            if (right)
                spriteBatch.Draw(_TextureRight, destinationRectangleRight, sourceRectangleRight, Color.White);
            else
                spriteBatch.Draw(_TextureLeft, destinationRectangleLeft, sourceRectangleLeft, Color.White);



        }
    }
}
