﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Soutenance2
{
    class Destruction
    {
        public static void Destroy(Vector2 position, int yLength, int xLength)
        {
            for (int y = -yLength; y < yLength; y++)
            {
                for (int x = -xLength; x < xLength; x++)
                {
                    int nbr = (y + (int)position.Y) * Play.map.Width + x + (int)position.X;
                    if (isInBound(nbr))
                        Collision.Collision.TerrainColor[nbr] = Color.Transparent;
                }
            }

            Play.map.Texture.SetData<Color>(Collision.Collision.TerrainColor);
        }

        private static bool isInBound(int nbr)
        {
            return (nbr >= 0 && nbr < Play.map.Width * Play.map.Height);
        }
    }
}
