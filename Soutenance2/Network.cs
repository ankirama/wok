﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace Soutenance2
{
    class Network
    {
        public int b = 1;
        public bool initialize = false;
        public string teamNameAdv = "Bob-Team", teamNameUs = "Snoozers";

        public int timer;
        public bool launched;
        public bool connected;

        public int nbrWeapon;
        public bool sheepLeft, sheepRight;
        public int decalX, decalY;
        public bool shootDirectionLeft1 = true, shootDirectionLeft2 = true;

        public static bool _quit = false;


        public bool _isPlayer1;
        public bool _turnToPlayer1;

        //actions
        public int _moveX;
        public int _moveXPlayer2;
        public bool _jump;
        public bool _jumpPlayer2;
        public bool _shoot;
        public bool _shootPlayer2;
        public bool _alreadyShoot;
        public static bool _pause;
        public static bool _pausePlayer2;
        public static bool _Player1;


        public bool _stillSamePlayer;
        public bool _continueGame;
        public bool isConnected;

        private Socket socket;

        #region Connexion
        public void ServerUDP()
        {
            launched = true;
            _isPlayer1 = true;
            _Player1 = true;
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            IPEndPoint endPointUdp = new IPEndPoint(IPAddress.Broadcast, 2424);

            //Envoi de l'addresse ip du serveur
            Console.WriteLine("En attente de joueurs..");
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    /*string[] verif = ip.ToString().Split('.');
                    if (verif[0] == "192" && verif[1] == "168")*/
                    localIP = ip.ToString();
                    break;
                }
            }
            Console.WriteLine(localIP);
            byte[] myIp = Encoding.UTF8.GetBytes(localIP);
            //byte[] myIp = Encoding.UTF8.GetBytes("127.0.0.1");
            isConnected = false;
            //Demarrage du thread TCP
            Thread threadServer = new Thread(ServerTCP);
            threadServer.Start();
            //Emission de la trame d'info
            timer = 15000;
            while (!isConnected && timer > 0 && !_quit)
            {
                client.SendTo(myIp, endPointUdp);
                Thread.Sleep(50);
            }
            if (timer <= 0)
            {
                launched = false;
                try
                {
                    Socket closer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    IPEndPoint closeEP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 2424);
                    closer.Connect(closeEP);
                    closer.Close();
                }
                catch { Console.WriteLine("Echec lors de la fermeture du thread server tcp"); }
            }
            else
                Console.WriteLine("Lancement du jeu!");
            client.Close();
        }

        public void ClientUDP()
        {
            timer = 10000;
            launched = true;
            _isPlayer1 = false;
            _Player1 = false;
            //initialisation
            UdpClient client = new UdpClient(2424);
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 2424);

            byte[] ipServer = client.Receive(ref endPoint);
            string ipToConnect = Encoding.UTF8.GetString(ipServer);
            if (ipToConnect.Length < 2)
            {
                launched = false;
                Console.WriteLine("L'addresse recue n'est pas valide");
                timer = 10;
                client.Close();
            }
            else
            {
                Console.WriteLine("Serveur avec l'addresse {0}", ipToConnect);
                client.Close();
                InitClientTCP(ipToConnect);
            }
        }

        private void InitClientTCP(string ipServer)
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ipServer), 2424);
            try
            {
                socket.Connect(endPoint);
                socket.NoDelay = true;
                byte[] data = new byte[1024];
                socket.Receive(data);
                b = BitConverter.ToInt32(data, 0);
                if (b > 3 || b < 1) b = 1;
                //foreach (Player player in MultiPlayers.MyTeam) player.teamName = Encoding.UTF8.GetString(data, 4, data.Length - 4);
                int length = BitConverter.ToInt32(data, 4);
                teamNameAdv = Encoding.UTF8.GetString(data, 8, length);
                /*data = new byte[4 + teamNameUs.Length];
                byte[] tmp = BitConverter.GetBytes(teamNameUs.Length);
                for (int i = 0; i < 4; i++)
                    data[i] = tmp[i];
                tmp = Encoding.UTF8.GetBytes(teamNameUs);
                for (int i = 0; i < teamNameUs.Length; i++)
                    data[i + 4] = tmp[i];
                socket.Send(data);*/
                socket.Send(Encoding.UTF8.GetBytes(teamNameUs));
                Thread threadReceiver = new Thread(ReceiverTCPPlayer2);
                threadReceiver.Start();
                isConnected = true;
                connected = true;
                initialize = true;
            }
            catch
            { Console.WriteLine("Erreur de connection"); launched = false; connected = false; }
        }

        private void ServerTCP()
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 2424);
            Socket tmpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                tmpSocket.Bind(endPoint);
            }
            catch { Console.WriteLine("Epic fail de binding"); launched = false; return; }
            tmpSocket.Listen(1);
            try
            {
                socket = tmpSocket.Accept();
            }
            catch { Console.WriteLine("Connexion annulee"); return; }
            Console.WriteLine("server cree!");
            tmpSocket.Close();
            try
            {
                byte[] data = new byte[8 + teamNameUs.Length];
                byte[] tmp = BitConverter.GetBytes(b);
                for (int i = 0; i < 4; i++)
                    data[i] = tmp[i];
                tmp = BitConverter.GetBytes(teamNameUs.Length);
                for (int i = 0; i < 4; i++)
                    data[i + 4] = tmp[i];
                tmp = Encoding.UTF8.GetBytes(teamNameUs);
                for (int i = 0; i < tmp.Length; i++)
                    data[i + 8] = tmp[i];
                socket.Send(data);
                Console.WriteLine("HELLO");

                socket.Receive(tmp);
                Console.WriteLine("WORLD");

                //foreach (Player player in MultiPlayers.AdvTeam) player.teamName = Encoding.UTF8.GetString(tmp);
                //int length = BitConverter.ToInt32(tmp, 0);
                teamNameAdv = Encoding.UTF8.GetString(tmp);
            }
            catch
            {
                Console.WriteLine("Erreur de l'envoi des donnee d'init");
                launched = false;
                connected = false;
                return;
            }
            isConnected = true;
            connected = true;
            socket.NoDelay = true;
            initialize = true;
            Thread threadReceiver = new Thread(ReceiverTCP);
            threadReceiver.Start();
        }
        #endregion

        #region In Game
        public void ReceiverTCP()
        {
            byte[] dataByte = new byte[26];
            _stillSamePlayer = true;
            try
            {
                while (!_turnToPlayer1)
                {
                    socket.Receive(dataByte);
                    _moveXPlayer2 = BitConverter.ToInt32(dataByte, 0);
                    if (_moveXPlayer2 == -1) break;
                    _stillSamePlayer = BitConverter.ToBoolean(dataByte, 4);
                    _turnToPlayer1 = BitConverter.ToBoolean(dataByte, 5);
                    _jumpPlayer2 = BitConverter.ToBoolean(dataByte, 6);
                    _shootPlayer2 = BitConverter.ToBoolean(dataByte, 7);
                    _alreadyShoot = BitConverter.ToBoolean(dataByte, 8);
                    _pausePlayer2 = BitConverter.ToBoolean(dataByte, 9);
                    nbrWeapon = BitConverter.ToInt32(dataByte, 10);
                    if (nbrWeapon == 1)
                    {
                        decalX = BitConverter.ToInt32(dataByte, 14);
                        decalY = BitConverter.ToInt32(dataByte, 18);
                    }
                    if (nbrWeapon == 2)
                    {
                        sheepLeft = BitConverter.ToBoolean(dataByte, 14);
                        sheepRight = BitConverter.ToBoolean(dataByte, 15);
                    }
                }
                _alreadyShoot = false;
            }
            catch { return; }
        }

        public void ReceiverTCPPlayer2()
        {
            byte[] dataByte = new byte[26];
            _stillSamePlayer = true;

            try
            {
                while (_turnToPlayer1)
                {
                    socket.Receive(dataByte);
                    _moveX = BitConverter.ToInt32(dataByte, 0);
                    if (_moveX == -1) break;
                    _stillSamePlayer = BitConverter.ToBoolean(dataByte, 4);
                    _turnToPlayer1 = BitConverter.ToBoolean(dataByte, 5);
                    _jump = BitConverter.ToBoolean(dataByte, 6);
                    _shoot = BitConverter.ToBoolean(dataByte, 7);
                    _alreadyShoot = BitConverter.ToBoolean(dataByte, 8);
                    _pause = BitConverter.ToBoolean(dataByte, 9);
                    nbrWeapon = BitConverter.ToInt32(dataByte, 10);
                    if (nbrWeapon == 1)
                    {
                        decalX = BitConverter.ToInt32(dataByte, 14);
                        decalY = BitConverter.ToInt32(dataByte, 18);
                    }
                    if (nbrWeapon == 2)
                    {
                        sheepLeft = BitConverter.ToBoolean(dataByte, 14);
                        sheepRight = BitConverter.ToBoolean(dataByte, 15);
                    }
                }
                _alreadyShoot = false;
            }
            catch { return; }
        }

        public void SenderTCP(int positionX)
        {
            //Format des donnees : [positionX]|[stillSamePlayer]|[turnToPlayer1]|[saut]|[shoot]|[alreadyShoot]|[pause]
            try
            {
                byte[] jump;
                byte[] shoot;
                byte[] pause;
                byte[] posByte = BitConverter.GetBytes(positionX);
                byte[] same = BitConverter.GetBytes(_stillSamePlayer);
                byte[] player = BitConverter.GetBytes(_turnToPlayer1);
                byte[] alreadyShoot = BitConverter.GetBytes(_alreadyShoot);
                byte[] armSelected = BitConverter.GetBytes(nbrWeapon);
                byte[] leftOrX, rightOrY;
                if (nbrWeapon == 1)   //Si bazooka ou mouton
                {
                    leftOrX = BitConverter.GetBytes(decalX);
                    rightOrY = BitConverter.GetBytes(decalY);
                }
                if (nbrWeapon == 2)
                {
                    leftOrX = BitConverter.GetBytes(sheepLeft);
                    rightOrY = BitConverter.GetBytes(sheepRight);
                }

                if (_isPlayer1)
                {
                    jump = BitConverter.GetBytes(_jump);
                    shoot = BitConverter.GetBytes(_shoot);
                    pause = BitConverter.GetBytes(_pause);
                }
                else
                {
                    jump = BitConverter.GetBytes(_jumpPlayer2);
                    shoot = BitConverter.GetBytes(_shootPlayer2);
                    pause = BitConverter.GetBytes(_pausePlayer2);
                }
                pause = BitConverter.GetBytes(_pause);
                byte[] dataByte = new byte[26];     //Taille du tableau
                for (int i = 0; i < 4; i++)
                    dataByte[i] = posByte[i];
                dataByte[4] = same[0];
                dataByte[5] = player[0];
                dataByte[6] = jump[0];
                dataByte[7] = shoot[0];
                dataByte[8] = alreadyShoot[0];
                dataByte[9] = pause[0];
                for (int i = 0; i < 4; i++)
                    dataByte[i + 10] = armSelected[i];
                if (nbrWeapon == 1)   //Si bazooka ou mouton
                {
                    leftOrX = BitConverter.GetBytes(decalX);
                    rightOrY = BitConverter.GetBytes(decalY);
                    for (int i = 0; i < 4; i++)
                    {
                        dataByte[i + 14] = leftOrX[i];
                    }
                    for (int i = 0; i < 4; i++)
                    {
                        dataByte[i + 18] = rightOrY[i];
                    }
                }
                if (nbrWeapon == 2)
                {
                    leftOrX = BitConverter.GetBytes(sheepLeft);
                    rightOrY = BitConverter.GetBytes(sheepRight);
                    dataByte[14] = leftOrX[0];
                    dataByte[15] = rightOrY[0];
                }
                socket.Send(dataByte);
            }
            catch { Console.WriteLine("Erreur d'envoi"); }
        }
        #endregion
        //
        public static void ITakeMyCoffePause()
        {
            if (Network._Player1)
            {
                Network._pause = true;
                new Network().SenderTCP(new Network()._moveX);
                Network._pause = false;
            }
            else
            {
                Network._pausePlayer2 = true;
                new Network().SenderTCP(new Network()._moveX);
                Network._pausePlayer2 = false;
            }
        }
        //
        public void threadLaucher()
        {
            if (_isPlayer1)
            {
                Thread myThread = new Thread(ReceiverTCP);
                myThread.Start();
            }
            else
            {
                Thread myThread = new Thread(ReceiverTCPPlayer2);
                myThread.Start();
            }
        }

        public void Close()
        {
            try
            {
                _moveX = _moveXPlayer2 = -1;
                if (_isPlayer1)
                    _turnToPlayer1 = true;
                else
                    _turnToPlayer1 = false;

                byte[] byteClose = new byte[26];
                byte[] moveByte = BitConverter.GetBytes(-1);
                byte[] same = BitConverter.GetBytes(false);

                for (int i = 0; i < 4; i++)
                    byteClose[i] = moveByte[i];
                byteClose[4] = same[0];
                byteClose[5] = same[0];
                byteClose[6] = byteClose[7] = byteClose[8] = byteClose[9] = same[0];

                socket.Send(byteClose);
                socket.Close();
            }
            catch
            {

            }
        }
    }
}
