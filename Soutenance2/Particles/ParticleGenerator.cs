﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Soutenance2
{
    class ParticleGenerator
    {
        Texture2D texture;//texture

        float spawnWidth;//
        float density;//densité
        float timer;//
        bool test;
        Keys _key;

        List<Raindrops> raindrops = new List<Raindrops>(); //liste des 

        Random rand1;//nombre aléatoire n°1
        Random rand2;//nombre aléatoire n°2

        public ParticleGenerator(Texture2D _texture, float _spamWidth, float _density)
        {
            texture = _texture;
            spawnWidth = _spamWidth;
            density = _density;

            rand1 = new Random();//création du nombre aléatoire 1
            rand2 = new Random();//création du nombre aléatoire 2
        }

        public ParticleGenerator(Texture2D _texture, float _spamWidth, float _density, Keys key)
        {
            texture = _texture;
            spawnWidth = _spamWidth;
            density = _density;

            rand1 = new Random();//création du nombre aléatoire 1
            rand2 = new Random();//création du nombre aléatoire 2
            this._key = key;
        }

        public void createParticle()//méthode de création de la particule 
        {
            //création de la liste de gouttes d'eau, avec (la texture, le vecteur d'origine qui part de -50 pour éviter qu'il y est un endroit sans 
            //gouttes visibles a gauche de l'écran du fait que les gouttes partent sur le coté, en fait pour faire simple, c'est le vecteur origine !
            // et 0 pour qu'il commence en haut de l'écran
            raindrops.Add(new Raindrops(texture, new Vector2( -spawnWidth + (float)rand1.NextDouble() * spawnWidth, 0), new Vector2(1, rand2.Next(5, 8))));
            raindrops.Add(new Raindrops(texture, new Vector2((float)rand1.NextDouble() * spawnWidth, 0), new Vector2(1, rand2.Next(5, 8))));
        }

        public void createParticleRandom(Vector2 position)
        {
            rand1 = new Random();//création du nombre aléatoire 1
            rand2 = new Random();
            //création de la liste de gouttes d'eau, avec (la texture, le vecteur d'origine qui part de -50 pour éviter qu'il y est un endroit sans 
            //gouttes visibles a gauche de l'écran du fait que les gouttes partent sur le coté, en fait pour faire simple, c'est le vecteur origine !
            // et 0 pour qu'il commence en haut de l'écran

                for (int i = 0; i < 30; i++)
                {
                    raindrops.Add(new Raindrops(texture, new Vector2(position.X - 800 + spawnWidth - Camera.move, position.Y), new Vector2(rand1.Next(-15, 15), rand1.Next(-15, 15))));
                }
        }

        public void Update(GameTime gametime, GraphicsDevice graphics)//méthode de mise a jour
        {
            timer += (float)gametime.ElapsedGameTime.TotalMilliseconds;//compteur de temps

            while (timer > 0)//boucle pour le timer superieur a zero
            {
                timer -= 1f / density;//decrementation du timer de 1 divisé par la densité du nombre des gouttes d'eau (afin d'avoir un nombre stable de gouttes d'eau)
                createParticle();//appel de la methode de creation de particule tant que la condition du timer est vérifiée
            }

            for (int i = 0; i < raindrops.Count; i++)// de i jusqu'au nombre de particule créées
            {
                raindrops[i].update(); //mettre a jour chaque particule

                if (raindrops[i].Position.Y > graphics.Viewport.Height)//si elle est hors de l'ecran 
                {
                    raindrops.RemoveAt(i);//alors on la supprime
                    i--;//et on décrémante i
                }
            }
        }

        public void UpdateRandom(GameTime gametime, GraphicsDevice graphics, Vector2 position)//méthode de mise a jour
        {
            timer += (float)gametime.ElapsedGameTime.TotalMilliseconds;//compteur de temps

            while (timer > 0)//boucle pour le timer superieur a zero
            {
                timer -= 10f / density;//decrementation du timer de 1 divisé par la densité du nombre des gouttes d'eau (afin d'avoir un nombre stable de gouttes d'eau)
                createParticleRandom(position);//appel de la methode de creation de particule tant que la condition du timer est vérifiée
            }

            for (int i = 0; i < raindrops.Count; i++)// de i jusqu'au nombre de particule créées
            {
                raindrops[i].update(); //mettre a jour chaque particule

                if (raindrops[i].Position.Y > graphics.Viewport.Height)//si elle est hors de l'ecran 
                {
                    raindrops.RemoveAt(i);//alors on la supprime
                    i--;//et on décrémante i
                }
            }
        }

        public void Draw(SpriteBatch spritebatch)
        {
            foreach (Raindrops raindrop in raindrops)//pour chaque particule de la liste 
                raindrop.Draw(spritebatch);//dessiner les gouttes :D
        }
    }
}
