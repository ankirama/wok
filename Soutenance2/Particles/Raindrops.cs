﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Soutenance2
{
    class Raindrops
    {
        Texture2D _texture; //texture (goutte d'eau)
        Vector2 _position; //position
        Vector2 _velocity; //vitesse

        public Vector2 Position
        {
            get { return _position; }
        }

        public Raindrops(Texture2D texture, Vector2 position, Vector2 velocity)
        {
            _texture = texture; //texture
            _position = position; //position
            _velocity = velocity; // vitesse
        }

        public void update()
        {
            _position += _velocity; //position recoit position + vitesse ! et ramene le cafe :D
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(_texture, _position, Color.White);// dessin de la texture appelé dans le game1 donc la goutte d'eau, avec sa position de depart
        }
    }
}
