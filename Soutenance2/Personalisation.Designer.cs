﻿namespace Soutenance2
{
    partial class Personalisation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Personalisation));
            this.labelTeamName = new System.Windows.Forms.Label();
            this.labelNamePlayers = new System.Windows.Forms.Label();
            this.textBoxTeamName = new System.Windows.Forms.TextBox();
            this.textBoxNamePl1 = new System.Windows.Forms.TextBox();
            this.textBoxNamePl2 = new System.Windows.Forms.TextBox();
            this.textBoxNamePl3 = new System.Windows.Forms.TextBox();
            this.buttonSaveNames = new System.Windows.Forms.Button();
            this.groupBoxNames = new System.Windows.Forms.GroupBox();
            this.labelErreurName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonNextView = new System.Windows.Forms.Button();
            this.buttonPreView = new System.Windows.Forms.Button();
            this.buttonSaveView = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.labelSaveView = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.buttonColor = new System.Windows.Forms.Button();
            this.buttonApplyColor = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.groupBoxNames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTeamName
            // 
            this.labelTeamName.Location = new System.Drawing.Point(12, 35);
            this.labelTeamName.Name = "labelTeamName";
            this.labelTeamName.Size = new System.Drawing.Size(100, 23);
            this.labelTeamName.TabIndex = 3;
            this.labelTeamName.Text = "Nom d\'equipe";
            // 
            // labelNamePlayers
            // 
            this.labelNamePlayers.Location = new System.Drawing.Point(12, 81);
            this.labelNamePlayers.Name = "labelNamePlayers";
            this.labelNamePlayers.Size = new System.Drawing.Size(100, 20);
            this.labelNamePlayers.TabIndex = 5;
            this.labelNamePlayers.Text = "Noms des joueurs";
            // 
            // textBoxTeamName
            // 
            this.textBoxTeamName.Location = new System.Drawing.Point(112, 32);
            this.textBoxTeamName.Name = "textBoxTeamName";
            this.textBoxTeamName.Size = new System.Drawing.Size(154, 20);
            this.textBoxTeamName.TabIndex = 7;
            // 
            // textBoxNamePl1
            // 
            this.textBoxNamePl1.Location = new System.Drawing.Point(113, 78);
            this.textBoxNamePl1.Name = "textBoxNamePl1";
            this.textBoxNamePl1.Size = new System.Drawing.Size(154, 20);
            this.textBoxNamePl1.TabIndex = 1;
            // 
            // textBoxNamePl2
            // 
            this.textBoxNamePl2.Location = new System.Drawing.Point(113, 104);
            this.textBoxNamePl2.Name = "textBoxNamePl2";
            this.textBoxNamePl2.Size = new System.Drawing.Size(154, 20);
            this.textBoxNamePl2.TabIndex = 6;
            // 
            // textBoxNamePl3
            // 
            this.textBoxNamePl3.Location = new System.Drawing.Point(113, 130);
            this.textBoxNamePl3.Name = "textBoxNamePl3";
            this.textBoxNamePl3.Size = new System.Drawing.Size(154, 20);
            this.textBoxNamePl3.TabIndex = 4;
            // 
            // buttonSaveNames
            // 
            this.buttonSaveNames.Location = new System.Drawing.Point(14, 160);
            this.buttonSaveNames.Name = "buttonSaveNames";
            this.buttonSaveNames.Size = new System.Drawing.Size(89, 23);
            this.buttonSaveNames.TabIndex = 2;
            this.buttonSaveNames.Text = "Sauvegarder";
            this.buttonSaveNames.UseVisualStyleBackColor = true;
            this.buttonSaveNames.Click += new System.EventHandler(this.buttonSaveNames_Click);
            // 
            // groupBoxNames
            // 
            this.groupBoxNames.Controls.Add(this.labelErreurName);
            this.groupBoxNames.Controls.Add(this.textBoxNamePl1);
            this.groupBoxNames.Controls.Add(this.buttonSaveNames);
            this.groupBoxNames.Controls.Add(this.labelTeamName);
            this.groupBoxNames.Controls.Add(this.textBoxNamePl3);
            this.groupBoxNames.Controls.Add(this.labelNamePlayers);
            this.groupBoxNames.Controls.Add(this.textBoxNamePl2);
            this.groupBoxNames.Controls.Add(this.textBoxTeamName);
            this.groupBoxNames.Location = new System.Drawing.Point(12, 12);
            this.groupBoxNames.Name = "groupBoxNames";
            this.groupBoxNames.Size = new System.Drawing.Size(272, 191);
            this.groupBoxNames.TabIndex = 9;
            this.groupBoxNames.TabStop = false;
            this.groupBoxNames.Text = "Personnalisation des noms";
            this.groupBoxNames.Enter += new System.EventHandler(this.groupBoxNames_Enter);
            // 
            // labelErreurName
            // 
            this.labelErreurName.Location = new System.Drawing.Point(109, 165);
            this.labelErreurName.Name = "labelErreurName";
            this.labelErreurName.Size = new System.Drawing.Size(157, 24);
            this.labelErreurName.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(90, 238);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(201, 235);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // buttonNextView
            // 
            this.buttonNextView.Location = new System.Drawing.Point(297, 352);
            this.buttonNextView.Name = "buttonNextView";
            this.buttonNextView.Size = new System.Drawing.Size(75, 23);
            this.buttonNextView.TabIndex = 7;
            this.buttonNextView.Text = "Suivant";
            this.buttonNextView.UseVisualStyleBackColor = true;
            this.buttonNextView.Click += new System.EventHandler(this.buttonNextView_Click);
            // 
            // buttonPreView
            // 
            this.buttonPreView.Location = new System.Drawing.Point(9, 352);
            this.buttonPreView.Name = "buttonPreView";
            this.buttonPreView.Size = new System.Drawing.Size(75, 23);
            this.buttonPreView.TabIndex = 6;
            this.buttonPreView.Text = "Precedent";
            this.buttonPreView.UseVisualStyleBackColor = true;
            this.buttonPreView.Click += new System.EventHandler(this.buttonPreView_Click);
            // 
            // buttonSaveView
            // 
            this.buttonSaveView.Location = new System.Drawing.Point(233, 209);
            this.buttonSaveView.Name = "buttonSaveView";
            this.buttonSaveView.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveView.TabIndex = 5;
            this.buttonSaveView.Text = "Sauvegarder";
            this.buttonSaveView.UseVisualStyleBackColor = true;
            this.buttonSaveView.Click += new System.EventHandler(this.buttonSaveView_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(71, 209);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(75, 23);
            this.buttonLoad.TabIndex = 4;
            this.buttonLoad.Text = "Charger";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // labelSaveView
            // 
            this.labelSaveView.Location = new System.Drawing.Point(314, 214);
            this.labelSaveView.Name = "labelSaveView";
            this.labelSaveView.Size = new System.Drawing.Size(74, 18);
            this.labelSaveView.TabIndex = 3;
            this.labelSaveView.Text = "Sauvegarde";
            // 
            // buttonColor
            // 
            this.buttonColor.Location = new System.Drawing.Point(297, 273);
            this.buttonColor.Name = "buttonColor";
            this.buttonColor.Size = new System.Drawing.Size(75, 23);
            this.buttonColor.TabIndex = 2;
            this.buttonColor.Text = "Couleur";
            this.buttonColor.UseVisualStyleBackColor = true;
            this.buttonColor.Click += new System.EventHandler(this.buttonColor_Click);
            // 
            // buttonApplyColor
            // 
            this.buttonApplyColor.Location = new System.Drawing.Point(297, 302);
            this.buttonApplyColor.Name = "buttonApplyColor";
            this.buttonApplyColor.Size = new System.Drawing.Size(75, 23);
            this.buttonApplyColor.TabIndex = 1;
            this.buttonApplyColor.Text = "Appliquer";
            this.buttonApplyColor.UseVisualStyleBackColor = true;
            this.buttonApplyColor.Click += new System.EventHandler(this.buttonApplyColor_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(152, 209);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 0;
            this.buttonReset.Text = "Reinitialiser";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // Personalisation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 485);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonApplyColor);
            this.Controls.Add(this.buttonColor);
            this.Controls.Add(this.labelSaveView);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.buttonSaveView);
            this.Controls.Add(this.buttonPreView);
            this.Controls.Add(this.buttonNextView);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBoxNames);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Personalisation";
            this.Text = "WoX Personnalisation";
            this.Load += new System.EventHandler(this.Personalisation_Load);
            this.groupBoxNames.ResumeLayout(false);
            this.groupBoxNames.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTeamName;
        private System.Windows.Forms.Label labelNamePlayers;
        private System.Windows.Forms.TextBox textBoxTeamName;
        private System.Windows.Forms.TextBox textBoxNamePl1;
        private System.Windows.Forms.TextBox textBoxNamePl2;
        private System.Windows.Forms.TextBox textBoxNamePl3;
        private System.Windows.Forms.Button buttonSaveNames;
        private System.Windows.Forms.GroupBox groupBoxNames;
        private System.Windows.Forms.Label labelErreurName;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonNextView;
        private System.Windows.Forms.Button buttonPreView;
        private System.Windows.Forms.Button buttonSaveView;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Label labelSaveView;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button buttonColor;
        private System.Windows.Forms.Button buttonApplyColor;
        private System.Windows.Forms.Button buttonReset;
    }
}