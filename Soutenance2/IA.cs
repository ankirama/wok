﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Soutenance2
{
    class IA
    {
        private int _widthScreen;

        //Une texture
        Texture2D _IALeft, _IARight;
        //une position de base
        Vector2 _positionIA;

        int _heightScreen;

        //List joueur proche
        List<Vector2> posPlayers;

        //Rectangle pour collision
        private Rectangle positionRectangle;
        public Rectangle PositionRectangle { get { return positionRectangle; } }

        //une animation
        AnimatedSprite _animatedIA;

        //timeToShoot -> permet d'attendre un peu avant de tirer comme un beau gosse
        public int timeToShoot = 0;

        //CoolDown -> Savoir s'il a tirer ou non, fire serait peut etre bon ?
        public bool coolDown = false;

        //nom ennemi plus proche
        string foeName;

        public WeaponsClass _weapons;

        //un tir
        bool shootDirectionLeft = true;
        public bool fire = false;

        int direction = 0;

        Vector2 bestPosition = new Vector2();

        int weaponChoice = 2;

        public Projectile _projectile;

        //A t il deja tire ?
        public bool _alreadyShoot = false;


        //position avec 0 = ne s'anime pas, 1 = droite, 2 = gauche

        public Vector2 Position { get; set; }

        //Life and Name
        public int Life { get; set; }
        public string NickName { get; set; }
        private string teamName;
        SpriteFont font;

        bool jump = false;
        Gravity _gravity;

        #region Collision
        public bool colli = false;

        public bool col_bas = false;
        public bool col_gauche = false;
        public bool col_droite = false;

        public bool col_bas2 = false;
        public bool col_gauche2 = false;
        public bool col_droite2 = false;
        #endregion

        public IA(ContentManager content, GraphicsDevice graphics, int widthScreen, int heightSCreen, Texture2D IALeft, Texture2D IARight, int origineX, string teamName, string nickName, SpriteFont font)
        {
            _gravity = new Gravity(heightSCreen);
            this._widthScreen = widthScreen;
            this._IALeft = IALeft;
            this._IARight = IARight;
            this._heightScreen = heightSCreen;
            _weapons = new WeaponsClass(content, graphics, widthScreen, heightSCreen, IARight);
            _animatedIA = new AnimatedSprite(IARight, IALeft, 2, 2, 2, 2);
            _positionIA = new Vector2(origineX, heightSCreen - IALeft.Height / 4);
            positionRectangle = new Rectangle((int)_positionIA.X, (int)_positionIA.Y, IALeft.Width / 4, IALeft.Height / 4);

            //Life and Name
            Life = 100;
            this.teamName = "[" + teamName + "]";
            NickName = this.teamName + nickName;
            this.font = font;
        }

        public void Init(Texture2D map)
        {
            _positionIA.Y = positionRectangle.Y = 0;
            while (!col_bas && _positionIA.Y < _heightScreen)
            {
                _positionIA.Y += 20;
                positionRectangle.Y += 20;
                col_bas = Collision.Collision.col_bas(new Rectangle(-_widthScreen / 2, 0, map.Width, map.Height), positionRectangle);
                if (_positionIA.Y >= _heightScreen)
                {
                    _positionIA.Y = _heightScreen / 2;
                }
            }
        }

        #region calculs IA
        public int WeaponChoice(Vector2 position)
        {
            if (Play.level == 2)
            {
                foreach (var item in posPlayers)
                {
                    bestPosition.X += (int)item.X + 800;
                }
                bestPosition.X = bestPosition.X / posPlayers.Count - 800;
                bestPosition.Y = _positionIA.Y;
                //Mouton
                return 0;
            }

            else
            {
                //kamea
                return 2;
            }
        }

        //Fonction permettant de definir le joueur le plus proche
        public Vector2 GetBetterPlayerAdvanced(List<Player> player)
        {
            Vector2 position = new Vector2();
            position.X = 2000;
            for (int i = 0; i < player.Count; i++)
            {
                if (Math.Abs(player[i]._positionPlayer.X - _positionIA.X) < Math.Abs(position.X))
                {
                    position.X = player[i]._positionPlayer.X;
                    position.Y = player[i]._positionPlayer.Y;
                    foeName = player[i].nickName;
                    //player.RemoveAt(i);
                }
            }
            weaponChoice = AroundYou(player, position);
            return position;
        }


        //Fonction permettant de definir le joueur le plus proche
        public Vector2 GetBetterPlayerBasic(List<Player> player)
        {
            Vector2 position = new Vector2();
            position.X = 2000;
            for (int i = 0; i < player.Count; i++)
            {
                if (Math.Abs(player[i]._positionPlayer.X - _positionIA.X) < Math.Abs(position.X))
                {
                    position.X = player[i]._positionPlayer.X;
                    position.Y = player[i]._positionPlayer.Y;
                    foeName = player[i].nickName;
                    //player.RemoveAt(i);
                }
            }
            weaponChoice = 2;
            return position;
        }

        private int AroundYou(List<Player> player, Vector2 currentTarget)
        {
            //50 = largeur mouton...
            posPlayers = new List<Vector2>();
            foreach (var item in player)
            {
                if (Math.Abs(Math.Abs(item._positionPlayer.X) - Math.Abs(currentTarget.X)) <= 50)
                {
                    posPlayers.Add(item._positionPlayer);
                }
            }
            return WeaponChoice(_positionIA);
        }

        #endregion

        public void Update(Vector2 positionPlayer, int r, GameTime gameTime)
        {
            jump = false;
            if (Math.Abs(positionPlayer.X - _positionIA.X) > r)
            {
                if (positionPlayer.X < _positionIA.X /*&& CollisionRectangles.collision(positionRectangle, new Rectangle((int)positionPlayer.X, (int)positionPlayer.Y, positionRectangle.Width, positionRectangle.Height))*/)
                {
                    if (col_bas)
                        _positionIA.Y -= 3;
                    else if (!col_bas2)
                        _positionIA.Y += 3;

                    if (!col_gauche)
                        _positionIA.X -= 6;
                    else
                    {
                        jump = true;
                    }
                    direction = 2;
                }
                else if (positionPlayer.X > _positionIA.X/* && CollisionRectangles.collision(positionRectangle, new Rectangle((int)positionPlayer.X, (int)positionPlayer.Y, positionRectangle.Width, positionRectangle.Height))*/)
                {
                    if (col_bas)
                        _positionIA.Y -= 3;
                    else if (!col_bas2)
                        _positionIA.Y += 3;
                    if(!col_droite)
                        _positionIA.X += 6;
                    else
                    {
                        jump = true;
                    }
                    direction = 1;
                }
                _animatedIA.Update(_positionIA, direction);
            }
            else
            {
                if (positionPlayer.X < _positionIA.X)
                {
                    if (timeToShoot >= 80 && timeToShoot < 500)
                    {
                        fire = true;
                        shootDirectionLeft = true;
                    }
                }
                else
                {
                    if (timeToShoot >= 80 && timeToShoot < 500)
                    {
                        fire = true;
                        shootDirectionLeft = false;
                    }
                }
                _alreadyShoot = false;
            }
            if (timeToShoot < 80)
            {
                timeToShoot++;
            }
            else timeToShoot = 500;

            switch (weaponChoice)
            {
                case 0:
                    _weapons.SheepIA(bestPosition, _positionIA, fire, gameTime);
                    //mouton
                    break;
                case 1:
                    _weapons.BazookaIA(positionPlayer, _positionIA, fire);
                    //bazooka
                    break;

                case 2:
                    _weapons.KameaIA(positionPlayer, _positionIA, fire);
                    //Kamea
                    break;
                
                default :
                    break;
            }
            //_projectile.UpdateIA(_positionIA, shootDirectionLeft, fire, _alreadyShoot, 0);
            Position = _positionIA;
            _alreadyShoot = true;
            fire = false;

            _positionIA.Y = _gravity.positionY(0.03f, jump, col_bas, _positionIA.Y);
            
            positionRectangle.X = (int)_positionIA.X;
            positionRectangle.Y = (int)_positionIA.Y;


            //Vie baisse si blesse

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (_weapons._tir)
            {
                switch (weaponChoice)
                {
                    case 0:
                        _weapons.DrawSheepIA(spriteBatch, shootDirectionLeft);
                        //mouton
                        break;
                    case 1:
                        _weapons.DrawBazookaIA(spriteBatch);
                        //bazooka
                        break;

                    case 2:
                        _weapons.DrawKameaIA(spriteBatch);
                        //Kamea
                        break;

                    default:
                        break;
                }
            }
            _animatedIA.Draw(spriteBatch, _positionIA);

            //Display life and nickname
            spriteBatch.DrawString(font, Life.ToString(), new Vector2(_positionIA.X + _IALeft.Width / 4 - (font.MeasureString(Life.ToString()).X / 2), _positionIA.Y - font.MeasureString(Life.ToString()).Y * 2), Color.Red);
            spriteBatch.DrawString(font, NickName, new Vector2(_positionIA.X + _IALeft.Width / 4 - (font.MeasureString(NickName).X / 2), _positionIA.Y - font.MeasureString(NickName).Y * 3), Color.Red);

        }
        public void DrawFire(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, NickName + " ose tirer sur " + foeName + " mais que va t-il se passer ?!", new Vector2(-font.MeasureString(NickName + " ose tirer sur " + foeName + " mais que va t-il se passer ?!").X / 2, 0), Color.White);
        }
    }
}
