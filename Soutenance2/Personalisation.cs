﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Soutenance2
{
    public partial class Personalisation : Form
    {
        string fileTexture = "Textures/player.png";
        //public static string _fileTexturePlayer { get {return fileTexture;} }
        string previousFile = "../../../../Soutenance2Content/", origineLeft = "Textures/playerLeftOrg", origineRight = "Textures/playerRightOrg", origine = "Textures/playerOrg";
        int i = 0, k = 0;
        List<string> paths = new List<string>()
            {
                "Textures/none.png", "Textures/chapeau.png", "Textures/chapeau2.png"
            };
        Color color = new Color();
        bool colorChange = false, reset = false;
        delegate void saveAction(ref Bitmap imageBase, Bitmap imageAdd, int x, int y, int decalage);

        public Personalisation()
        {
            InitializeComponent();
            if (Options._english)
            {
                groupBoxNames.Text = "Names personalisation";
                labelTeamName.Text = "Team name";
                labelNamePlayers.Text = "Player names";
                buttonSaveNames.Text = "Save";
                buttonLoad.Text = "Load";
                buttonReset.Text = "Reset";
                buttonSaveView.Text = "Save";
                buttonPreView.Text = "Previous";
                buttonNextView.Text = "Next";
                buttonColor.Text = "Color";
                buttonApplyColor.Text = "Apply";
            }
        }

        private void Personalisation_Load(object sender, EventArgs e)
        {

        }

        private void buttonSaveNames_Click(object sender, EventArgs e)
        {
            if (textBoxTeamName.Text != "" && textBoxNamePl1.Text != "" && textBoxNamePl2.Text != "" && textBoxNamePl3.Text != "")
            {
                StreamWriter writer = new StreamWriter("Names.txt");
                writer.Write(textBoxTeamName.Text + "\n" + textBoxNamePl1.Text + "\n" + textBoxNamePl2.Text + "\n" + textBoxNamePl3.Text);
                writer.Close();
                if (Options._english)
                    labelErreurName.Text = "Save succeeded !";
                else
                    labelErreurName.Text = "Enregistrement reussi!";
            }
            else
            {
                if (Options._english)
                    labelErreurName.Text = "One name or more is invalid..";
                else
                    labelErreurName.Text = "Au moins un nom est invalide..";
            }
        }

        private void groupBoxNames_Enter(object sender, EventArgs e)
        {

        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = previousFile + fileTexture;
            buttonSaveView.Enabled = true;
            buttonNextView.Enabled = true;
            buttonColor.Enabled = true;
            buttonReset.Enabled = true;
            colorChange = false;
        }

        private void buttonNextView_Click(object sender, EventArgs e)
        {
            Bitmap bmpBase = (Bitmap)Image.FromFile(previousFile + fileTexture)/*(Bitmap)pictureBox1.Image*/;
            i++;
            if (i >= paths.Count) i = 0;
            Bitmap bmpAdd = (Bitmap)Image.FromFile(previousFile + paths[i]);
            pictureBox1.Image = insert(bmpBase, bmpAdd);
            buttonSaveView.Enabled = true;
            buttonPreView.Enabled = true;
            colorChange = false;
            bmpBase.Dispose();
        }

        private void buttonPreView_Click(object sender, EventArgs e)
        {
            Bitmap bmpBase = (Bitmap)Image.FromFile(previousFile + fileTexture);
            i--;
            if (i < 0) i = paths.Count - 1;
            Bitmap bmpAdd = (Bitmap)Image.FromFile(previousFile + paths[i]);
            pictureBox1.Image = insert(bmpBase, bmpAdd);
            colorChange = false;
            bmpBase.Dispose();
        }

        private void buttonSaveView_Click(object sender, EventArgs e)
        {
            if (Options._english)
                labelSaveView.Text = "Saving";
            else
                labelSaveView.Text = "Sauvegarde";
            Bitmap imageBaseRight = new Bitmap(previousFile + origineRight);
            Bitmap imageBaseLeft = new Bitmap(previousFile + origineLeft);

            Image img = pictureBox1.Image;
            img.Save("tmp.png");
            pictureBox1.Image.Dispose();
            //pictureBox1.Dispose();
            img = Image.FromFile("tmp.png");
            fileTexture = "Textures/player" + k + ".png";
            //if (k > 1) File.Delete(previousFile + "Textures/player" + (k - 2) + ".png");
            img.Save(previousFile + fileTexture);
            pictureBox1.Image = Image.FromFile(previousFile + fileTexture);
            img.Dispose();
            k++;
            /*pictureBox1.Dispose();
            pictureBox1.Image = Image.FromFile(previousFile + "Textures/map.png");
            new Bitmap(previousFile + fileTexture).Save(previousFile + "Textures/player.png");*/
            //new Bitmap(previousFile + "Textures/playerTmp.png").Save(previousFile + fileTexture);
            /*File.Delete(previousFile + fileTexture);
            File.Copy(previousFile + "Textures/playerTmp.png", previousFile + fileTexture);
            pictureBox1.ImageLocation = previousFile + fileTexture;*/
            //pictureBox1.Image.Save(previousFile + fileTexture);

            if (colorChange)    //Sauvegarde de la couleur
            {
                colorToSave(ref imageBaseLeft);
                colorToSave(ref imageBaseRight);
            }
            else        //Sauvegarde d'un chapeau
            {
                Bitmap bmpAdd = (Bitmap)Image.FromFile(previousFile + paths[i]);
                //Image Left
                insertToSave(ref imageBaseLeft, bmpAdd, imageBaseLeft.Width / 4 - bmpAdd.Width / 2, 0, -10);
                insertToSave(ref imageBaseLeft, bmpAdd, 3 * imageBaseLeft.Width / 4 - bmpAdd.Width / 2, 0, -10);
                insertToSave(ref imageBaseLeft, bmpAdd, imageBaseLeft.Width / 4 - bmpAdd.Width / 2, imageBaseLeft.Height / 2, -10);
                insertToSave(ref imageBaseLeft, bmpAdd, 3 * imageBaseLeft.Width / 4 - bmpAdd.Width / 2, imageBaseLeft.Height / 2, -10);
                //Image Right
                insertToSave(ref imageBaseRight, bmpAdd, imageBaseRight.Width / 4 - bmpAdd.Width / 2, 0, 10);
                insertToSave(ref imageBaseRight, bmpAdd, 3 * imageBaseRight.Width / 4 - bmpAdd.Width / 2, 0, 10);
                insertToSave(ref imageBaseRight, bmpAdd, imageBaseRight.Width / 4 - bmpAdd.Width / 2, imageBaseRight.Height / 2, 10);
                insertToSave(ref imageBaseRight, bmpAdd, 3 * imageBaseRight.Width / 4 - bmpAdd.Width / 2, imageBaseRight.Height / 2, 10);
            }

            origineLeft = "Textures/playerLeftTmp" + k + ".png";
            origineRight = "Textures/playerRightTmp" + k + ".png";
            imageBaseLeft.Save(previousFile + origineLeft);
            imageBaseRight.Save(previousFile + origineRight);
            imageBaseLeft.Save(previousFile + "Textures/playerLeft.png");
            imageBaseRight.Save(previousFile + "Textures/playerRight.png");
            imageBaseLeft.Dispose();
            imageBaseRight.Dispose();
            if (k > 2) File.Delete(previousFile + "Textures/playerLeftTmp" + (k - 2) + ".png"); File.Delete(previousFile + "Textures/playerRightTmp" + (k - 2) + ".png");
            if (Options._english)
                labelSaveView.Text = "Saved !";
            else
                labelSaveView.Text = "Sauvegardé !";
            reset = false;
            buttonSaveView.Enabled = false;
        }

        private void insertToSave(ref Bitmap imageBase, Bitmap imageAdd, int x, int y, int decalage)
        {
            for (int j = y; j < y + imageAdd.Height; j++)
            {
                for (int i = x; i < x + imageAdd.Width; i++)
                {
                    if (imageAdd.GetPixel(i - x, j - y).A == 255)
                        imageBase.SetPixel(i + decalage, j, imageAdd.GetPixel(i - x, j - y));
                }
            }
        }

        private void colorToSave(ref Bitmap imageBase)
        {
            int r, g, b;
            for (int j = 0; j < imageBase.Height; j++)
            {
                for (int i = 0; i < imageBase.Width; i++)
                {
                    r = imageBase.GetPixel(i, j).R;
                    g = imageBase.GetPixel(i, j).G;
                    b = imageBase.GetPixel(i, j).B;
                    if ((r + g + b) / 3 < (color.R + color.B + color.G) / 3 && imageBase.GetPixel(i, j).A >= 50)
                        imageBase.SetPixel(i, j, color);
                }
            }
            colorChange = false;
        }

        private Image insert(Bitmap imageBase, Bitmap imageAdd)
        {
            Bitmap result = new Bitmap(imageBase);

            for (int j = 0; j < imageAdd.Height; j++)
            {
                for (int i = imageBase.Width / 2 - (imageAdd.Width) / 2; i < imageBase.Width / 2 + (imageAdd.Width) / 2; i++)
                {
                    if (imageAdd.GetPixel(i - (imageBase.Width / 2 - imageAdd.Width / 2), j).A == 255)
                        result.SetPixel(i, j, imageAdd.GetPixel(i - (imageBase.Width / 2 - imageAdd.Width / 2), j));
                }
            }
            return (Image)result;
        }

        private void buttonColor_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            color = colorDialog1.Color;
            buttonApplyColor.Enabled = true;
        }

        private void buttonApplyColor_Click(object sender, EventArgs e)
        {
            Bitmap bmp = (Bitmap)Image.FromFile(pictureBox1.ImageLocation);
            int r, g, b;
            for (int j = 0; j < /*pictureBox1.Image.Height*/bmp.Height; j++)
            {
                for (int i = 0; i < /*pictureBox1.Image.Width*/bmp.Width; i++)
                {
                    r = bmp.GetPixel(i, j).R;
                    g = bmp.GetPixel(i, j).G;
                    b = bmp.GetPixel(i, j).B;
                    if ((r + g + b) / 3 < (color.R + color.B + color.G) / 3 && bmp.GetPixel(i, j).A >= 50)
                        bmp.SetPixel(i, j, color);
                }
            }
            pictureBox1.Image = bmp;
            colorChange = true;
            buttonSaveView.Enabled = true;
            buttonApplyColor.Enabled = false;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            origineRight = "Textures/playerRightOrg";
            origineLeft = "Textures/playerLeftOrg";
            fileTexture = "Textures/player.png";
            new Bitmap(previousFile + origine).Save(previousFile + fileTexture);
            new Bitmap(previousFile + origineLeft).Save(previousFile + "Textures/playerLeft.png");
            new Bitmap(previousFile + origineRight).Save(previousFile + "Textures/playerRight.png");
            pictureBox1.ImageLocation = previousFile + fileTexture;
            buttonSaveView.Enabled = true;
            buttonNextView.Enabled = true;
            buttonColor.Enabled = true;
            colorChange = false;
            i = 0;
            reset = true;
        }
    }
}
