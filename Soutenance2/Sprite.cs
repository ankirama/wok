﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;
using System;

//Classe pour creer texture et tout
namespace Soutenance2
{
    class Sprite
    {
        //Permet chargement sprite
        ContentManager content;

        protected Vector2 _position;
        protected Rectangle _rectangle;
        protected Texture2D _texture;
        protected Vector2 _speed;
        protected bool _active;

        Stream file;

        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public Rectangle Rectangle
        {
            get { return _rectangle; }
            set { _rectangle = value; }
        }

        public Texture2D Texture
        {
            get { return _texture; }
            set { _texture = value; }
        }

        public Vector2 Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public int Width
        {
            get { return _texture.Width; }
        }

        public int Height
        {
            get { return _texture.Height; }
        }

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        //Utilise juste pour pouvoir faire un Content.Load<>
        public ContentManager Content
        {
            get { return content; }
        }

        public Sprite(ContentManager c)
        {
            content = c;
            _position = Vector2.Zero;
            _speed = Vector2.One;
            _active = true;
        }

        public Sprite(ContentManager c, int div)
        {
            content = c;
            _position = Vector2.Zero;
            _speed = Vector2.One;
            _active = true;

        }
        public Sprite(ContentManager c, Vector2 position)
        {
            content = c;
            _position = position;
            _active = true;
        }

        public virtual void Initialize()
        {
            _active = true;
        }

        public virtual void LoadContent(string textureName)
        {
            _texture = Content.Load<Texture2D>(textureName);
            _rectangle = new Rectangle(0, 0, _texture.Width / 2, _texture.Height / 2);
        }

        public virtual void UnloadContent()
        {
            if (_texture != null)
                _texture.Dispose();
            try
            {
                file.Close();
                file.Dispose();
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (_active)
                spriteBatch.Draw(_texture, _position, Color.White);
        }

        public virtual void DrawRec(SpriteBatch spriteBatch)
        {
            if (_active)
                spriteBatch.Draw(_texture, _rectangle, Color.White);
        }

        public void Load(string path, GraphicsDevice graphics)
        {
            try
            {
                file.Close();
                file.Dispose();
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            file = File.Open(path, FileMode.Open);

            _texture = Texture2D.FromStream(graphics, file);
        }
    }
}
