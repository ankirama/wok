﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Soutenance2
{
    class WeaponsClass
    {
        #region Variables globales.. + initialisation
        Button bazookaButton, sheepButton, kameaButton;
        Projectile bazookaProjectile, sheepProjectile, kameaProjectile;
        Texture2D bazookaG, bazookaD, sheepG, sheepD, kameaG, kameaD, background, projectileBazooka, powerBar;
        SoundEffect bazookaSound, sheepSound, kameaSound;
        Rectangle positionBar, positionProjectileBazooka, positionProjectileSheep;
        public bool troll = false;
        bool isActiveSheep = false;

        GraphicsDevice _graphics;

        public bool _alreadyShoot = false, alreadyMulti = false;
        public int _weaponNbr, _width, _mouseX, _mouseY;
        int _direction;
        Vector2 positionProjectile;
        SpriteBatch _spriteBatch;
        public bool _tir = false;


        public Rectangle _position = new Rectangle();
        int decalX, decalY;
        int textureHeight, textureWidth;

        public bool alreadyPress = false;

        public bool touch = false;

        delegate void selectWeapon(Vector2 positionPlayer);
        selectWeapon selectFct;
        delegate void drawShoot(SpriteBatch spriteBatch, Vector2 positionProjectile);
        drawShoot DrawProjectile;
        delegate void selectWeaponMulti(Vector2 positionPlayer, Network network);
        selectWeaponMulti selectFctMulti;
        public Rectangle positionProjectileKamea;

        //Particles
        ParticleGenerator sheepParticles;
        public bool sheepActive = false;
        int count = 0;

        public int timer = 0;

        //Degat inflige par l'arme
        public int _dommage = 0;

        Rectangle pperso_colgauche;
        Rectangle pperso_colbas2;
        Rectangle pperso_colbas;
        Rectangle pperso_coldroit;
        bool col_bas2 = false, col_bas = false, col_droite = false, col_gauche = false;
        Gravity _gravity;



        public WeaponsClass(ContentManager content, GraphicsDevice graphics, /*Texture2D player, *//*Vector2 positionPlayer,*/ int widthScreen, int heightScreen, Texture2D texturePlayer)
        {
            _gravity = new Gravity(heightScreen);
            textureHeight = texturePlayer.Height;
            textureWidth = texturePlayer.Width;

            this._graphics = graphics;
            //Textures
            background = content.Load<Texture2D>("Weapons/menuArmes");
            sheepParticles = new ParticleGenerator(content.Load<Texture2D>("Weapons/laineParticle"), widthScreen, 0.02f);

            bazookaG = content.Load<Texture2D>("Weapons/bazookaG");
            bazookaD = content.Load<Texture2D>("Weapons/bazookaD");
            sheepG = content.Load<Texture2D>("Weapons/moutonAttackG"); //nom changer pour tests
            sheepD = content.Load<Texture2D>("Weapons/moutonAttack2"); //ici aussi
            kameaD = kameaG = content.Load<Texture2D>("Weapons/projectile");
            projectileBazooka = content.Load<Texture2D>("Weapons/bazookaProjectile");
            powerBar = content.Load<Texture2D>("Weapons/powerbar");

            //Boutons pour le menu
            bazookaButton = new Button(content.Load<Texture2D>("Weapons/bazooka"), graphics, widthScreen);
            sheepButton = new Button(content.Load<Texture2D>("Weapons/mouton"), graphics, widthScreen);
            kameaButton = new Button(content.Load<Texture2D>("Weapons/kamea"), graphics, widthScreen);
            bazookaButton.setPosition(new Vector2(-75, background.Height / 2));
            sheepButton.setPosition(new Vector2(0, background.Height / 2));
            kameaButton.setPosition(new Vector2(75, background.Height / 2));

            sheepSound = content.Load<SoundEffect>("Songs/sheepSound");
            kameaSound = content.Load<SoundEffect>("Songs/kameaSound");
            bazookaSound = content.Load<SoundEffect>("songs/gunSound");

            this._width = widthScreen;

            Collision.Collision.KameaColor = new Color[kameaG.Width * kameaG.Height];
            kameaG.GetData<Color>(Collision.Collision.KameaColor);
            Collision.Collision.KameaColor = new Color[sheepG.Width * sheepG.Height];
            sheepG.GetData<Color>(Collision.Collision.KameaColor);
            Collision.Collision.KameaColor = new Color[bazookaG.Width * bazookaG.Height];
            bazookaG.GetData<Color>(Collision.Collision.KameaColor);

            positionProjectileKamea.Width = kameaG.Width / 20;
            positionProjectileKamea.Height = kameaG.Height / 20;
            positionBar = new Rectangle(0, heightScreen/* - powerBar.Height*/, 0, powerBar.Height);

            positionProjectileSheep = new Rectangle((int)positionProjectile.X, (int)positionProjectile.Y, sheepG.Width, sheepG.Height);

            pperso_colbas = new Rectangle(
                (int)(positionProjectileSheep.X + 30),
                (int)(positionProjectileSheep.Y + 5),
                (int)positionProjectileSheep.Width / 2,
                (int)positionProjectileSheep.Height);
            pperso_colbas2 = new Rectangle(
                (int)(positionProjectileSheep.X + 30),
                (int)(positionProjectileSheep.Y + 10),
                (int)positionProjectileSheep.Width / 2,
                (int)positionProjectileSheep.Height);

            pperso_colgauche = new Rectangle(
                (int)(positionProjectileSheep.X + 15),
                (int)(positionProjectileSheep.Y + 5),
                (int)positionProjectileSheep.Width / 3,
                (int)positionProjectileSheep.Height / 2 + 25);

            pperso_coldroit = new Rectangle(
                (int)(positionProjectileSheep.X + 50),
                (int)(positionProjectileSheep.Y + 5),
                (int)positionProjectileSheep.Width / 3,
                (int)positionProjectileSheep.Height / 2 + 25);

            try
            {
                if (Collision.Collision.KameaColor.Length != 0)
                {
                    
                }
                if (Collision.Collision.SheepColor.Length != 0)
                {
                    
                }
                if (Collision.Collision.BazookaColor.Length != 0)
                {
                    
                }
            }
            catch
            {
                Collision.Collision.KameaColor = new Color[kameaG.Width * kameaG.Height];
                kameaG.GetData<Color>(Collision.Collision.KameaColor);
                Collision.Collision.SheepColor = new Color[sheepD.Width * sheepD.Height];
                sheepD.GetData<Color>(Collision.Collision.SheepColor);
                Collision.Collision.BazookaColor = new Color[bazookaG.Width * bazookaG.Height];
                bazookaG.GetData<Color>(Collision.Collision.BazookaColor);
            }

        }

        #endregion

        #region Joueur Humain
        public void Update(GameTime gameTime, MouseState mouse, Vector2 positionPlayer, int direction)
        {
            mouse = Mouse.GetState();
            //if (_direction != 0)
            //_direction = _direction;

            //si un tir est deja lance, on ne s'occupe pas du menu
            if (!_alreadyShoot)
            {
                //_alreadyShoot = true;
                //mise a jour des delegate Tir et Draw selon le bouton clique
                if (bazookaButton.isClicked)
                {
                    selectFct = Bazooka;
                    _mouseX = mouse.X;
                    _mouseY = mouse.Y;
                    Bazooka(positionPlayer);
                    DrawProjectile = DrawBazooka;
                }
                else if (sheepButton.isClicked)
                {
                    selectFct = Sheep;
                    Sheep(positionPlayer);
                    DrawProjectile = DrawSheep;
                }
                else if (kameaButton.isClicked)
                {
                    selectFct = Kamea;
                    Kamea(positionPlayer);
                    DrawProjectile = DrawKamea;
                }
                /*else
                {
                    //selectFct = Sheep;
                    DrawProjectile = DrawSheep;
                }*/
                //_alreadyShoot = false;
                _tir = false;
            }

            else
            {
                selectFct(positionPlayer);
            }

            if (sheepActive)
            {
                sheepParticles.UpdateRandom(gameTime, _graphics, positionProjectile);
                count++;
                if (count > 20)
                {
                    count = 0;
                    sheepActive = false;
                    _alreadyShoot = false;
                }
            }


            //MAJ des boutons
            bazookaButton.UpdateWeapons(mouse);
            sheepButton.UpdateWeapons(mouse);
            kameaButton.UpdateWeapons(mouse);
        }

        public void Bazooka(Vector2 positionPlayer)
        {
            _dommage = 20;
            if (!_alreadyShoot)
            {
                _alreadyShoot = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !_tir)
            {
                _tir = true;
                decalX = (int)(Mouse.GetState().X - _width / 2 - positionPlayer.X /*+ _width / 2 */+ textureWidth / 4);
                decalY = (int)(Mouse.GetState().Y - positionPlayer.Y - textureHeight / 4);
                /*positionProjectileBazooka.X += decalX / 100;
                positionProjectileBazooka.Y += decalY / 100;*/
                bazookaSound.Play();
            }

            if (_tir)
            {
                positionProjectileBazooka.X += decalX / 20/* / (_width / 10)*/;
                positionProjectileBazooka.Y += decalY / 20/* / (_width / 10)*/;
                if (positionProjectileBazooka.X > _width / 2 || positionProjectileBazooka.X < -_width / 2 || touch/*|| positionProjectileBazooka.Y < 0 || positionProjectileBazooka.Y > 900*/)
                {
                    positionProjectileBazooka = new Rectangle((int)positionPlayer.X + textureWidth / 2 - projectileBazooka.Width / 2, (int)positionPlayer.Y + textureHeight / 2 - projectileBazooka.Height / 2, projectileBazooka.Width, projectileBazooka.Height);
                    _alreadyShoot = false;
                    _tir = false;
                    touch = false;
                }
            }
            else
            {
                //Si on est pas en tir, MaJ de la powerbar et de la position du projectile
                positionProjectileBazooka = new Rectangle((int)positionPlayer.X + textureWidth / 8 - projectileBazooka.Width / 2, (int)positionPlayer.Y + textureHeight / 4 - projectileBazooka.Height, projectileBazooka.Width / 1, projectileBazooka.Height / 1);
                positionBar.X = (int)positionPlayer.X + textureWidth / 8 - powerBar.Width / 2;
                positionBar.Y = (int)positionPlayer.Y - 3 * powerBar.Height;
                if ((int)Math.Abs(Mouse.GetState().X - _width / 2 - positionPlayer.X - textureWidth / 4) + (int)Math.Abs(Mouse.GetState().Y - positionPlayer.Y - textureHeight / 4) / 4 / powerBar.Width < powerBar.Width)
                {
                    positionBar.Width = (int)Math.Abs(Mouse.GetState().X - _width / 2 - positionPlayer.X + textureWidth / 4) + (int)Math.Abs(Mouse.GetState().Y - positionPlayer.Y - textureHeight / 4) / 4 / powerBar.Width;
                }
                else
                    positionBar.Width = powerBar.Width;
            }
            _position = positionProjectileBazooka;
            //Console.WriteLine("{0} {1}",/* (1/10) */ (Mouse.GetState().X - _width / 2 - positionPlayer.X /*+ _width / 2 */- textureWidth / 4) / 10/* / _width*/, /*(1/10) **/ (Mouse.GetState().Y - positionPlayer.Y - textureHeight / 4) /10/*/ _width*/);
        }

        public void Sheep(Vector2 positionPlayer)
        {
            _dommage = 30;

            #region Collision
            pperso_colbas.X = (int)positionProjectile.X + 30;
            pperso_colbas.Y = (int)positionProjectile.Y + 5;

            pperso_colbas2.X = (int)positionProjectile.X + 30;
            pperso_colbas2.Y = (int)positionProjectile.Y + 10;

            pperso_colgauche.X = (int)positionProjectile.X + 15;
            pperso_colgauche.Y = (int)positionProjectile.Y + 5;

            pperso_coldroit.X = (int)(positionProjectile.X + 50);
            pperso_coldroit.Y = (int)(positionProjectile.Y + 5);

            col_bas2 = Collision.Collision.col_bas(
               sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
                pperso_colbas);

            col_bas = Collision.Collision.col_bas(
                sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
                pperso_colbas);

            col_gauche = Collision.Collision.col_droite(
               sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
               pperso_colgauche);

            col_droite = Collision.Collision.col_droite(
               sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
               pperso_coldroit);

            #endregion

            //Premier appel => initialisation positions,...
            if (!_alreadyShoot)
            {
                sheepActive = false;
                sheepSound.Play();
                _alreadyShoot = true;
                positionProjectile.X = positionPlayer.X;
                positionProjectile.Y = positionPlayer.Y - sheepD.Height / 2;
                _direction = 1;
                _tir = false;
            }
            //Directions
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                _direction = 1;
                if (col_bas)
                {
                    positionProjectile.Y -= 4;
                }

                if (!col_gauche)
                {
                    positionProjectile.X -= 4;
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                _direction = 2;
                if (col_bas)
                {
                    positionProjectile.Y -= 4;
                }

                if (!col_droite)
                {
                    positionProjectile.X += 4;
                }
            }

            positionProjectile.Y = _gravity.positionY(0.03f, Keyboard.GetState().IsKeyDown(Keys.Space), col_bas, positionProjectile.Y);
            //Explosion

            _position = new Rectangle((int)positionProjectile.X, (int)positionProjectile.Y, sheepD.Width, sheepD.Height);
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !alreadyPress)
            {
                if (Collision.Collision.IntersectPixels(new Rectangle((int)positionProjectile.X, (int)positionProjectile.Y, sheepD.Width, sheepD.Height), Collision.Collision.SheepColor, new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height), Collision.Collision.TerrainColor))
                {
                    Destruction.Destroy(new Vector2(_position.X + 800 + sheepD.Width / 2, _position.Y + sheepD.Height / 2), sheepD.Height, sheepD.Width);
                }
                sheepActive = true;
                alreadyPress = true;
                _direction = 0;

                sheepSound.Play();
                _tir = true;
            }

            positionProjectileSheep = _position;

        }

        public void Kamea(Vector2 positionPlayer)
        {
            _dommage = 40;
            //Premier appel => initialisation positions,...
            if (!_alreadyShoot)
            {
                touch = false;
                _alreadyShoot = true;
                /*positionProjectile.X = positionPlayer.X;
                positionProjectile.Y = positionPlayer.Y - sheepD.Height / 2;*/
                //_direction = 1;
                //_tir = false;
            }
            //Directions
            if (!_tir)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Left))
                {
                    _direction = 2;
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.Right))
                {
                    _direction = 1;
                }
                //Explosion
                if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    positionProjectileKamea.X = (int)positionPlayer.X;
                    positionProjectileKamea.Y = (int)positionPlayer.Y + 25;
                    _tir = true;
                    kameaSound.Play();
                }
            }
            else
            {
                if (Collision.Collision.IntersectPixels(positionProjectileKamea, Collision.Collision.KameaColor, new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height), Collision.Collision.TerrainColor))
                {
                    Destruction.Destroy(new Vector2(positionProjectileKamea.X + _width / 2, positionProjectileKamea.Y), 40, 40);
                }
                else if (positionProjectileKamea.X > _width / 2 || positionProjectileKamea.X < -_width / 2 || touch)
                {
                    _alreadyShoot = false;
                    _tir = false;
                    touch = false;

                }
                else
                {
                    if (_direction == 2)
                        positionProjectileKamea.X -= 10;
                    else
                        positionProjectileKamea.X += 10;
                }
            }
            _position = positionProjectileKamea;
        }
        #endregion
        #region IA
        public void BazookaIA(Vector2 positionAdv, Vector2 positionIA, bool press/*, bool alreadyShoot*/)
        {
            DrawProjectile = DrawBazooka;
            _dommage = 20;
            if (!press)
            {
                decalX = (int)(positionAdv.X - _width / 2 - positionIA.X + textureWidth / 4);
                decalY = (int)(positionAdv.Y - positionIA.Y - textureHeight / 4);
                positionProjectileBazooka.X = (int)positionIA.X;
                positionProjectileBazooka.Y = (int)positionIA.Y;
            }
            else
            {
                if (!_alreadyShoot)
                {
                    positionProjectileBazooka.X = (int)positionIA.X;
                    positionProjectileBazooka.Y = (int)positionIA.Y;
                    _alreadyShoot = true;
                    bazookaSound.Play();
                }
                else
                {
                    positionProjectileBazooka.X += decalX / 20;
                    positionProjectileBazooka.Y += decalY / 20;
                }

                if (positionProjectileBazooka.X > _width / 2 || positionProjectileBazooka.X < -_width / 2 || touch)
                {
                    _alreadyShoot = false;
                }
            }

            _position = positionProjectileBazooka;
        }

        public void SheepIA(Vector2 positionAdv, Vector2 positionIA, bool press, GameTime gameTime)
        {
            _dommage = 30;
            #region Collision
            pperso_colbas.X = (int)positionProjectile.X + 30;
            pperso_colbas.Y = (int)positionProjectile.Y + 5;

            pperso_colbas2.X = (int)positionProjectile.X + 30;
            pperso_colbas2.Y = (int)positionProjectile.Y + 10;

            pperso_colgauche.X = (int)positionProjectile.X + 15;
            pperso_colgauche.Y = (int)positionProjectile.Y + 5;

            pperso_coldroit.X = (int)(positionProjectile.X + 50);
            pperso_coldroit.Y = (int)(positionProjectile.Y + 5);

            col_bas2 = Collision.Collision.col_bas(
               sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
                pperso_colbas);

            col_bas = Collision.Collision.col_bas(
                sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
                pperso_colbas);

            col_gauche = Collision.Collision.col_droite(
               sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
               pperso_colgauche);

            col_droite = Collision.Collision.col_droite(
               sheepG,
               positionProjectileSheep,
               Play.map.Texture,
               new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height),
               pperso_coldroit);

            #endregion
            DrawProjectile = DrawSheep;
            //Premier appel -> placement mouton et sons
            if (!_alreadyShoot)
            {
                touch = false;
                _alreadyShoot = true;
                _tir = false;
            }
            if (!_tir)
            {
                if (press)
                {
                    positionProjectile.X = positionIA.X;
                    positionProjectile.Y = positionIA.Y - sheepG.Height / 2;
                    _tir = true;
                }
            }
            else
            {
                if (positionProjectile.X <= 230 && positionProjectile.X >= 200)
                {
                    if (Collision.Collision.IntersectPixels(new Rectangle((int)positionProjectile.X, (int)positionProjectile.Y, sheepD.Width, sheepD.Height), Collision.Collision.SheepColor, new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height), Collision.Collision.TerrainColor))
                    {
                        Destruction.Destroy(new Vector2(_position.X + 800 + sheepD.Width / 2, _position.Y + sheepD.Height / 2), sheepD.Height, sheepD.Width);
                    }
                    troll = true; 
                    sheepActive = true;
                    sheepSound.Play();
                    isActiveSheep = false;
                    _tir = false;
                }
                else
                {
                    //Directions
                    if (positionIA.X > positionAdv.X)
                    {
                        _direction = 1;
                        if (col_bas)
                        {
                            positionProjectile.Y -= 4;
                        }

                        if (!col_gauche)
                        {
                            positionProjectile.X -= 4;
                        }
                    }
                    else
                    {
                        _direction = 2;
                        if (col_bas)
                        {
                            positionProjectile.Y -= 4;
                        }

                        if (!col_droite)
                        {
                            positionProjectile.X += 4;
                        }
                    }
                }
            }

            if (sheepActive)
            {
                sheepParticles.UpdateRandom(gameTime, _graphics, positionProjectile);
                count++;
                if (count > 20)
                {
                    count = 0;
                    sheepActive = false;
                    _alreadyShoot = false;
                }
            }

            _position.X = (int)positionProjectile.X;
            _position.Y = (int)positionProjectile.Y;
        }
        private bool isInBoundSheep(Vector2 positionAdv)
        {
            if (positionProjectile.Y > positionAdv.Y - textureHeight / 2 && positionProjectile.Y < positionAdv.Y + (3 / 4) * textureHeight)
            {
                return positionProjectile.X < positionAdv.X || positionProjectile.X > positionProjectile.X - textureWidth;
            }
            else
                return false;
        }

        public void KameaIA(Vector2 positionAdv, Vector2 positionIA, bool press)
        {
            _dommage = 40;
            DrawProjectile = DrawKamea;
            //Premier appel
            if (!_alreadyShoot)
            {
                touch = false;
                _alreadyShoot = true;
            }

            if (!_tir)
            {
                if (press)
                {
                    positionProjectileKamea.X = (int)positionIA.X;
                    positionProjectileKamea.Y = (int)positionIA.Y;
                    _tir = true;
                    kameaSound.Play();
                }
            }
            else
            {
                if (Collision.Collision.IntersectPixels(positionProjectileKamea, Collision.Collision.KameaColor, new Rectangle(-_width / 2, 0, Play.map.Width, Play.map.Height), Collision.Collision.TerrainColor))
                {
                    Destruction.Destroy(new Vector2(positionProjectileKamea.X + _width / 2, positionProjectileKamea.Y), 40, 40);
                }

                else if (positionProjectileKamea.X > _width / 2 || positionProjectileKamea.X < -_width / 2 || touch)
                {
                    _alreadyShoot = false;
                    _tir = false;

                }
                else
                {
                    if (positionIA.X > positionAdv.X)
                        positionProjectileKamea.X -= 10;
                    else
                        positionProjectileKamea.X += 10;
                }
            }

            _position = positionProjectileKamea;
        }
        #endregion
        #region Multi
        public void UpdateMulti(GameTime gameTime, Vector2 positionPlayer, bool shoot, Network network)
        {
            #region Initialisation
            if (network.nbrWeapon != 0)
            {
                if (!_alreadyShoot)
                {
                    _alreadyShoot = true;
                    sheepActive = false;
                    alreadyMulti = false;
                    if (network.nbrWeapon == 1)     //Bazooka
                    {
                        DrawProjectile = DrawBazooka;
                        positionProjectileBazooka.X = (int)positionPlayer.X + textureWidth / 4;
                        positionProjectileBazooka.Y = (int)positionPlayer.Y + textureHeight / 4;
                        _weaponNbr = 1;
                    }
                    if (network.nbrWeapon == 2)     //Sheep
                    {
                        DrawProjectile = DrawSheep;
                        positionProjectile.X = positionPlayer.X;
                        positionProjectile.Y = positionPlayer.Y - sheepD.Height / 2;
                        _weaponNbr = 2;
                    }
                    if (network.nbrWeapon == 3)     //Kamea
                    {
                        DrawProjectile = DrawKamea;
                        positionProjectileKamea.X = (int)positionPlayer.X;
                        positionProjectileKamea.Y = (int)positionPlayer.Y + 25;
                        _weaponNbr = 3;
                    }
                }
            }
            #endregion
            if (_alreadyShoot)
            {
                #region Bazooka
                if (_weaponNbr == 1)     //Bazooka
                {

                    _weaponNbr = 0;
                    alreadyMulti = false;
                    network.nbrWeapon = 0;
                }
                #endregion
                #region Sheep
                else if (_weaponNbr == 2)    //Sheep
                {
                    if (network.sheepLeft)
                    {
                        positionProjectile.X -= 10;
                        _direction = 1;
                        sheepActive = false;
                    }
                    else if (network.sheepRight)
                    {
                        positionProjectile.X += 10;
                        _direction = 2;
                        sheepActive = false;
                    }
                    if (network._isPlayer1)
                    {
                        if (network._shootPlayer2)
                        {
                            sheepActive = true;
                            alreadyMulti = true;
                            sheepSound.Play();
                        }
                    }
                    else
                    {
                        if (network._shoot)
                        {
                            sheepActive = true;
                            alreadyMulti = true;
                            _weaponNbr = 0;
                            sheepSound.Play();
                        }
                    }

                    _position.X = (int)positionProjectile.X;
                    _position.Y = (int)positionProjectile.Y;
                }
                #endregion
                #region Kamea
                else if (_weaponNbr == 3)    //Kamea
                {
                    DrawProjectile = DrawKamea;
                    //Console.WriteLine("Kamea");
                    if (shoot && !alreadyMulti)
                    {
                        positionProjectileKamea.X = (int)positionPlayer.X;
                        positionProjectileKamea.Y = (int)positionPlayer.Y + 25;
                        alreadyMulti = true;
                        _tir = true;
                        if (network._isPlayer1)
                        {
                            if (network.shootDirectionLeft2)
                                _direction = 2;
                            else
                                _direction = 1;
                        }
                        else
                        {
                            if (network.shootDirectionLeft1)
                                _direction = 2;
                            else
                                _direction = 1;
                        }
                    }
                    if (alreadyMulti)
                    {
                        if (_direction == 2)
                            positionProjectileKamea.X -= 10;
                        else
                            positionProjectileKamea.X += 10;

                        if (positionProjectileKamea.X > _width || positionProjectileKamea.X < 0 || touch)
                        {
                            _alreadyShoot = false;
                            touch = false;
                            alreadyMulti = false;
                            _direction = 0;
                            _weaponNbr = 0;
                            _tir = false;
                        }
                    }
                    _position = positionProjectileKamea;
                }
                #endregion
                if (sheepActive)
                {
                    sheepParticles.UpdateRandom(gameTime, _graphics, positionProjectile);
                    count++;
                    if (count > 20)
                    {
                        count = 0;
                        sheepActive = false;
                        _alreadyShoot = false;
                        alreadyMulti = false;
                    }
                }
            }
        }

        public void UpdateMulti(GameTime gameTime, Vector2 positionPlayer, MouseState mouse, int direction, Network network)
        {
            mouse = Mouse.GetState();

            //si un tir est deja lance, on ne s'occupe pas du menu
            if (!_alreadyShoot)
            {
                network.nbrWeapon = 0;
                _weaponNbr = 0;
                network._alreadyShoot = false;
                //mise a jour des delegate Tir et Draw selon le bouton clique
                if (bazookaButton.isClicked)
                {
                    selectFctMulti = BazookaMulti;
                    _mouseX = mouse.X;
                    _mouseY = mouse.Y;
                    BazookaMulti(positionPlayer, network);
                    DrawProjectile = DrawBazooka;
                    network.nbrWeapon = 1;
                    sheepActive = false;
                }
                else if (sheepButton.isClicked)
                {
                    selectFctMulti = SheepMulti;
                    SheepMulti(positionPlayer, network);
                    DrawProjectile = DrawSheep;
                    network.nbrWeapon = 2;
                }
                else if (kameaButton.isClicked)
                {
                    sheepActive = false;
                    selectFctMulti = KameaMulti;
                    KameaMulti(positionPlayer, network);
                    DrawProjectile = DrawKamea;
                    network.nbrWeapon = 3;
                    _weaponNbr = 3;
                }
                _tir = false;
            }
            if (_alreadyShoot || network._alreadyShoot)
            {
                selectFctMulti(positionPlayer, network);
            }
            if (sheepActive)
            {
                sheepParticles.UpdateRandom(gameTime, _graphics, positionProjectile);
                count++;
                if (count > 20)
                {
                    count = 0;
                    sheepActive = false;
                    _alreadyShoot = false;
                    network._alreadyShoot = false;
                    alreadyMulti = false;
                    if (network._isPlayer1)
                        network._shoot = false;
                    else
                        network._shootPlayer2 = false;
                    network.nbrWeapon = 0;
                }
            }

            //MAJ des boutons
            bazookaButton.UpdateWeapons(mouse);
            sheepButton.UpdateWeapons(mouse);
            kameaButton.UpdateWeapons(mouse);
        }

        private void BazookaMulti(Vector2 positionPlayer, Network network)
        {
            _dommage = 40;
            if (!_alreadyShoot)
                _alreadyShoot = true;
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !_tir)
            {
                _tir = true;
                decalX = (int)(Mouse.GetState().X - positionPlayer.X + textureWidth / 4);
                decalY = (int)(Mouse.GetState().Y - positionPlayer.Y - textureHeight / 4);
                _weaponNbr = 0;
                sheepSound.Play();
            }
            if (_tir)
            {
                positionProjectileBazooka.X += decalX / 15;
                positionProjectileBazooka.Y += decalY / 15;
                //conditions a ajouter la pour collisions rectangle
                //Console.WriteLine( "                          " + positionProjectileBazooka.X + " " + positionProjectileBazooka.Y);
                if (positionProjectileBazooka.X > _width || positionProjectileBazooka.X < 0 || touch)
                {
                    positionProjectileBazooka = new Rectangle((int)positionPlayer.X + textureWidth / 2 - projectileBazooka.Width / 2, (int)positionPlayer.Y + textureHeight / 2 - projectileBazooka.Height / 2, projectileBazooka.Width, projectileBazooka.Height);
                    _tir = false;
                    _alreadyShoot = false;
                }
            }
            else
            {
                //Si on est pas en tir, MaJ de la powerbar et de la position du projectile
                positionProjectileBazooka = new Rectangle((int)positionPlayer.X + textureWidth / 4 - projectileBazooka.Width / 2, (int)positionPlayer.Y + textureHeight / 4 - projectileBazooka.Height / 2, projectileBazooka.Width / 1, projectileBazooka.Height / 1);
                positionBar.X = (int)positionPlayer.X + textureWidth / 8 - powerBar.Width / 2;
                positionBar.Y = (int)positionPlayer.Y - 3 * powerBar.Height;
                if ((int)Math.Abs(Mouse.GetState().X - _width / 2 - positionPlayer.X - textureWidth / 4) + (int)Math.Abs(Mouse.GetState().Y - positionPlayer.Y - textureHeight / 4) / 4 / powerBar.Width < powerBar.Width)
                {
                    positionBar.Width = (int)Math.Abs(Mouse.GetState().X - _width / 2 - positionPlayer.X + textureWidth / 4) + (int)Math.Abs(Mouse.GetState().Y - positionPlayer.Y - textureHeight / 4) / 4 / powerBar.Width;
                }
                else
                    positionBar.Width = powerBar.Width;
            }
            _position = positionProjectileBazooka;
        }

        private void SheepMulti(Vector2 positionPlayer, Network network)
        {
            _dommage = 35;
            _weaponNbr = 2;
            //Premier appel => initialisation positions,...
            if (!_alreadyShoot)
            {
                sheepActive = false;
                sheepSound.Play();
                _alreadyShoot = true;
                alreadyMulti = false;
                network._alreadyShoot = true;
                positionProjectile.X = positionPlayer.X;
                positionProjectile.Y = positionPlayer.Y - sheepD.Height / 2;
                _direction = 1;
                _tir = false;
                if (network._isPlayer1)
                    network._shoot = false;
                else
                    network._shootPlayer2 = false;
            }
            //Directions
            lock (new object())
            {
                network.sheepLeft = false;
                network.sheepRight = false;
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                {
                    _direction = 1;
                    positionProjectile.X -= 10;
                    network.sheepLeft = true;
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                    _direction = 2;
                    positionProjectile.X += 10;
                    network.sheepRight = true;
                }
            }
            //Explosion
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !alreadyMulti)
            {
                sheepActive = true;
                _direction = 0;

                sheepSound.Play();
                _tir = true;
                timer = 16;
                if (network._isPlayer1)
                    network._shoot = true;
                else
                    network._shootPlayer2 = true;
                alreadyMulti = true;
                _weaponNbr = 0;
            }
            _position = new Rectangle((int)positionProjectile.X, (int)positionProjectile.Y, sheepD.Width, sheepD.Height);
        }

        private void KameaMulti(Vector2 positionPlayer, Network network)
        {
            _dommage = 20;
            //Premier appel => initialisation positions,...
            if (!_alreadyShoot)
            {
                touch = false;
                _alreadyShoot = true;
                network._alreadyShoot = true;
                _tir = false;
                alreadyMulti = false;
                _weaponNbr = 3;
            }
            //Directions
            if (!_tir)
            {
                //Explosion
                if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    network.nbrWeapon = 0;
                    positionProjectileKamea.X = (int)positionPlayer.X;
                    positionProjectileKamea.Y = (int)positionPlayer.Y + 25;
                    _tir = true;
                    if (network._isPlayer1)
                        network._shoot = true;
                    else
                        network._shootPlayer2 = true;
                    kameaSound.Play();
                    alreadyMulti = true;
                    if (network._isPlayer1)
                    {
                        if (network.shootDirectionLeft1)
                            _direction = 2;
                        else
                            _direction = 1;
                    }
                    else
                    {
                        if (network.shootDirectionLeft2)
                            _direction = 2;
                        else
                            _direction = 1;
                    }
                }
            }
            else
            {
                if (network._isPlayer1)
                    network._shoot = false;
                else
                    network._shootPlayer2 = false;
                if (positionProjectileKamea.X > _width || positionProjectileKamea.X < 0 || touch)
                {
                    _alreadyShoot = false;
                    _tir = false;
                    if (network._isPlayer1)
                        network._shoot = false;
                    else
                        network._shootPlayer2 = false;
                    network._alreadyShoot = false;
                    alreadyMulti = false;
                    network.nbrWeapon = 0;
                    _weaponNbr = 0;
                    touch = false;
                }
                else
                {
                    if (_direction == 2)
                        positionProjectileKamea.X -= 10;
                    else
                        positionProjectileKamea.X += 10;
                }
            }
            _position = positionProjectileKamea;
        }
        #endregion



        #region All the draws
        public void Draw(SpriteBatch spriteBatch)
        {
            if (!_alreadyShoot)
            {
                spriteBatch.Draw(background, new Rectangle(-100 + _width / 2 - Camera.move, 15, 250, 200), Color.White);
                bazookaButton.DrawWeapons(spriteBatch);
                sheepButton.DrawWeapons(spriteBatch);
                kameaButton.DrawWeapons(spriteBatch);
            }
            else
                DrawProjectile(spriteBatch, positionProjectile);
            if (sheepActive && count >= 0)
            {
                sheepParticles.Draw(spriteBatch);
            }

        }

        public void DrawBazooka(SpriteBatch spriteBatch, Vector2 positionProjectile)
        {
            if (_tir)
                spriteBatch.Draw(projectileBazooka, positionProjectileBazooka, Color.White);
            spriteBatch.Draw(powerBar, positionBar, Color.White);
        }

        public void DrawSheep(SpriteBatch spriteBatch, Vector2 positionProjectile)
        {
            if (_direction == 1)
                spriteBatch.Draw(sheepG, positionProjectile, Color.White);
            else if (_direction == 2)
                spriteBatch.Draw(sheepD, positionProjectile, Color.White);

            if (sheepActive && count >= 0)
            {
                sheepParticles.Draw(spriteBatch);
            }
        }

        public void DrawKamea(SpriteBatch spriteBatch, Vector2 positionProjectile)
        {
            if (_tir || alreadyMulti)
                spriteBatch.Draw(kameaG, positionProjectileKamea, Color.White);
        }

        public void DrawBazookaIA(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(projectileBazooka, positionProjectileBazooka, Color.White);
        }

        public void DrawSheepIA(SpriteBatch spriteBatch, bool directionLeft)
        {
            if (_tir)
            {
                if (directionLeft)
                    spriteBatch.Draw(sheepG, positionProjectile, Color.White);
                else
                    spriteBatch.Draw(sheepD, positionProjectile, Color.White);
            }
            if (sheepActive && count >= 0)
            {
                sheepParticles.Draw(spriteBatch);
            }
        }
        public void DrawKameaIA(SpriteBatch spriteBatch)
        {
            if (_tir)
            {
                spriteBatch.Draw(kameaG, positionProjectileKamea, Color.White);
            }
        }

        public void DrawMulti(SpriteBatch spriteBatch, int nbrWeapon, Network network)
        {
            if (!network._alreadyShoot)
            {
                spriteBatch.Draw(background, new Rectangle(-100 + _width / 2, 42, 250, 200), Color.White);
                bazookaButton.DrawWeapons(spriteBatch);
                sheepButton.DrawWeapons(spriteBatch);
                kameaButton.DrawWeapons(spriteBatch);
            }
            if (_weaponNbr != 0 && _alreadyShoot)
                DrawProjectile(spriteBatch, positionProjectile);

            if (sheepActive && count >= 0)
            {
                sheepParticles.Draw(spriteBatch);
            }
        }
        #endregion
    }
}
