﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Soutenance2
{
    class CollisionRectangles
    {
        public static Rectangle position = new Rectangle();
        public static bool collision(Rectangle weapon, List<Rectangle> objects)
        {
            foreach (Rectangle item in objects)
            {
                if (weapon.Intersects(item))
                {
                    position = item;
                    return true;
                }
            }
            return false;
        }
        public static bool collision(Rectangle perso1, Rectangle perso2)
        {
            return perso1.Intersects(perso2);
        }


    }
}
